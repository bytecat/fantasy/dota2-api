package fantasy.dota.db

import scopt.{OParser, OParserBuilder}

case class DbOpt(url: String, user: String, password: String)

case class MigrationOpt(db: DbOpt = DbOpt("", "", ""), migrate: Boolean = false)

object MigrationOpt {
  val parser: OParser[Unit, MigrationOpt] = {
    val builder: OParserBuilder[MigrationOpt] = OParser.builder[MigrationOpt]
    import builder._
    OParser.sequence(
      programName("Dota2 Fantasy App migrator"),
      opt[String]("db.url")
        .valueName("<database url>")
        .action((p, c) => c.copy(db = c.db.copy(url = p))),
      opt[String]("db.user")
        .valueName("<database user>")
        .action((p, c) => c.copy(db = c.db.copy(user = p))),
      opt[String]("db.password")
        .valueName("<database user>")
        .action((p, c) => c.copy(db = c.db.copy(password = p))),
      cmd("liquibase")
        .text("run liquibase")
        .action((p, c) => c.copy(migrate = true))
    )
  }
}
