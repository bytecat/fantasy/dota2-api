package fantasy.dota.db

import java.sql.Connection

import cats.Applicative
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.effect.Bracket
import fantasy.dota.db.Migration.ThrowableBracket
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor

class Migration[F[_]: ThrowableBracket](masterChangeLog: String) {

  private def liquibase(connect: Connection): Liquibase = {
    val database         = DatabaseFactory.getInstance.findCorrectDatabaseImplementation(new JdbcConnection(connect))
    val classLoader      = classOf[Migration[F]].getClassLoader
    val resourceAccessor = new ClassLoaderResourceAccessor(classLoader)

    new Liquibase(masterChangeLog, resourceAccessor, database)
  }

  def update(connection: Connection): F[Unit] = {
    val liq = liquibase(connection)

    Bracket[F, Throwable].guarantee(liq.update(null: String).pure[F])(Applicative[F].unit.map(_ => {
      liq.forceReleaseLocks()
      connection.rollback()
    }))
  }
}

object Migration {
  type ThrowableBracket[F[_]] = Bracket[F, Throwable]

  def create[F[_]: ThrowableBracket](changeLog: String): F[Migration[F]] = new Migration[F](changeLog).pure
}
