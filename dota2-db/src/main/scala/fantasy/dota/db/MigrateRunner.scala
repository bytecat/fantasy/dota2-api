package fantasy.dota.db

import java.sql.{Connection, DriverManager}

import cats.syntax.applicative._
import cats.effect.{ExitCode, IO, IOApp, Resource}
import com.typesafe.scalalogging.StrictLogging
import org.postgresql.Driver
import scopt.OParser

object MigrateRunner extends IOApp with StrictLogging {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      opt <- IO.fromEither(
              OParser
                .parse(MigrationOpt.parser, args, MigrationOpt())
                .toRight(new IllegalStateException("Invalid CLI arguments"))
            )
      _ <- migrate(opt).whenA(opt.migrate)
    } yield ExitCode.Success

  private def migrate(opt: MigrationOpt): IO[Unit] =
    for {
      _ <- IO(logger.info("start migrate"))
      _ <- IO(DriverManager.registerDriver(new Driver()))
      connection = Resource.fromAutoCloseable[IO, Connection](
        IO(DriverManager.getConnection(opt.db.url, opt.db.user, opt.db.password))
      )
      migrator = new Migration[IO]("changelog/master.xml")
      _        <- connection.use(migrator.update)
    } yield ()
}
