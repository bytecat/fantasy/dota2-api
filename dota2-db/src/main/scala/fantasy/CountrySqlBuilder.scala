package fantasy

import java.io.{InputStream, InputStreamReader}

import cats.syntax.either._
import cats.effect.{ExitCode, IO, IOApp, Resource}
import com.typesafe.scalalogging.StrictLogging
import io.circe._
import io.circe.generic.semiauto._
import io.circe.yaml

object CountrySqlBuilder extends IOApp with StrictLogging {

  private def map(c: Country): String =
    s"insert into countries (name, code) values ('{${mapNames(c.name)}}'::json, '${c.iso_2}');"

  private def mapNames(name: Map[String, String]) =
    name.mapValues(_.replaceAll("['\"]", "`")).map { case (k, v) => s""""$k": "$v"""" }.mkString(", ")

  override def run(args: List[String]): IO[ExitCode] =
    Resource
      .fromAutoCloseable[IO, InputStream](
        IO(CountrySqlBuilder.getClass.getClassLoader.getResourceAsStream("countries.yaml"))
      )
      .use(
        in =>
          IO {
            val json = yaml.parser
              .parse(new InputStreamReader(in))
            json
              .leftMap(err => err: Error)
              .flatMap(_.as[Countries])
              .valueOr(throw _)
          }
      )
      .redeem(err => {
        logger.error("failed parse yaml", err)
        ExitCode.Error
      }, c => {
        c.countries.values.map(map).foreach(println)
        ExitCode.Success
      })
}

case class Country(iso_2: String, name: Map[String, String])
object Country {
  implicit val decoder: Decoder[Country] = deriveDecoder
}
case class Countries(countries: Map[String, Country])
object Countries {
  implicit val decoder: Decoder[Countries] = deriveDecoder
}
