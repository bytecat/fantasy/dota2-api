insert into valve_teams (valve_id, team_id)
select
    543897 as valve_id,
    id as team_id
from teams where tag = 'Mski';

insert into valve_players (valve_id, player_id)
select
    126417273 as valve_id,
    id as player_id
from players where nick = 'Ahjit';

insert into valve_players (valve_id, player_id)
select
    113457795 as valve_id,
    id as player_id
from players where nick = 'Moon';

insert into valve_players (valve_id, player_id)
select
    87012746 as valve_id,
    id as player_id
from players where nick = 'kpii';

insert into valve_players (valve_id, player_id)
select
    112377459 as valve_id,
    id as player_id
from players where nick = 'Febby';

insert into valve_players (valve_id, player_id)
select
    91443418 as valve_id,
    id as player_id
from players where nick = 'ninjaboogie';