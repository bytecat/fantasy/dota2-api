insert into teams (name, tag, region) values ('Keen Gaming', 'KG', 'China');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Wang Zhiyong' as name,
    'old chicken' as nick,
    (select id from teams where tag = 'KG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhai Jingkai' as name,
    '一' as nick,
    (select id from teams where tag = 'KG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Ren Yangwei' as name,
    'eLeVeN' as nick,
    (select id from teams where tag = 'KG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Hu Liangzhi' as name,
    'Kaka' as nick,
    (select id from teams where tag = 'KG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Song Runxi' as name,
    'dark' as nick,
    (select id from teams where tag = 'KG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';
