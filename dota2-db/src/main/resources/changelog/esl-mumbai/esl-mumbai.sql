insert into tournaments (name, start_date, end_date, country_id, logo)
select
    'ESL One Mumbai 2019' as name,
    '2019-04-16' as start_date,
    '2019-04-21' as end_date,
    id as coutry_id,
    '/static/tournaments/esl-mumbai-2019.jpg' as logo
from countries where code = 'IN';

insert into aggregation_points (player_id, points) select id as player_id, '{"0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0}'::json as points from players;
