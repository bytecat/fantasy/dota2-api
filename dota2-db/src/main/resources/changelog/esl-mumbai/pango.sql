insert into teams (name, tag, region) values ('The Pango', 'ThePango', 'CIS');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Aybek Tokaev' as name,
    'Naive-' as nick,
    (select id from teams where tag = 'ThePango') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'KZ';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Bogdan Vasilenko' as name,
    'Iceberg' as nick,
    (select id from teams where tag = 'ThePango') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'UA';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Andrey Kadyk' as name,
    'Ghostik' as nick,
    (select id from teams where tag = 'ThePango') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'UA';

insert into players (nick, team_id, position, country_id, price)
select
    'XSvamp1Re' as nick,
    (select id from teams where tag = 'ThePango') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'KZ';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Semion Krivulya' as name,
    'CemaTheSlayer' as nick,
    (select id from teams where tag = 'ThePango') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'UA';
