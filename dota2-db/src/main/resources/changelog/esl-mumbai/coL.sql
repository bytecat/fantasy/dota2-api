insert into teams (name, tag, region) values ('compLexity Gaming', 'coL', 'NorthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Galvin Kang Jian Wen' as name,
    'Meracle' as nick,
    (select id from teams where tag = 'coL') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SG';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Linus Blomdin' as name,
    'Limmp' as nick,
    (select id from teams where tag = 'coL') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yang Wu Heng' as name,
    'Deth' as nick,
    (select id from teams where tag = 'coL') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SG';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zakari Freedman' as name,
    'Zfreek' as nick,
    (select id from teams where tag = 'coL') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Adam Erwann Shah bin Akhtar Hussein' as name,
    '343' as nick,
    (select id from teams where tag = 'coL') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'MY';