insert into valve_teams (valve_id, team_id)
select
    2108395 as valve_id,
    id as team_id
from teams where tag = 'TNC';

insert into valve_players (valve_id, player_id)
select
    152545459 as valve_id,
    id as player_id
from players where nick = 'Gabbi';

insert into valve_players (valve_id, player_id)
select
    164532005 as valve_id,
    id as player_id
from players where nick = 'Armel';

insert into valve_players (valve_id, player_id)
select
    184950344 as valve_id,
    id as player_id
from players where nick = 'Kuku';

insert into valve_players (valve_id, player_id)
select
    155494381 as valve_id,
    id as player_id
from players where nick = 'Tims';

insert into valve_players (valve_id, player_id)
select
    173476224 as valve_id,
    id as player_id
from players where nick = 'eyyou';