insert into teams (name, tag, region) values ('Signify', 'Signify', 'SoutheastAsia');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Balaji Ramnarayan' as name,
    'BlizzarD' as nick,
    (select id from teams where tag = 'Signify') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'IN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jeet Kundra' as name,
    'Swifty' as nick,
    (select id from teams where tag = 'Signify') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'IN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Raunak Sen' as name,
    'Crowley' as nick,
    (select id from teams where tag = 'Signify') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'IN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Moin Ejaz' as name,
    'NO_Chanc3' as nick,
    (select id from teams where tag = 'Signify') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'IN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Dhvanit Negi' as name,
    'eyyNegiou' as nick,
    (select id from teams where tag = 'Signify') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'IN';
