insert into valve_teams (valve_id, team_id)
select
    3 as valve_id,
    id as team_id
from teams where tag = 'coL';

insert into valve_players (valve_id, player_id)
select
    91369376 as valve_id,
    id as player_id
from players where nick = 'Meracle';

insert into valve_players (valve_id, player_id)
select
    12231202 as valve_id,
    id as player_id
from players where nick = 'Limmp';

insert into valve_players (valve_id, player_id)
select
    99983413 as valve_id,
    id as player_id
from players where nick = 'Deth';

insert into valve_players (valve_id, player_id)
select
    50828662 as valve_id,
    id as player_id
from players where nick = 'Zfreek';

insert into valve_players (valve_id, player_id)
select
    111034589 as valve_id,
    id as player_id
from players where nick = '343';