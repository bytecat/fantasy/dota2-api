insert into valve_teams (valve_id, team_id)
select
    6904594 as valve_id,
    id as team_id
from teams where tag = 'TT';

insert into valve_players (valve_id, player_id)
select
    43276219 as valve_id,
    id as player_id
from players where nick = 'EternaLEnVy';

insert into valve_players (valve_id, player_id)
select
    126238768 as valve_id,
    id as player_id
from players where nick = 'Gunnar';

insert into valve_players (valve_id, player_id)
select
    31818853 as valve_id,
    id as player_id
from players where nick = 'Brax';

insert into valve_players (valve_id, player_id)
select
    1296625 as valve_id,
    id as player_id
from players where nick = 'Newsham';

insert into valve_players (valve_id, player_id)
select
    86715129 as valve_id,
    id as player_id
from players where nick = 'ixmike88';