insert into teams (name, tag, region) values ('TNC Predator', 'TNC', 'SoutheastAsia');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kim Villafuerte' as name,
    'Gabbi' as nick,
    (select id from teams where tag = 'TNC') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Armel Paul Tabios' as name,
    'Armel' as nick,
    (select id from teams where tag = 'TNC') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Carlo Palad' as name,
    'Kuku' as nick,
    (select id from teams where tag = 'TNC') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Timothy John Randrup' as name,
    'Tims' as nick,
    (select id from teams where tag = 'TNC') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Nico Barcelon' as name,
    'eyyou' as nick,
    (select id from teams where tag = 'TNC') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';
