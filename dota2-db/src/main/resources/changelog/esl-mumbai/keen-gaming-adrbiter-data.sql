insert into valve_teams (valve_id, team_id)
select
    2626685 as valve_id,
    id as team_id
from teams where tag = 'KG';

insert into valve_players (valve_id, player_id)
select
    135878232 as valve_id,
    id as player_id
from players where nick = 'old chicken';

insert into valve_players (valve_id, player_id)
select
    255219872 as valve_id,
    id as player_id
from players where nick = '一';

insert into valve_players (valve_id, player_id)
select
    134276083 as valve_id,
    id as player_id
from players where nick = 'eLeVeN';

insert into valve_players (valve_id, player_id)
select
    139876032 as valve_id,
    id as player_id
from players where nick = 'Kaka';

insert into valve_players (valve_id, player_id)
select
    397462905 as valve_id,
    id as player_id
from players where nick = 'dark';