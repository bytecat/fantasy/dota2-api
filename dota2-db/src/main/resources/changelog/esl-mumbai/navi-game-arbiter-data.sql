insert into valve_teams (valve_id, team_id)
select
    36 as valve_id,
    id as team_id
from teams where tag = 'Na`Vi';

insert into valve_players (valve_id, player_id)
select
    114619230 as valve_id,
    id as player_id
from players where nick = 'Crystallize';

insert into valve_players (valve_id, player_id)
select
    171981096 as valve_id,
    id as player_id
from players where nick = 'MagicaL';

insert into valve_players (valve_id, player_id)
select
    234699894 as valve_id,
    id as player_id
from players where nick = 'Blizzy';

insert into valve_players (valve_id, player_id)
select
    117483894 as valve_id,
    id as player_id
from players where nick = 'Chuvash';

insert into valve_players (valve_id, player_id)
select
    90125566 as valve_id,
    id as player_id
from players where nick = 'Misha';