insert into teams (name, tag, region) values ('TEAM TEAM', 'TT', 'NorthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jacky Mao' as name,
    'EternaLEnVy' as nick,
    (select id from teams where tag = 'TT') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CA';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Nico Lopez' as name,
    'Gunnar' as nick,
    (select id from teams where tag = 'TT') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Braxton Paulson' as name,
    'Brax' as nick,
    (select id from teams where tag = 'TT') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jason Newsham' as name,
    'Newsham' as nick,
    (select id from teams where tag = 'TT') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Michael Ghannam' as name,
    'ixmike88' as nick,
    (select id from teams where tag = 'TT') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';
