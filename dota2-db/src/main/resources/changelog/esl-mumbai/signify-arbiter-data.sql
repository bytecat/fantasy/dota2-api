insert into valve_teams (valve_id, team_id)
select
    5138280 as valve_id,
    id as team_id
from teams where tag = 'Signify';

insert into valve_players (valve_id, player_id)
select
    111637216 as valve_id,
    id as player_id
from players where nick = 'BlizzarD';

insert into valve_players (valve_id, player_id)
select
    124553723 as valve_id,
    id as player_id
from players where nick = 'Swifty';

insert into valve_players (valve_id, player_id)
select
    51747431 as valve_id,
    id as player_id
from players where nick = 'Crowley';

insert into valve_players (valve_id, player_id)
select
    107227595 as valve_id,
    id as player_id
from players where nick = 'NO_Chanc3';

insert into valve_players (valve_id, player_id)
select
    102126005 as valve_id,
    id as player_id
from players where nick = 'eyyNegiou';