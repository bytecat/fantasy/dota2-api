insert into valve_teams (valve_id, team_id)
select
    6298307 as valve_id,
    id as team_id
from teams where tag = 'ThePango';

insert into valve_players (valve_id, player_id)
select
    96183976 as valve_id,
    id as player_id
from players where nick = 'Naive-';

insert into valve_players (valve_id, player_id)
select
    250114507 as valve_id,
    id as player_id
from players where nick = 'Iceberg';

insert into valve_players (valve_id, player_id)
select
    92847434 as valve_id,
    id as player_id
from players where nick = 'Ghostik';

insert into valve_players (valve_id, player_id)
select
    326327879 as valve_id,
    id as player_id
from players where nick = 'XSvamp1Re';

insert into valve_players (valve_id, player_id)
select
    91460772 as valve_id,
    id as player_id
from players where nick = 'CemaTheSlayer';