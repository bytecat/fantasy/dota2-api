insert into teams (name, tag, region) values ('Natus Vincere', 'Na`Vi', 'CIS');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Vladislav Krystanek' as name,
    'Crystallize' as nick,
    (select id from teams where tag = 'Na`Vi') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'UA';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Idan Vardanian' as name,
    'MagicaL' as nick,
    (select id from teams where tag = 'Na`Vi') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'UA';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Evgeniy Ree' as name,
    'Blizzy' as nick,
    (select id from teams where tag = 'Na`Vi') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'KG';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Evgeniy Makarov' as name,
    'Chuvash' as nick,
    (select id from teams where tag = 'Na`Vi') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Mihail Agatov' as name,
    'Misha' as nick,
    (select id from teams where tag = 'Na`Vi') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';
