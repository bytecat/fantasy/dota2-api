insert into teams (name, tag, region) values ('Mineski', 'Mski', 'SoutheastAsia');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Lai Jay Son' as name,
    'Ahjit' as nick,
    (select id from teams where tag = 'Mski') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'MY';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kam Boon Seng' as name,
    'Moon' as nick,
    (select id from teams where tag = 'Mski') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'MY';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Damien Chok' as name,
    'kpii' as nick,
    (select id from teams where tag = 'Mski') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'AU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kim Yong-min' as name,
    'Febby' as nick,
    (select id from teams where tag = 'Mski') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'KR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Michael Ross Jr.' as name,
    'ninjaboogie' as nick,
    (select id from teams where tag = 'Mski') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';