insert into teams (name, tag, region) values ('PSG.LGD', 'PSG.LGD', 'China');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Wang Chunyu' as name,
    'Ame' as nick,
    (select id from teams where tag = 'PSG.LGD') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Lu Yao' as name,
    'Somnus丶M' as nick,
    (select id from teams where tag = 'PSG.LGD') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yang Shenyi' as name,
    'Chalice' as nick,
    (select id from teams where tag = 'PSG.LGD') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Xu Linsen' as name,
    'fy' as nick,
    (select id from teams where tag = 'PSG.LGD') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yap Jian Wei' as name,
    'xNova' as nick,
    (select id from teams where tag = 'PSG.LGD') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'MY';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    15 as valve_id,
    id as team_id
from teams where tag = 'PSG.LGD';

insert into valve_players (valve_id, player_id)
select
    125581247 as valve_id,
    id as player_id
from players where nick = 'Ame';

insert into valve_players (valve_id, player_id)
select
    106863163 as valve_id,
    id as player_id
from players where nick = 'Somnus丶M';

insert into valve_players (valve_id, player_id)
select
    94738847 as valve_id,
    id as player_id
from players where nick = 'Chalice';

insert into valve_players (valve_id, player_id)
select
    101695162 as valve_id,
    id as player_id
from players where nick = 'fy';

insert into valve_players (valve_id, player_id)
select
    94296097 as valve_id,
    id as player_id
from players where nick = 'xNova';