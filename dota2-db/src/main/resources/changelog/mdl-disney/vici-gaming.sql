insert into teams (name, tag, region) values ('Vici Gaming', 'VG', 'China');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhang Chengjun' as name,
    'Paparazi' as nick,
    (select id from teams where tag = 'VG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zeng Jiaoyang' as name,
    'Ori' as nick,
    (select id from teams where tag = 'VG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhou Haiyang' as name,
    'Yang' as nick,
    (select id from teams where tag = 'VG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Pan Yi' as name,
    'Fade' as nick,
    (select id from teams where tag = 'VG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Ding Cong' as name,
    'Dy' as nick,
    (select id from teams where tag = 'VG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    726228 as valve_id,
    id as team_id
from teams where tag = 'VG';

insert into valve_players (valve_id, player_id)
select
    137193239 as valve_id,
    id as player_id
from players where nick = 'Paparazi';

insert into valve_players (valve_id, player_id)
select
    107803494 as valve_id,
    id as player_id
from players where nick = 'Ori';

insert into valve_players (valve_id, player_id)
select
    139937922 as valve_id,
    id as player_id
from players where nick = 'Yang';

insert into valve_players (valve_id, player_id)
select
    182331313 as valve_id,
    id as player_id
from players where nick = 'Fade';

insert into valve_players (valve_id, player_id)
select
    143693439 as valve_id,
    id as player_id
from players where nick = 'Dy';