update players set team_id = null where nick = 'Deth';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jaron Clinton' as name,
    'monkeys-forever' as nick,
    (select id from teams where tag = 'coL') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into valve_players (valve_id, player_id)
select
    86811043 as valve_id,
    id as player_id
from players where nick = 'monkeys-forever';