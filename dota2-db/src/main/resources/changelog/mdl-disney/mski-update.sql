update players set team_id = null where nick = 'Febby';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Ryan Jay Qui' as name,
    'Bimbo' as nick,
    (select id from teams where tag = 'Mski') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into valve_players (valve_id, player_id)
select
    192914280 as valve_id,
    id as player_id
from players where nick = 'Bimbo';