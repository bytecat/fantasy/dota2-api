insert into teams (name, tag, region) values ('paiN Gaming', 'paiN', 'SouthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Leonardo Viana' as name,
    'Mandy' as nick,
    (select id from teams where tag = 'paiN') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Adriano Machado' as name,
    '4dr' as nick,
    (select id from teams where tag = 'paiN') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Rodrigo Lelis' as name,
    'Lelis' as nick,
    (select id from teams where tag = 'paiN') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Thiago Cordeiro' as name,
    'Thiolicor' as nick,
    (select id from teams where tag = 'paiN') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Anderson Santos' as name,
    '444' as nick,
    (select id from teams where tag = 'paiN') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    67 as valve_id,
    id as team_id
from teams where tag = 'paiN';

insert into valve_players (valve_id, player_id)
select
    131706718 as valve_id,
    id as player_id
from players where nick = 'Mandy';

insert into valve_players (valve_id, player_id)
select
    85937380 as valve_id,
    id as player_id
from players where nick = '4dr';

insert into valve_players (valve_id, player_id)
select
    87063175 as valve_id,
    id as player_id
from players where nick = 'Lelis';

insert into valve_players (valve_id, player_id)
select
    105045291 as valve_id,
    id as player_id
from players where nick = 'Thiolicor';

insert into valve_players (valve_id, player_id)
select
    234237646 as valve_id,
    id as player_id
from players where nick = '444';