insert into teams (name, tag, region) values ('Team Empire', 'Empire', 'CIS');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kiyalbek Tayirov' as name,
    'dream`' as nick,
    (select id from teams where tag = 'Empire') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'KG';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Konstantin Kogai' as name,
    'Kodos' as nick,
    (select id from teams where tag = 'Empire') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Igor Modenov' as name,
    'Maden' as nick,
    (select id from teams where tag = 'Empire') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Oleg Kalenbet' as name,
    'sayuw' as nick,
    (select id from teams where tag = 'Empire') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Rinat Abdullin' as name,
    'KingR' as nick,
    (select id from teams where tag = 'Empire') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    46 as valve_id,
    id as team_id
from teams where tag = 'Empire';

insert into valve_players (valve_id, player_id)
select
    241519559 as valve_id,
    id as player_id
from players where nick = 'dream`';

insert into valve_players (valve_id, player_id)
select
    230328929 as valve_id,
    id as player_id
from players where nick = 'Kodos';

insert into valve_players (valve_id, player_id)
select
    93473848 as valve_id,
    id as player_id
from players where nick = 'Maden';

insert into valve_players (valve_id, player_id)
select
    145065875 as valve_id,
    id as player_id
from players where nick = 'sayuw';

insert into valve_players (valve_id, player_id)
select
    182993582 as valve_id,
    id as player_id
from players where nick = 'KingR';