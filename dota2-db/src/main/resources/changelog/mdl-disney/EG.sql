insert into teams (name, tag, region) values ('Evil Geniuses', 'EG', 'SouthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Artour Babaev' as name,
    'Arteezy' as nick,
    (select id from teams where tag = 'EG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CA';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Sumail Hassan' as name,
    'SumaiL' as nick,
    (select id from teams where tag = 'EG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PK';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Gustav Magnusson' as name,
    's4' as nick,
    (select id from teams where tag = 'EG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Andreas Nielsen' as name,
    'Cr1t-' as nick,
    (select id from teams where tag = 'EG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'DK';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Tal Aizik' as name,
    'Fly' as nick,
    (select id from teams where tag = 'EG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'IL';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    39 as valve_id,
    id as team_id
from teams where tag = 'EG';

insert into valve_players (valve_id, player_id)
select
    86745912 as valve_id,
    id as player_id
from players where nick = 'Arteezy';

insert into valve_players (valve_id, player_id)
select
    111620041 as valve_id,
    id as player_id
from players where nick = 'SumaiL';

insert into valve_players (valve_id, player_id)
select
    41231571 as valve_id,
    id as player_id
from players where nick = 's4';

insert into valve_players (valve_id, player_id)
select
    25907144 as valve_id,
    id as player_id
from players where nick = 'Cr1t-';

insert into valve_players (valve_id, player_id)
select
    94155156 as valve_id,
    id as player_id
from players where nick = 'Fly';