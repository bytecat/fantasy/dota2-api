insert into teams (name, tag, region) values ('Team Liquid', 'Liquid', 'Europa');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Lasse Urpalainen' as name,
    'MATUMBAMAN' as nick,
    (select id from teams where tag = 'Liquid') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'FI';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Amer Al-Barkawi' as name,
    'Miracle-' as nick,
    (select id from teams where tag = 'Liquid') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'JO';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Ivan Ivanov' as name,
    'MinD_ContRoL' as nick,
    (select id from teams where tag = 'Liquid') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BG';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Maroun Merhej' as name,
    'GH' as nick,
    (select id from teams where tag = 'Liquid') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'LB';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kuro Salehi Takhasomi' as name,
    'KuroKy' as nick,
    (select id from teams where tag = 'Liquid') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'DE';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    2163 as valve_id,
    id as team_id
from teams where tag = 'Liquid';

insert into valve_players (valve_id, player_id)
select
    72312627 as valve_id,
    id as player_id
from players where nick = 'MATUMBAMAN';

insert into valve_players (valve_id, player_id)
select
    105248644 as valve_id,
    id as player_id
from players where nick = 'Miracle-';

insert into valve_players (valve_id, player_id)
select
    34505203 as valve_id,
    id as player_id
from players where nick = 'MinD_ContRoL';

insert into valve_players (valve_id, player_id)
select
    101356886 as valve_id,
    id as player_id
from players where nick = 'GH';

insert into valve_players (valve_id, player_id)
select
    82262664 as valve_id,
    id as player_id
from players where nick = 'KuroKy';