insert into teams (name, tag, region) values ('beastcoast', 'bc', 'NorthAmerica');

update players set team_id = (select id from teams where tag = 'bc')
where nick = 'EternaLEnVy';

update players set team_id = (select id from teams where tag = 'bc')
where nick = 'Brax';

update players set team_id = (select id from teams where tag = 'bc')
where nick = 'ixmike88';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Eric Dong' as name,
    'Ryoya' as nick,
    (select id from teams where tag = 'bc') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Timothy Ong' as name,
    'tmt' as nick,
    (select id from teams where tag = 'bc') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'SG';

--- meta

insert into valve_teams (valve_id, team_id)
select
    7079109 as valve_id,
    id as team_id
from teams where tag = 'bc';

insert into valve_players (valve_id, player_id)
select
    115141430 as valve_id,
    id as player_id
from players where nick = 'Ryoya';

insert into valve_players (valve_id, player_id)
select
    168340162 as valve_id,
    id as player_id
from players where nick = 'tmt';