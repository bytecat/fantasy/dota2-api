insert into teams (name, tag, region) values ('Chaos Esports Club', 'Chaos', 'SouthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'William Medeiros' as name,
    'hFn' as nick,
    (select id from teams where tag = 'Chaos') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Aliwi Omar' as name,
    'w33' as nick,
    (select id from teams where tag = 'Chaos') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RO';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Otavio Gabriel' as name,
    'tavo' as nick,
    (select id from teams where tag = 'Chaos') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Rasmus Berth Filipsen' as name,
    'MISERY' as nick,
    (select id from teams where tag = 'Chaos') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'DK';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Danylo Nascimento' as name,
    'Kingrd' as nick,
    (select id from teams where tag = 'Chaos') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    6666989 as valve_id,
    id as team_id
from teams where tag = 'Chaos';

insert into valve_players (valve_id, player_id)
select
    94004717 as valve_id,
    id as player_id
from players where nick = 'hFn';

insert into valve_players (valve_id, player_id)
select
    86700461 as valve_id,
    id as player_id
from players where nick = 'w33';

insert into valve_players (valve_id, player_id)
select
    117956848 as valve_id,
    id as player_id
from players where nick = 'tavo';

insert into valve_players (valve_id, player_id)
select
    87382579 as valve_id,
    id as player_id
from players where nick = 'MISERY';

insert into valve_players (valve_id, player_id)
select
    84853828 as valve_id,
    id as player_id
from players where nick = 'Kingrd';