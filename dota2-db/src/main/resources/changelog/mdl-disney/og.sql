insert into teams (name, tag, region) values ('OG', 'OG', 'Europa');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Anathan Pham' as name,
    'ana' as nick,
    (select id from teams where tag = 'OG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'AU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Topias Taavitsainen' as name,
    'Topson' as nick,
    (select id from teams where tag = 'OG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'FI';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Sébastien Debs' as name,
    '7ckngMad' as nick,
    (select id from teams where tag = 'OG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'FR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jesse Vainikka' as name,
    'JerAx' as nick,
    (select id from teams where tag = 'OG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'FI';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Johan Sundstein' as name,
    'N0tail' as nick,
    (select id from teams where tag = 'OG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'DK';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    2586976 as valve_id,
    id as team_id
from teams where tag = 'OG';

insert into valve_players (valve_id, player_id)
select
    311360822 as valve_id,
    id as player_id
from players where nick = 'ana';

insert into valve_players (valve_id, player_id)
select
    94054712 as valve_id,
    id as player_id
from players where nick = 'Topson';

insert into valve_players (valve_id, player_id)
select
    88271237 as valve_id,
    id as player_id
from players where nick = '7ckngMad';

insert into valve_players (valve_id, player_id)
select
    26771994 as valve_id,
    id as player_id
from players where nick = 'JerAx';

insert into valve_players (valve_id, player_id)
select
    19672354 as valve_id,
    id as player_id
from players where nick = 'N0tail';