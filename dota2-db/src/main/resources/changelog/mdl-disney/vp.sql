insert into teams (name, tag, region) values ('Virtus.pro', 'VP', 'CIS');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Roman Kushnarev' as name,
    'RAMZES666' as nick,
    (select id from teams where tag = 'VP') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Vladimir Minenko' as name,
    'No[o]ne' as nick,
    (select id from teams where tag = 'VP') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'UA';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Pavel Khvastunov' as name,
    '9pasha' as nick,
    (select id from teams where tag = 'VP') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Vladimir Nikogosyan' as name,
    'RodjER' as nick,
    (select id from teams where tag = 'VP') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Alexei Berezin' as name,
    'Solo' as nick,
    (select id from teams where tag = 'VP') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    1883502 as valve_id,
    id as team_id
from teams where tag = 'VP';

insert into valve_players (valve_id, player_id)
select
    132851371 as valve_id,
    id as player_id
from players where nick = 'RAMZES666';

insert into valve_players (valve_id, player_id)
select
    106573901 as valve_id,
    id as player_id
from players where nick = 'No[o]ne';

insert into valve_players (valve_id, player_id)
select
    92423451 as valve_id,
    id as player_id
from players where nick = '9pasha';

insert into valve_players (valve_id, player_id)
select
    159020918 as valve_id,
    id as player_id
from players where nick = 'RodjER';

insert into valve_players (valve_id, player_id)
select
    134556694 as valve_id,
    id as player_id
from players where nick = 'Solo';