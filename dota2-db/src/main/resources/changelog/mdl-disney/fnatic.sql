insert into teams (name, tag, region) values ('Fnatic', 'Fnatic', 'SoutheastAsia');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Pyo No-a' as name,
    'MP' as nick,
    (select id from teams where tag = 'Fnatic') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'KR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Abed Azel Yusop' as name,
    'Abed' as nick,
    (select id from teams where tag = 'Fnatic') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Daryl Koh Pei Xiang' as name,
    'iceiceice' as nick,
    (select id from teams where tag = 'Fnatic') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SG';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Djardel Mampusti' as name,
    'DJ' as nick,
    (select id from teams where tag = 'Fnatic') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'PH';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Anucha Jirawong' as name,
    'Jabz' as nick,
    (select id from teams where tag = 'Fnatic') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'TH';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    350190 as valve_id,
    id as team_id
from teams where tag = 'Fnatic';

insert into valve_players (valve_id, player_id)
select
    101450083 as valve_id,
    id as player_id
from players where nick = 'MP';

insert into valve_players (valve_id, player_id)
select
    154715080 as valve_id,
    id as player_id
from players where nick = 'Abed';

insert into valve_players (valve_id, player_id)
select
    84772440 as valve_id,
    id as player_id
from players where nick = 'iceiceice';

insert into valve_players (valve_id, player_id)
select
    102099826 as valve_id,
    id as player_id
from players where nick = 'DJ';

insert into valve_players (valve_id, player_id)
select
    100471531 as valve_id,
    id as player_id
from players where nick = 'Jabz';