insert into teams (name, tag, region) values ('Team Secret', 'Secret', 'Europa');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Michał Jankowski' as name,
    'Nisha' as nick,
    (select id from teams where tag = 'Secret') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PL';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yeik Nai Zheng' as name,
    'MidOne' as nick,
    (select id from teams where tag = 'Secret') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'MY';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Ludwig Wåhlberg' as name,
    'zai' as nick,
    (select id from teams where tag = 'Secret') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yazied Jaradat' as name,
    'YapzOr' as nick,
    (select id from teams where tag = 'Secret') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'JO';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Clement Ivanov' as name,
    'Puppey' as nick,
    (select id from teams where tag = 'Secret') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'EE';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    1838315 as valve_id,
    id as team_id
from teams where tag = 'Secret';

insert into valve_players (valve_id, player_id)
select
    121769650 as valve_id,
    id as player_id
from players where nick = 'Nisha';

insert into valve_players (valve_id, player_id)
select
    116585378 as valve_id,
    id as player_id
from players where nick = 'MidOne';

insert into valve_players (valve_id, player_id)
select
    73562326 as valve_id,
    id as player_id
from players where nick = 'zai';

insert into valve_players (valve_id, player_id)
select
    89117038 as valve_id,
    id as player_id
from players where nick = 'YapzOr';

insert into valve_players (valve_id, player_id)
select
    87278757 as valve_id,
    id as player_id
from players where nick = 'Puppey';