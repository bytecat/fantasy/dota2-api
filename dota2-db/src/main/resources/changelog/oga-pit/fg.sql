insert into teams (name, tag, region) values ('Forward Gaming', 'FWD', 'NorthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yawar Hassan' as name,
    'YawaR' as nick,
    (select id from teams where tag = 'FWD') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PK';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Quinn Callahan' as name,
    'CCnC' as nick,
    (select id from teams where tag = 'FWD') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jingjun Wu' as name,
    'Sneyking' as nick,
    (select id from teams where tag = 'FWD') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kartik Rathi' as name,
    'Kitrak' as nick,
    (select id from teams where tag = 'FWD') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Arif Anwar' as name,
    'MSS' as nick,
    (select id from teams where tag = 'FWD') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    6214538 as valve_id,
    id as team_id
from teams where tag = 'FWD';

insert into valve_players (valve_id, player_id)
select
    108452107 as valve_id,
    id as player_id
from players where nick = 'YawaR';

insert into valve_players (valve_id, player_id)
select
    221666230 as valve_id,
    id as player_id
from players where nick = 'CCnC';

insert into valve_players (valve_id, player_id)
select
    10366616 as valve_id,
    id as player_id
from players where nick = 'Sneyking';

insert into valve_players (valve_id, player_id)
select
    187824320 as valve_id,
    id as player_id
from players where nick = 'Kitrak';

insert into valve_players (valve_id, player_id)
select
    86726887 as valve_id,
    id as player_id
from players where nick = 'MSS';