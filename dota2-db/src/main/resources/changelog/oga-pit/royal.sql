insert into teams (name, tag, region) values ('Royal Never Give Up', 'RNG', 'China');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Du Peng' as name,
    'Monet' as nick,
    (select id from teams where tag = 'RNG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Gao Zhenxiong' as name,
    'Setsu' as nick,
    (select id from teams where tag = 'RNG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Su Lei' as name,
    'Flyby' as nick,
    (select id from teams where tag = 'RNG') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhang Zhicheng' as name,
    'LaNm' as nick,
    (select id from teams where tag = 'RNG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Tue Soon Chuan' as name,
    'ah fu' as nick,
    (select id from teams where tag = 'RNG') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'MY';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    6209804 as valve_id,
    id as team_id
from teams where tag = 'RNG';

insert into valve_players (valve_id, player_id)
select
    148215639 as valve_id,
    id as player_id
from players where nick = 'Monet';

insert into valve_players (valve_id, player_id)
select
    139822354 as valve_id,
    id as player_id
from players where nick = 'Setsu';

insert into valve_players (valve_id, player_id)
select
    186627166 as valve_id,
    id as player_id
from players where nick = 'Flyby';

insert into valve_players (valve_id, player_id)
select
    89423756 as valve_id,
    id as player_id
from players where nick = 'LaNm';

insert into valve_players (valve_id, player_id)
select
    119576842 as valve_id,
    id as player_id
from players where nick = 'ah fu';