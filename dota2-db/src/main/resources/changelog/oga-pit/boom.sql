insert into teams (name, tag, region) values ('BOOM ID', 'BOOM', 'SoutheastAsia');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Rafli Fathur Rahman' as name,
    'Mikoto' as nick,
    (select id from teams where tag = 'BOOM') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'ID';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Randy Sapoetra' as name,
    'Dreamocel' as nick,
    (select id from teams where tag = 'BOOM') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'ID';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Saieful Ilham' as name,
    'Fbz' as nick,
    (select id from teams where tag = 'BOOM') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'ID';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Tri Kuncoro' as name,
    'Jhocam' as nick,
    (select id from teams where tag = 'BOOM') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'ID';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Alfi Nelphyana' as name,
    'Khezcut' as nick,
    (select id from teams where tag = 'BOOM') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'ID';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    3785359 as valve_id,
    id as team_id
from teams where tag = 'BOOM';

insert into valve_players (valve_id, player_id)
select
    301750126 as valve_id,
    id as player_id
from players where nick = 'Mikoto';

insert into valve_players (valve_id, player_id)
select
    389033587 as valve_id,
    id as player_id
from players where nick = 'Dreamocel';

insert into valve_players (valve_id, player_id)
select
    156328257 as valve_id,
    id as player_id
from players where nick = 'Fbz';

insert into valve_players (valve_id, player_id)
select
    152859296 as valve_id,
    id as player_id
from players where nick = 'Jhocam';

insert into valve_players (valve_id, player_id)
select
    237325411 as valve_id,
    id as player_id
from players where nick = 'Khezcute';