insert into teams (name, tag, region) values ('Gambit Esports', 'Gambit', 'CIS');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Nikita Kuzmin' as name,
    'Daxak ' as nick,
    (select id from teams where tag = 'Gambit') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Andrey Afonin' as name,
    'Afoninje' as nick,
    (select id from teams where tag = 'Gambit') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Vasily Shishkin' as name,
    'AfterLife' as nick,
    (select id from teams where tag = 'Gambit') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Alexander Hmelevskoy' as name,
    'Immersion' as nick,
    (select id from teams where tag = 'Gambit') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Artiom Barshack' as name,
    'fng' as nick,
    (select id from teams where tag = 'Gambit') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'RU';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    6209143 as valve_id,
    id as team_id
from teams where tag = 'Gambit';

insert into valve_players (valve_id, player_id)
select
    177411785 as valve_id,
    id as player_id
from players where nick = 'Daxak ';

insert into valve_players (valve_id, player_id)
select
    81852496 as valve_id,
    id as player_id
from players where nick = 'Afoninje';

insert into valve_players (valve_id, player_id)
select
    86785083 as valve_id,
    id as player_id
from players where nick = 'AfterLife';

insert into valve_players (valve_id, player_id)
select
    295697470 as valve_id,
    id as player_id
from players where nick = 'Immersion';

insert into valve_players (valve_id, player_id)
select
    94049589 as valve_id,
    id as player_id
from players where nick = 'fng';