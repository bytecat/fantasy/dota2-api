insert into teams (name, tag, region) values ('Alliance', '[A]', 'Europa');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Omar Dabachach' as name,
    'Madara' as nick,
    (select id from teams where tag = '[A]') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'GR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Max Bröcker' as name,
    'qojqva' as nick,
    (select id from teams where tag = '[A]') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'DE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Samuel Svahn' as name,
    'Boxi' as nick,
    (select id from teams where tag = '[A]') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'SE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Tommy Le' as name,
    'Taiga' as nick,
    (select id from teams where tag = '[A]') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'NO';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Aydin Sarkohi' as name,
    'iNSaNiA' as nick,
    (select id from teams where tag = '[A]') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'SE';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    111474 as valve_id,
    id as team_id
from teams where tag = '[A]';

insert into valve_players (valve_id, player_id)
select
    95430068 as valve_id,
    id as player_id
from players where nick = 'Madara';

insert into valve_players (valve_id, player_id)
select
    86738694 as valve_id,
    id as player_id
from players where nick = 'qojqva';

insert into valve_players (valve_id, player_id)
select
    77490514 as valve_id,
    id as player_id
from players where nick = 'Boxi';

insert into valve_players (valve_id, player_id)
select
    401792574 as valve_id,
    id as player_id
from players where nick = 'Taiga';

insert into valve_players (valve_id, player_id)
select
    54580962 as valve_id,
    id as player_id
from players where nick = 'iNSaNiA';