insert into teams (name, tag, region) values ('EHOME', 'EHOME', 'China');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Yang Pu' as name,
    'END' as nick,
    (select id from teams where tag = 'EHOME') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Luo Feichi' as name,
    'Ferrari_430' as nick,
    (select id from teams where tag = 'EHOME') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhang Ning' as name,
    'xiao8' as nick,
    (select id from teams where tag = 'EHOME') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhao Zixing' as name,
    'XinQ' as nick,
    (select id from teams where tag = 'EHOME') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Zhang Yiping' as name,
    'y`' as nick,
    (select id from teams where tag = 'EHOME') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'CN';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    4 as valve_id,
    id as team_id
from teams where tag = 'EHOME';

insert into valve_players (valve_id, player_id)
select
    139280377 as valve_id,
    id as player_id
from players where nick = 'END';

insert into valve_players (valve_id, player_id)
select
    88585077 as valve_id,
    id as player_id
from players where nick = 'Ferrari_430';

insert into valve_players (valve_id, player_id)
select
    98887913 as valve_id,
    id as player_id
from players where nick = 'xiao8';

insert into valve_players (valve_id, player_id)
select
    157475523 as valve_id,
    id as player_id
from players where nick = 'XinQ';

insert into valve_players (valve_id, player_id)
select
    111114687 as valve_id,
    id as player_id
from players where nick = 'y`';