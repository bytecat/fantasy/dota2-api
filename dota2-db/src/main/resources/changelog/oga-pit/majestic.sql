insert into teams (name, tag, region) values ('Majestic Esports', 'Majestic', 'SouthAmerica');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Hector Antonio Rodriguez' as name,
    'K1' as nick,
    (select id from teams where tag = 'Majestic') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Jean Pierre Gonzales' as name,
    'Chris Brown' as nick,
    (select id from teams where tag = 'Majestic') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'PE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Adrian Cespedes Dobles Nicosia' as name,
    'Wisper' as nick,
    (select id from teams where tag = 'Majestic') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'BO';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Kaue Camuci' as name,
    'Dunha1' as nick,
    (select id from teams where tag = 'Majestic') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'BR';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Junior Reyes Rimari' as name,
    'Yadomi' as nick,
    (select id from teams where tag = 'Majestic') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'PE';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    486667 as valve_id,
    id as team_id
from teams where tag = 'Majestic';

insert into valve_players (valve_id, player_id)
select
    164685175 as valve_id,
    id as player_id
from players where nick = 'K1';

insert into valve_players (valve_id, player_id)
select
    153836240 as valve_id,
    id as player_id
from players where nick = 'Chris Brown';

insert into valve_players (valve_id, player_id)
select
    292921272 as valve_id,
    id as player_id
from players where nick = 'Wisper';

insert into valve_players (valve_id, player_id)
select
    88153289 as valve_id,
    id as player_id
from players where nick = 'Dunha1';

insert into valve_players (valve_id, player_id)
select
    177953305 as valve_id,
    id as player_id
from players where nick = 'Yadomi';