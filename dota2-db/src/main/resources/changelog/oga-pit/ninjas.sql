insert into teams (name, tag, region) values ('Ninjas in Pyjamas', 'NiP', 'Europa');

insert into players (name, nick, team_id, position, country_id, price)
select
    'Marcus Hoelgaard' as name,
    'Ace' as nick,
    (select id from teams where tag = 'NiP') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'DK';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Adrian Trinks' as name,
    'Fata' as nick,
    (select id from teams where tag = 'NiP') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'DE';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Neta Shapira' as name,
    '33' as nick,
    (select id from teams where tag = 'NiP') as team_id,
    1 as position,
    id as country_id,
    10 as price
from countries where code = 'IL';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Martin Sazdov' as name,
    'Saksa' as nick,
    (select id from teams where tag = 'NiP') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'MK';

insert into players (name, nick, team_id, position, country_id, price)
select
    'Peter Dager' as name,
    'ppd' as nick,
    (select id from teams where tag = 'NiP') as team_id,
    2 as position,
    id as country_id,
    10 as price
from countries where code = 'US';

-------------meta

insert into valve_teams (valve_id, team_id)
select
    6214973 as valve_id,
    id as team_id
from teams where tag = 'NiP';

insert into valve_players (valve_id, player_id)
select
    97590558 as valve_id,
    id as player_id
from players where nick = 'Ace';

insert into valve_players (valve_id, player_id)
select
    86799300 as valve_id,
    id as player_id
from players where nick = 'Fata';

insert into valve_players (valve_id, player_id)
select
    86698277 as valve_id,
    id as player_id
from players where nick = '33';

insert into valve_players (valve_id, player_id)
select
    103735745 as valve_id,
    id as player_id
from players where nick = 'Saksa';

insert into valve_players (valve_id, player_id)
select
    86727555 as valve_id,
    id as player_id
from players where nick = 'ppd';