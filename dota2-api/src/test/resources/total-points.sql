insert into aggregation_points(player_id, points)
select p.id as player_id,
       '{"0":0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 0, "6": 0, "7": 0, "8": 0, "9": 0, "10": 0, "11": 0, "12": 0, "13": 0, "14": 0, "15": 0, "16": 0, "17": 0, "18": 0, "19": 0}'::json as points
from players p where not exists(select 1 from aggregation_points ap where ap.player_id = p.id);