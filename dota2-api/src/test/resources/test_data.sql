insert into countries (id, name, code) values (1, '{"RU": "Россия", "EN":"Russia"}'::json, 'RU');
insert into countries (id, name, code) values (2, '{"RU": "Швеция", "EN":"Sweden"}'::json, 'SE');

insert into tournaments (name, start_date, end_date, country_id, logo) values ('DreamLeague Season 11', '2019-03-14', '2019-03-24', 2, null);

insert into rounds (from_date, to_date, tournament_id) select '2019-03-20 00:00:00', '2019-03-20 23:00:00', id from tournaments where name = 'DreamLeague Season 11';
insert into rounds (from_date, to_date, tournament_id) select '2019-03-22 00:00:00', '2019-03-22 23:00:00', id from tournaments where name = 'DreamLeague Season 11';

insert into teams (name, tag, region) values ('Virtus.pro', 'VP', 'CIS');
insert into teams (name, tag, region) values ('Evil Genius', 'EG', 'America');
insert into teams (name, tag, region) values ('Team Secret', 'Secret', 'Europa');
insert into teams (name, tag, region) values ('J.Storm', 'J.Storm', 'America');

-- J.Storm roster
insert into players (name, nick, team_id, position, country_id, price)
    select 'Park Tae-won', 'Match', id, 2, 1, 10 from teams where tag = 'J.Storm';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Clinton Loomis', 'Fear', id, 2, 1, 10 from teams where tag = 'J.Storm';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Lee Sang-don', 'FoReV', id, 1, 1, 10 from teams where tag = 'J.Storm';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Jonathan De Guia', 'Bryle', id, 1, 1, 10 from teams where tag = 'J.Storm';
insert into players (name, nick, team_id, position, country_id, price)
    select 'David Hull', 'Moo', id, 1, 1, 10 from teams where tag = 'J.Storm';

-- Secret roster
insert into players (name, nick, team_id, position, country_id, price)
    select 'Clement Ivanov', 'Puppey', id, 2, 2, 10 from teams where tag = 'Secret';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Yazied Jaradat', 'YapzOr', id, 2, 2, 10 from teams where tag = 'Secret';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Ludwig Wåhlberg', 'zai', id, 1, 2, 10 from teams where tag = 'Secret';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Yeik Nai Zheng', 'MidOne', id, 1, 2, 10 from teams where tag = 'Secret';
insert into players (name, nick, team_id, position, country_id, price)
    select 'Michał Jankowski', 'Nisha', id, 1, 2, 10 from teams where tag = 'Secret';

-- series of round 1
insert into series (home_team_id, guest_team_id, time, round_id, max_games, stage)
select
    (select id from teams where tag = 'J.Storm') as home_team_id,
    (select id from teams where tag = 'Secret') as guest_team_id,
    '2019-03-20 11:00:00' as time,
    id as round_id,
    3 as max_games,
    'Playoff' as stage
from rounds where from_date = '2019-03-20 00:00:00';

insert into series (home_team_id, guest_team_id, time, round_id, max_games, stage)
select
    (select id from teams where tag = 'VP') as home_team_id,
    (select id from teams where tag = 'EG') as guest_team_id,
    '2019-03-20 15:00:00' as time,
    id as round_id,
    3 as max_games,
    'Playoff' as stage
from rounds where from_date = '2019-03-20 00:00:00';

-- matches of J.Storm vs Secret series
insert into matches (series_id, date, left_team_id, right_team_id, win_left)
select
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    '2019-03-20 11:05:00' as date,
    (select id from teams where tag = 'J.Storm') as left_team_id,
    id as righ_team_id,
    false as win_left
from teams where tag = 'Secret';

/*insert into matches (series_id, date, left_team_id, right_team_id, win_left)
select
    (select id from series where time = '2019-03-20 12:10:00') as series_id,
    '2019-03-20 11:05:00' as date,
    (select id from teams where tag = 'Secret') as left_team_id,
    id as righ_team_id,
    true as win_left
from teams where tag = 'J.Storm';*/

-- match points J.Storm team
insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'Match') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'J.Storm') as team_id,
    '{"0": 4,"1":  2, "2": 1, "3":  -1, "4":  1, "5": 1, "6":  0, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'Fear') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'J.Storm') as team_id,
    '{"0": 6, "1":  3, "2": 2, "3":  -2, "4":  0, "5": 2, "6":  0, "7": 1, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 1.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'FoReV') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'J.Storm') as team_id,
    '{"0": 4, "1":  0, "2": 1, "3":  -1, "4":  2, "5": 1, "6":  1, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'Bryle') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'J.Storm') as team_id,
    '{"0": 5, "1":  2, "2": 1, "3":  -2, "4":  1, "5": 3, "6":  0, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 3.0, "3":  4.0, "4":  349.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'Moo') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'J.Storm') as team_id,
    '{"0": 6,"1":  4, "2": 1, "3":  -1, "4":  1, "5": 1, "6":  0, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  9.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

-- match points Secret team
insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'Nisha') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'Secret') as team_id,
    '{"0": 4,"1":  2, "2": 1, "3":  -1, "4":  1, "5": 1, "6":  0, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'MidOne') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'Secret') as team_id,
    '{"0": 5,"1":  3, "2": 2, "3":  -2, "4":  0, "5": 2, "6":  0, "7": 1, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 1.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'zai') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'Secret') as team_id,
    '{"0": 4,"1":  0, "2": 1, "3":  -1, "4":  2, "5": 1, "6":  1, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'YapzOr') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'Secret') as team_id,
    '{"0": 5, "1":  2, "2": 1, "3":  -2, "4":  1, "5": 3, "6":  0, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  4.0, "2": 3.0, "3":  4.0, "4":  349.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into points (player_id, match_id, series_id, team_id, points, raw_data, round_id)
select
    (select id from players where nick = 'Puppey') as player_id,
    (select id from matches where date = '2019-03-20 11:05:00') as match_id,
    (select id from series where time = '2019-03-20 11:00:00') as series_id,
    (select id from teams where tag = 'Secret') as team_id,
    '{"0": 6, "1":  4, "2": 1, "3":  -1, "4":  1, "5": 1, "6":  0, "7": 0, "8": 0}'::jsonb as points,
    '{"1":  9.0, "2": 7.0, "3":  4.0, "4":  342.0, "5": 18000.0, "6":  0.0, "7": 0.0, "8": 3.2}'::jsonb as raw_data,
    id as round_id
from rounds where from_date = '2019-03-20 00:00:00';

insert into aggregation_points (player_id, points) select player_id, points from points;

insert into leagues (name, started, type) select 'Overall' as name, id as started, 'System' as type from rounds where from_date = '2019-03-20 00:00:00';

insert into rounds (from_date, to_date, tournament_id) select '2020-04-22 00:00:00', '2020-04-22 23:00:00', id from tournaments where name = 'DreamLeague Season 11';
insert into series (home_team_id, guest_team_id, time, round_id, max_games, stage)
select
    (select id from teams where tag = 'J.Storm') as home_team_id,
    (select id from teams where tag = 'Secret') as guest_team_id,
    '2020-04-22 11:00:00' as time,
    id as round_id,
    3 as max_games,
    'Playoff' as stage
from rounds where from_date = '2020-04-22 00:00:00';

-- pass lol123QW
insert into accounts (email, password, name, language, EULA, country_id)
values ('test@test.ru', 'e106495429b21896dbad14ea236228c39cc86f3cf4eb1baeab902eb33e330b53', 'Jin?', 'RU', true, 1);

insert into rosters (id, account_id, available_money, players, captain, name, fan_team_id)
select
       (select id from accounts where name = 'Jin?') as id,
       (select id from accounts where name = 'Jin?') as account_id,
       50.0 as available_money,
       '{1,2,3,8,9}' as players,
       2 as captain,
       'DreamTeam' as name,
       id as fan_team_id from teams where tag = 'J.Storm';

insert into rosters_players (roster_id, player_id)
select id as roster_id, 1 as player_id from rosters where name = 'DreamTeam';

insert into rosters_players (roster_id, player_id)
select id as roster_id, 2 as player_id from rosters where name = 'DreamTeam';

insert into rosters_players (roster_id, player_id)
select id as roster_id, 3 as player_id from rosters where name = 'DreamTeam';

insert into rosters_players (roster_id, player_id)
select id as roster_id, 8 as player_id from rosters where name = 'DreamTeam';

insert into rosters_players (roster_id, player_id)
select id as roster_id, 9 as player_id from rosters where name = 'DreamTeam';

insert into league_members (league_id, roster_id)
select id as league_id,
    (select id as roster_id from rosters where name = 'DreamTeam')
from leagues where name = 'Overall';

insert into leagues_table (roster_id, league_id, places, points, total_points)
select id as roster_id,
       (select id as league_id from leagues where name = 'Overall'),
       '{"1":  1, "2":  1}'::json as places,
       '{"1":  10, "2":  10}'::json as points,
       20 as total_points
from rosters where name = 'DreamTeam';

insert into roster_histories (roster_id, round_id, players, captain, points, teams)
select id as roster_id,
       (select id as round_id from rounds where from_date = '2019-03-20 00:00:00'),
       '{4,5,7,8, 10}' as players,
       8 as captain,
       '{"4": 2, "5": 1, "7": 1, "8": 4, "10": 2}'::json as points,
       '{"4": 4, "5": 4, "7": 3, "8": 3, "10": 3}'::json as teams
from rosters where name = 'DreamTeam';

insert into roster_histories (roster_id, round_id, players, captain, points, teams)
select id as roster_id,
       (select id as round_id from rounds where from_date = '2019-03-22 00:00:00'),
       '{4,5,7,8, 3}' as players,
       3 as captain,
       '{"4": 1, "5": 1, "7": 2, "8": 0, "3": 6}'::json as points,
       '{"4": 4, "5": 4, "7": 3, "8": 3, "3": 4}'::json as teams
from rosters where name = 'DreamTeam';
