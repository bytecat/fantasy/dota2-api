insert into rounds (from_date, to_date, tournament_id)
select
    '2019-04-24 05:00:00' as from_date,
    '2019-04-24 19:00:00' as to_date,
    id as tournament_id
from tournaments where start_date = '2019-04-16';

insert into leagues (name, started, type) select 'Overall' as name, id as started, 'System' as type from rounds where from_date = '2019-04-24 05:00:00';
insert into leagues (name, started, type) select 'Round 1' as name, id as started, 'System' as type from rounds where from_date = '2019-04-24 05:00:00';


insert into series (home_team_id, guest_team_id, time, round_id, max_games, stage)
select
    (select id from teams where tag = 'KG') as home_team_id,
    (select id from teams where tag = 'ThePango') as guest_team_id,
    '2019-04-20 06:00:00' as time,
    id as round_id,
    3 as max_games,
    'PlayOff' as stage
from rounds where from_date = '2019-04-20 05:00:00';



insert into leagues (name, started, type) select 'Overall' as name, id as started, 'System' as type from rounds where from_date = '2019-04-16 06:00:00';
insert into leagues (name, started, type) select 'Round 1' as name, id as started, 'System' as type from rounds where from_date = '2019-04-16 06:00:00';

insert into rounds (from_date, to_date, tournament_id)
select
    '2019-04-21 05:00:00' as from_date,
    '2019-04-21 19:00:00' as to_date,
    id as tournament_id
from tournaments where start_date = '2019-04-16';

insert into series (home_team_id, guest_team_id, time, round_id, max_games, stage)
select
    (select id from teams where tag = 'Na`Vi') as home_team_id,
    (select id from teams where tag = 'KG') as guest_team_id,
    '2019-04-21 06:00:00' as time,
    id as round_id,
    3 as max_games,
    'PlayOff' as stage
from rounds where from_date = '2019-04-24 05:00:00';

insert into series (home_team_id, guest_team_id, time, round_id, max_games, stage)
select
    (select id from teams where tag = 'Mski') as home_team_id,
    (select id from teams where tag = 'KG') as guest_team_id,
    '2019-04-21 10:30:00' as time,
    id as round_id,
    3 as max_games,
    'PlayOff' as stage
from rounds where from_date = '2019-04-24 05:00:00';

update rounds set from_date = '2019-04-21 05:00:00', to_date = '2019-04-21 16:00:00';

insert into tournaments (name, start_date, end_date, country_id, logo)
select
    'OGA Dota PIT Minor 2019' as name,
    '2019-04-22' as start_date,
    '2019-04-28' as end_date,
    id as coutry_id,
    null as logo
from countries where code = 'HR';

insert into rounds (from_date, to_date, tournament_id)
select
    '2019-04-22 05:00:00' as from_date,
    '2019-04-22 22:00:00' as to_date,
    id as tournament_id
from tournaments where start_date = '2019-04-22';