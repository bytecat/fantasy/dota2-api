package fantasy.typeclass

import cats.{Functor, Monad}
import cats.mtl.ApplicativeAsk
import tofu.Context
import tofu.optics.Extract

trait Contextual[F[_], Ctx] {
  def ask: F[Ctx]
  def askF[A](f: Ctx => F[A])(implicit M: Monad[F]): F[A] = M.flatten(ctx(f))
  def ctx[A](f: Ctx => A): F[A]
}

object Contextual {
  implicit def `FromApplicativeAsk`[F[_], Ctx](implicit Ask: ApplicativeAsk[F, Ctx]): Contextual[F, Ctx] =
    new Contextual[F, Ctx] {
      override def ctx[A](f: Ctx => A): F[A] = Ask.reader(f)

      override def ask: F[Ctx] = Ask.ask
    }

  implicit def `FromTofu`[F[_], Ctx](implicit C: Context.Aux[F, Ctx]): Contextual[F, Ctx] = new Contextual[F, Ctx] {
    override def ask: F[Ctx] = C.context

    override def ctx[A](f: Ctx => A): F[A] = C.ask(f)
  }

  def apply[F[_], Ctx](implicit ev: Contextual[F, Ctx]): Contextual[F, Ctx] = ev

  implicit def functor[F[_]]: Functor[Contextual[F, *]] = new Functor[Contextual[F, *]] {
    override def map[A, B](fa: Contextual[F, A])(f: A => B): Contextual[F, B] = new Contextual[F, B] {
      override def ask: F[B] = fa.ctx(f)

      override def ctx[G](f2: B => G): F[G] = fa.ctx(f andThen f2)
    }
  }

  implicit def subContext[F[_], Ctx, SubCtx](
      implicit C: Contextual[F, Ctx],
      E: Ctx Extract SubCtx
  ): Contextual[F, SubCtx] = new Contextual[F, SubCtx] {
    override def ask: F[SubCtx] = C.ctx(E.extract)

    override def ctx[A](f: SubCtx => A): F[A] = C.ctx(c => f(E.extract(c)))
  }
}
