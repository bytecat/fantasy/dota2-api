package fantasy.typeclass
import cats.{Applicative, ApplicativeError}

import scala.language.higherKinds

trait Raise[F[_], -E] {
  def raise[A](err: E): F[A]
}

object Raise {
  implicit def raiseApplicativeError[F[_], E, E1](
      implicit appErr: ApplicativeError[F, E],
      sub: E1 <:< E
  ): Raise[F, E1] =
    new Raise[F, E1] {
      override def raise[A](err: E1): F[A] = appErr.raiseError(err)
    }

  trait ToRaiseOps {
    implicit def toRaiseOps[F[_], E](err: E)(implicit R: Raise[F, E]): syntax.RaiseOps[E] = new syntax.RaiseOps[E](err)
    implicit def toRaiseOptionOps[A](opt: Option[A])                                      = new syntax.RaiseOptionOps[A](opt)
    implicit def toRaiseEither[A, E](either: Either[E, A])                                = new syntax.RaiseEither[A, E](either)
  }

  object syntax {
    final implicit class RaiseOps[E](val err: E) extends AnyVal {
      def raise[F[_], A](implicit raise: Raise[F, E]): F[A] = raise.raise(err)
    }

    final implicit class RaiseOptionOps[A](val opt: Option[A]) extends AnyVal {
      def liftTo[F[_]] = new RaiseLiftToApplied[F, A](opt)
    }

    final implicit class RaiseEither[A, E](val either: Either[E, A]) extends AnyVal {
      def liftTo[F[_]](implicit raise: Raise[F, E], app: Applicative[F]): F[A] = either match {
        case Left(err) => err.raise
        case Right(v)  => app.pure(v)
      }
    }
  }

  class RaiseLiftToApplied[F[_], A](val opt: Option[A]) extends AnyVal {
    def apply[E](err: => E)(implicit raise: Raise[F, E], app: Applicative[F]): F[A] =
      opt match {
        case None    => raise.raise(err)
        case Some(a) => app.pure(a)
      }
  }
}
