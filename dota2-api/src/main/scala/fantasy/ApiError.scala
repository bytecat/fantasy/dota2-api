package fantasy

import cats.data.NonEmptyList
import fantasy.Error.PlainError
import fantasy.context.TraceId
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import ru.tinkoff.tschema.swagger.SwaggerTypeable

import scala.util.control.NoStackTrace

trait ApiError extends Throwable {
  def code: Int
}

trait DetailedApiError extends ApiError {
  def messageCode: String
}

object ApiError {

  final case class ApiFieldError(field: String, messageCode: String)

  final case class ApiValidationError(errors: NonEmptyList[ApiFieldError]) extends ApiError {
    override def code: Int = 400
  }

  object ApiValidationError {
    def apply(errors: NonEmptyList[ApiFieldError]): ApiValidationError = new ApiValidationError(errors)

    def apply(single: ApiFieldError): ApiValidationError = new ApiValidationError(NonEmptyList.of(single))
  }

  case object BadRequest extends ApiError {
    override def code: Int = 400
  }

  case object Forbidden extends ApiError {
    override def code: Int = 403
  }

  case object NotFound extends ApiError {
    override def code: Int = 404
  }

  case class InternalError(ex: Throwable) extends ApiError {
    override def code: Int = 500
  }

  case object NotImplemented extends ApiError {
    override def code: Int = 501
  }

  abstract class ThrowableDetailedApiError(val code: Int, message: String, val messageCode: String)
      extends Throwable(message) with NoStackTrace with DetailedApiError

  abstract class ThrowableApiError(val code: Int, message: String)
      extends Throwable(message) with NoStackTrace with ApiError
}

@ConfiguredJsonCodec
sealed trait Error {
  def msg: String
}

object Error {
  implicit val config: Configuration = Configuration.default.withDiscriminator("type")

  final case class FieldError(field: String, msg: String) extends Error
  final case class PlainError(msg: String)                extends Error

  implicit val few: SwaggerTypeable[FieldError] = SwaggerTypeable.genTypeable
  implicit val fep: SwaggerTypeable[PlainError] = SwaggerTypeable.genTypeable
  implicit def swagger: SwaggerTypeable[Error]  = SwaggerTypeable.genTypeable
}

@derive(encoder)
final case class ErrorResponse(errors: NonEmptyList[Error], traceId: TraceId)
object ErrorResponse {
  def plain(err: String, traceId: TraceId): ErrorResponse = ErrorResponse(NonEmptyList.of(PlainError(err)), traceId)
  import fantasy.util.swagger.data.Implicits._

  implicit val swagger: SwaggerTypeable[ErrorResponse] = SwaggerTypeable.genTypeable
}
