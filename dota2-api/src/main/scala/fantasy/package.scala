import cats.{ApplicativeError, MonadError}
import doobie.free.connection.ConnectionIO
import fantasy.context.ProcessContext
import fantasy.typeclass.{Contextual, Raise}
import fantasy.util.i18n.Language
import tofu.env.{Env, EnvSpecializedFunctions}
import ru.tinkoff.tschema.finagle.routing._
import tofu.lift.Lift

package object fantasy {

  type FantasyTask[A] = Env[ProcessContext, A]
  type HasLang[F[_]]  = Contextual[F, Language]

  type LiftCIO[F[_]]          = Lift[ConnectionIO, F]
  type LiftStream[I[_], F[_]] = Lift[fs2.Stream[I, *], fs2.Stream[F, *]]

  type ApiErrorRaise[F[_]] = Raise[F, ApiError]
  type ApiErrorMonad[F[_]] = MonadError[F, ApiError]

  type ThrowApplicative[F[_]] = ApplicativeError[F, Throwable]
  type ThrowMonad[F[_]]       = MonadError[F, Throwable]

  type IdCompiler[F[_]] = fs2.Stream.Compiler[F, F]

  type Http[+A] = EnvRouting.EnvHttp[ProcessContext, A]

  object FantasyTask extends EnvSpecializedFunctions[ProcessContext]
  object Http        extends EnvSpecializedFunctions[EnvRouting[ProcessContext]]
}
