package fantasy.context

import cats.syntax.semigroup._
import fantasy.domain.account.AccountId
import fantasy.util.i18n.Language
import tofu.logging.{DictLoggable, HideLoggable, LogRenderer, Loggable}
import tofu.optics.Extract
import tofu.syntax.logRenderer._

final case class ProcessContext(traceId: TraceId, language: Language, user: Option[AccountId])
object ProcessContext {
  def create(traceId: TraceId, lang: Language): ProcessContext = ProcessContext(traceId, lang, None)

  def create(lang: Language): ProcessContext = ProcessContext(TraceId.create, lang, None)

  implicit val loggable: Loggable[ProcessContext] = new HideLoggable[ProcessContext] with DictLoggable[ProcessContext] {

    override def fields[I, V, R, S](a: ProcessContext, i: I)(implicit r: LogRenderer[I, V, R, S]): R =
      i.addString("traceId", a.traceId.id) |+| a.user.fold(i.noop)(a => i.addInt("user", a.value))
  }

  implicit def `ExtractLang`: Extract[ProcessContext, Language] = _.language

}
