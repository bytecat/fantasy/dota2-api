package fantasy.context

import cats.effect.Sync
import com.typesafe.config.ConfigFactory
import fantasy.Profile
import pureconfig._
import pureconfig.generic.auto._

final case class CoreConfig(
    http: HttpConfig,
    db: DBConfig,
    game: GameRules,
    odota: Odota,
    sentry: Sentry,
    secret: String
)
final case class HttpConfig(host: String, port: Int, errorsBundle: String, messagesBundle: String)
final case class DBConfig(
    driver: String,
    url: String,
    user: String,
    password: String,
    maxConnects: Int
)
final case class GameRules(maxPlayersFromTeam: Int, startMoney: Double)
final case class Odota(service: String, token: String)
final case class Sentry(dsn: String)

object CoreConfig {
  def load[F[_]: Sync](profile: Profile): F[CoreConfig] = Sync[F].delay {
    val root  = ConfigFactory.load("application.conf")
    val infra = ConfigFactory.load(s"infra-${profile.toString.toLowerCase}.conf")
    loadConfigOrThrow[CoreConfig](root.withFallback(infra))
  }
}
