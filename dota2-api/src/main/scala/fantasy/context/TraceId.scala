package fantasy.context

import java.util.UUID

import io.circe.{Decoder, Encoder}
import ru.tinkoff.tschema.swagger.{SwaggerPrimitive, SwaggerTypeable}
import tofu.logging.Loggable

final case class TraceId(id: String) extends AnyVal
object TraceId extends (String => TraceId) {
  def create: TraceId = TraceId(UUID.randomUUID().toString.replaceAll("-", ""))

  implicit val encoder: Encoder[TraceId]         = Encoder.encodeString.contramap(_.id)
  implicit val decoder: Decoder[TraceId]         = Decoder.decodeString.map(TraceId)
  implicit val loggable: Loggable[TraceId]       = Loggable.stringValue.contramap(_.id)
  implicit val swagger: SwaggerTypeable[TraceId] = SwaggerTypeable.make(SwaggerPrimitive.uuid)
}
