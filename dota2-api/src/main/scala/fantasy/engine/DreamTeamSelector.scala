package fantasy.engine

import cats.data.{NonEmptyList, NonEmptyMap, NonEmptySet}
import cats.~>
import fantasy.domain.`match`.{CalculatedPointValue, MatchPoints, Point}
import fantasy.domain.player.{Player, PlayerId}

trait DreamTeamSelector[F[_], P <: Point, Pos] {
  def select(
      points: NonEmptyList[MatchPoints[P]],
      players: NonEmptySet[Player[Pos]]
  ): F[NonEmptyMap[PlayerId, CalculatedPointValue]]
}

object DreamTeamSelector {

  def apply[F[_], P <: Point, Pos](implicit drs: DreamTeamSelector[F, P, Pos]): DreamTeamSelector[F, P, Pos] = drs

  def mapK[I[_], O[_], P <: Point, Pos](dts: DreamTeamSelector[I, P, Pos])(fk: I ~> O): DreamTeamSelector[O, P, Pos] =
    (points: NonEmptyList[MatchPoints[P]], players: NonEmptySet[Player[Pos]]) => fk(dts.select(points, players))
}
