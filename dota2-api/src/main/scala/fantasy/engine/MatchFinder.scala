package fantasy.engine

import java.time.LocalDateTime

import cats.data.NonEmptyList
import fantasy.domain.`match`.RawPointValue
import fantasy.domain.player.PlayerId
import fantasy.domain.series.{RoundId, SeriesId}
import fantasy.domain.team.TeamId
import fantasy.dota2.engine.ExternalData.ExternalMatchId
import fantasy.engine.MatchFinder.MatchInfo

trait MatchFinder[F[_], T] {
  def lookup(round: RoundId): F[List[MatchInfo[T, RawPointValue]]]

  def lookup(round: RoundId, series: Map[SeriesId, List[ExternalMatchId]]): F[List[MatchInfo[T, RawPointValue]]]
}

object MatchFinder {

  def apply[F[_], P](implicit MF: MatchFinder[F, P]): MatchFinder[F, P] = MF

  case class PlayerMatchInfo[T, V](playerId: PlayerId, teamId: TeamId, points: Map[T, V])
  case class MatchInfo[T, V](
      rawMatchId: ExternalMatchId,
      home: TeamId,
      guest: TeamId,
      start: LocalDateTime,
      end: LocalDateTime,
      duration: Long,
      winHome: Boolean,
      seriesId: SeriesId,
      players: NonEmptyList[PlayerMatchInfo[T, V]]
  )
}
