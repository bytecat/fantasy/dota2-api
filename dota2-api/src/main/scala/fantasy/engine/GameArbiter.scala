package fantasy.engine

import cats.data.NonEmptyList
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.foldable._
import cats.syntax.functor._
import cats.syntax.option._
import cats.syntax.order._
import cats.syntax.traverse._
import cats.tagless.FunctorK
import cats.Functor
import fantasy.domain.`match`._
import fantasy.domain.dreamteam.{DreamTeam, DreamTeamRepository}
import fantasy.domain.league._
import fantasy.domain.player.PlayerRepository
import fantasy.domain.roster._
import fantasy.domain.series._
import fantasy.dota2.boot.{Repositories, Services}
import fantasy.dota2.engine.ExternalData
import fantasy.dota2.engine.ExternalData.ExternalMatchId
import fantasy.engine.MatchFinder.MatchInfo
import fantasy.{IdCompiler, ThrowMonad}
import simulacrum.typeclass
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

import scala.Numeric.Implicits._

class GameArbiter[P, PP <: Point: TotalPoints, F[_]: ThrowMonad: ArbiterData: PointCalculus[*[_], PP, PP]: Logging: DreamTeamSelector[
  *[_],
  PP,
  P
]: IdCompiler: MatchFinder[*[_], PP]: ExternalData](
    services: Services[F, P, PP],
    repositories: Repositories[F, P, PP]
) {
  private implicit val s: SeriesService[F]                   = services.series
  private implicit val r: RoundService[F]                    = services.round
  private implicit val m: MatchService[F]                    = services.matchService
  private implicit val mp: MatchPointService[F, PP]          = services.matchPoints
  private implicit val lm: LeagueMembers[F]                  = repositories.members
  private implicit val l: LeagueRepository[F]                = repositories.leagues
  private implicit val lt: LeagueTableRepository[F]          = repositories.leagueTable
  private implicit val p: PlayerRepository[F, P, PP]         = repositories.players
  private implicit val rr: RostersRepository[F]              = repositories.rosters
  private implicit val history: RosterHistoriesRepository[F] = repositories.history
  private implicit val dr: DreamTeamRepository[F]            = repositories.dreamTeam

  def run(): F[Unit] =
    for {
      _ <- info"start arbiter work"
      _ <- saveRostersToHistory.handleErrorWith("failed save rosters to history ".cause[F])
      _ <- createNextRoundLeague.handleErrorWith("failed next round league ".cause[F])
      calculatedRounds <- calculateRound.handleErrorWith(
                           err => "failed calculate round ".cause[F](err).map(_ => List())
                         )
      _ <- updateRostersHistories(calculatedRounds).handleErrorWith("failed update roster histories ".cause[F])
      _ <- updateLeagueTables(calculatedRounds).handleErrorWith("failed update tables ".cause[F])
      _ <- info"end arbiter work"
    } yield ()

  def calculate(matches: List[(SeriesId, ExternalMatchId)]): F[Unit] =
    for {
      _              <- info"start calculate matches ${matches.map(t => s"${t._1} -> ${t._2}").mkString(", ")}"
      seriesMatches  = matches.groupBy(_._1).mapValues(_.map(_._2))
      series         <- seriesMatches.keys.toList.traverse(SeriesService[F].get)
      groupedByRound = series.groupBy(_.round)
      _ <- groupedByRound
            .mapValues(_.map(s => s.id -> seriesMatches.getOrElse(s.id, List())).toMap)
            .toList
            .flatTraverse(t => MatchFinder[F, PP].lookup(t._1, t._2))
            .flatMap(_.traverse(saveMatch))
      _ <- updateRostersHistories(groupedByRound.keys.toList)
      _ <- updateLeagueTables(groupedByRound.keys.toList)
      _ <- info"end calculate matches"
    } yield ()

  private def saveRostersToHistory: F[Unit] =
    for {
      currentRound <- RoundService[F].current()
      last         <- ArbiterData[F].lastStartedRound
      _ <- RostersRepository[F].all.compile.toList
            .flatMap(_.traverse(r => saveRoster(r, currentRound.number)))
            .unlessA(last.contains(currentRound.number))
      _ <- ArbiterData[F].saveStarted(currentRound.number).unlessA(last.contains(currentRound.number))
    } yield ()

  private def createNextRoundLeague: F[Unit] =
    for {
      nextRound <- RoundService[F].nextRound()
      leagues   <- LeagueRepository[F].byType(LeagueType.System)
      has       = nextRound.forall(r => leagues.exists(l => l.name == League.roundLeague(r.number)))
      res <- nextRound
              .map(
                r =>
                  LeagueRepository[F]
                    .create(League.roundLeague(r.number), r.number, LeagueType.System, None)
                    .unlessA(has)
              )
              .getOrElse(().pure[F])
    } yield res

  private def saveRoster(r: Roster, round: RoundId): F[Unit] =
    for {
      players <- r.players.traverse(PlayerRepository[F, P, PP].get(_))
      rh = RosterHistory(
        r.id,
        round,
        r.players,
        r.captain,
        r.players.map(_ -> CalculatedPointValue(0)).toNem,
        CalculatedPointValue(0),
        players.map(_.flatMap(p => p.teamId.map(p.id -> _))).collect { case Some(v) => v }.toMap
      )
      res <- RosterHistoriesRepository[F].create(rh)
    } yield res

  private def calculateRound: F[List[RoundId]] =
    for {
      lastCompleted  <- RoundService[F].lastCompleted
      lastCalculated <- ArbiterData[F].lastCompletedRound
      need = lastCalculated
        .map(r => lastCompleted.exists(r2 => r2.number > r))
        .getOrElse(lastCompleted.isDefined)
      r1    = lastCalculated.map(_.value).getOrElse(0L)
      r2    = lastCompleted.map(_.number.value).getOrElse(0L)
      range = (Math.min(r1, r2) to Math.max(r1, r2)).filter(i => lastCalculated.forall(_.value < i))
      _     <- info"start calculate ${range.mkString(", ")} rounds"
      _ <- range.toList
            .flatTraverse(rid => MatchFinder[F, PP].lookup(RoundId(rid)))
            .flatMap(data => data.traverse(saveMatch))
            .whenA(need)
      _   <- ArbiterData[F].saveLastCompeted(RoundId(Math.max(r1, r2))).whenA(need)
      res = if (need) range.map(RoundId).toList else List()
    } yield res

  private def saveMatch(d: MatchInfo[PP, RawPointValue]): F[Unit] =
    SeriesService[F].get(d.seriesId).flatMap { series =>
      MatchService[F]
        .create(d.seriesId, d.start, d.home, d.guest, d.winHome)
        .flatMap { id =>
          PointCalculus[F, PP, PP]
            .calculate(d)
            .flatMap(
              points =>
                d.players.traverse { p =>
                  val playerPoints =
                    points.players.find(_.playerId === p.playerId).map(_.points).getOrElse(Map.empty)
                  MatchPointService[F, PP]
                    .create(id, p.playerId, p.teamId, playerPoints, p.points) >> repositories.points
                    .saveRound(series.round, p.playerId, playerPoints)
                }
            ) >> ExternalData[F].saveLink(d.rawMatchId, id)
        }
    }

  private def updateRostersHistories(rounds: List[RoundId]): F[Unit] =
    rounds.traverse { round =>
      for {
        series <- SeriesService[F].getByRound(round)
        matches <- series.flatTraverse(
                    s =>
                      MatchService[F]
                        .getBySeries(s.id)
                        .map(_.toList)
                        .handleErrorWith(err => s"failed get matches for ${s.id} series".cause[F](err).map(_ => List()))
                  )
        points <- matches.flatTraverse(m => MatchPointService[F, PP].findByMatch(m.id).map(_.toList))
        histories <- RostersRepository[F].all
                      .evalMap(r => RosterHistoriesRepository[F].get(r.id, round))
                      .collect { case Some(v) => v }
                      .compile
                      .toList
        res = histories.map(calculatePoints(_, points))
        _   <- res.traverse(RosterHistoriesRepository[F].update)
        average = if (res.nonEmpty)
          CalculatedPointValue(res.foldMap(_.total).value / BigDecimal(histories.size)).some
        else none
        top = res.filter(_.total > CalculatedPointValue(0)).sortBy(_.total).lastOption
        _   <- info"average points: $average, top: ${top.map(_.total)}, top roster: ${top.map(_.id)}"
        _ <- (average, top)
              .mapN((avg, top) => RoundPoints(avg, top.total, top.id))
              .traverse(rp => RoundService[F].get(round).flatMap(r => services.round.update(r.copy(points = rp.some))))
        _ <- saveDreamTeam(round, points)
      } yield ()
    }.void

  private def updateLeagueTables(rounds: List[RoundId]): F[Unit] =
    rounds
      .sortBy(_.value)
      .traverse { round =>
        for {
          rosters <- RostersRepository[F].all
                      .evalMap(r => RosterHistoriesRepository[F].get(r.id, round))
                      .collect { case Some(value) => value }
                      .compile
                      .toList
          rosterLeagues <- rosters
                            .traverse(
                              r => LeagueMembers[F].consists(r.id).map(_.filter(m => m.from <= round)).map(r -> _)
                            )
                            .map(_.toMap)
          rosterTotal = rosters.map(r => r.id -> r.total).toMap
          leagues     <- rosterLeagues.flatMap(_._2.map(_.league)).toList.traverse(LeagueRepository[F].get)
          _ <- leagues.flatten
                .filter(_.started <= round)
                .map(_.id)
                .traverse(league => updateLeagueTable(league, rosterTotal, round))
        } yield ()
      }
      .void

  private def calculatePoints(rh: RosterHistory, points: List[MatchPoints[PP]]): RosterHistory = {
    val players =
      rh.players
        .map(p => p -> points.filter(_.playerId === p).foldMap(m => TotalPoints[PP].total(m.points)))
        .map {
          case (p, pts) => if (p === rh.captain) p -> CalculatedPointValue(pts.value * 2) else p -> pts
        }
        .toNem

    val total = players.reduce match {
      case x if x < CalculatedPointValue(0) => CalculatedPointValue(0)
      case x                                => x
    }

    rh.copy(points = players, total = total)
  }

  private def updateLeagueTable(id: LeagueId, rosterTotal: Map[RosterId, CalculatedPointValue], round: RoundId) =
    for {
      table <- LeagueTableRepository[F].all(id)
      updatedPoints = table.map { t =>
        val roundPts = rosterTotal.getOrElse(t.roster, CalculatedPointValue(0))
        t.copy(points = t.points + (round -> roundPts))
          .copy(totalPoints = t.totalPoints - t.points.getOrElse(round, CalculatedPointValue(0)) + roundPts)
      }
      positions = updatedPoints
        .map(t => t.roster -> t.totalPoints)
        .groupBy(_._2)
        .toList
        .sortBy(-_._1)
        .foldl(List.empty[(RosterId, Int)]) {
          case (prev, (_, players)) => prev ++ players.map(_._1 -> (prev.size + 1))
        }
        .toMap
      res = updatedPoints.map { t =>
        positions.get(t.roster).map(pos => t.copy(places = t.places + (round -> pos.toLong))).getOrElse(t)
      }
      _ <- res.traverse(LeagueTableRepository[F].update)
    } yield ()

  private def saveDreamTeam(r: RoundId, points: List[MatchPoints[PP]]): F[Unit] =
    NonEmptyList
      .fromList(points)
      .traverse { pts =>
        pts
          .map(_.playerId)
          .traverse(p => repositories.players.get(p))
          .map(_.collect { case Some(v) => v })
          .flatMap(
            l =>
              NonEmptyList
                .fromList(l)
                .map(_.toNes)
                .traverse(players => DreamTeamSelector[F, PP, P].select(pts, players))
          )
          .flatMap(_.traverse { selected =>
            val teams = selected.keys.toList
              .map(p => p -> points.find(_.playerId === p).map(_.teamId))
              .collect {
                case (p, Some(t)) => p -> t
              }
              .toMap
            val roster = DreamTeam(r, selected.keys.toNonEmptyList, selected, teams)
            DreamTeamRepository[F]
              .get(r)
              .flatMap(_.fold(DreamTeamRepository[F].create(roster))(_ => DreamTeamRepository[F].update(roster)))
          })
      }
      .void
      .onError {
        case err => warn"failed build dream team ${err.getMessage}"
      }
}

object GameArbiter {

  def create[I[_]: Functor, P, PP <: Point: TotalPoints, F[_]: ThrowMonad: ArbiterData: PointCalculus[
    *[_],
    PP,
    PP
  ]: IdCompiler: MatchFinder[*[_], PP]: DreamTeamSelector[*[_], PP, P]: ExternalData: Logs[
    I,
    *[_]
  ]](services: Services[F, P, PP], repositories: Repositories[F, P, PP]): I[GameArbiter[P, PP, F]] =
    Logs[I, F].forService[GameArbiter[P, PP, F]].map(implicit l => new GameArbiter[P, PP, F](services, repositories))

}

@typeclass
trait ArbiterData[F[_]] {
  def lastStartedRound: F[Option[RoundId]]
  def saveStarted(r: RoundId): F[Unit]
  def lastCompletedRound: F[Option[RoundId]]
  def saveLastCompeted(r: RoundId): F[Unit]
}

object ArbiterData {
  implicit val functorK: FunctorK[ArbiterData] = cats.tagless.Derive.functorK
}
