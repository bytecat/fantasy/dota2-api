package fantasy.engine

import fantasy.domain.`match`.{CalculatedPointValue, RawPointValue}
import fantasy.engine.MatchFinder.MatchInfo

trait PointCalculus[F[_], P, R] {
  def calculate(info: MatchInfo[P, RawPointValue]): F[MatchInfo[R, CalculatedPointValue]]
}

object PointCalculus {
  def apply[F[_], P, R](implicit PC: PointCalculus[F, P, R]): PointCalculus[F, P, R] = PC
}
