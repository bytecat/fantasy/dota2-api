package fantasy.util
import java.time.format.DateTimeFormatter

object DateUtil {
  val LocalDateTimeFormat: DateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
}
