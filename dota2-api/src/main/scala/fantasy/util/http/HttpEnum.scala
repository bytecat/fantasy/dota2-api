package fantasy.util.http

import cats.Order
import cats.instances.string._
import enumeratum.{CirceEnum, EnumEntry}
import ru.tinkoff.tschema.param.HttpParam
import ru.tinkoff.tschema.swagger.SwaggerTypeable.SwaggerTypeableEnum

trait HttpEnum[E <: EnumEntry]
    extends enumeratum.Enum[E] with CirceEnum[E] with SwaggerTypeableEnum[E] with HttpParam.Enum[E] {
  implicit val ord: Order[E] = Order.by(_.entryName)
}
