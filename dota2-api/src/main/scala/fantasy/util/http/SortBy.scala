package fantasy.util.http

import enumeratum.EnumEntry

import scala.collection.immutable

final case class SortBy[B](by: B, direction: Direction)

sealed trait Direction extends EnumEntry
object Direction extends HttpEnum[Direction] {
  override val values: immutable.IndexedSeq[Direction] = findValues

  case object ASC  extends Direction
  case object DESC extends Direction

}
