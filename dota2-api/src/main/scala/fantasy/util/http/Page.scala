package fantasy.util.http

import cats.{Applicative, Apply, Functor, Monad}
import cats.syntax.apply._
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.syntax.traverse._
import cats.instances.list._
import doobie.util.Read
import doobie.util.fragment.Fragment
import doobie.util.query.Query0
import fantasy.ApiError.{ApiFieldError, ApiValidationError}
import fantasy.ApiErrorRaise
import fantasy.typeclass.Raise.syntax._
import fantasy.util.db.Pageable
import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import ru.tinkoff.tschema.swagger.SwaggerTypeable

@derive(encoder, decoder)
final case class Page[T] protected[http] (data: List[T], currentPage: Int, totalPages: Int)
object Page {

  def create[T](data: List[T], pageRequest: PageRequest, totalElements: Long): Page[T] = {
    val totalPages = (totalElements / pageRequest.size) + (if (totalElements % pageRequest.size > 0) 1 else 0)
    new Page(data, pageRequest.page, totalPages.toInt)
  }

  def single[T](elem: T): Page[T] = new Page[T](List(elem), 1, 1)

  def fromPageable[F[_]: Functor: Apply, A](data: Pageable[F, A], page: PageRequest): F[Page[A]] =
    data.data.map2(data.total)(Page.create(_, page, _))

  def traverse[F[_]: Applicative: Functor, A, B](page: Page[A])(f: A => F[B]): F[Page[B]] =
    page.data.traverse(f).map(d => Page(d, page.currentPage, page.totalPages))

  implicit val functor: Functor[Page] = new Functor[Page] {
    override def map[A, B](fa: Page[A])(f: A => B): Page[B] = Page(fa.data.map(f), fa.currentPage, fa.totalPages)
  }

  implicit def swagger[T: SwaggerTypeable]: SwaggerTypeable[Page[T]] = SwaggerTypeable.genTypeable

}

/**
  * @param page not zero based
  * @param size positive int
  */
final case class PageRequest protected[http] (protected[http] val page: Int, protected[http] val size: Int) {

  import doobie.implicits._

  def take[F[_], A](s: fs2.Stream[F, A]): fs2.Stream[F, A] = s.take(page.toLong * size).takeRight(size)
  def take[A](list: List[A]): List[A]                      = list.slice(page * size, page * size + size)
  def paginate[A: Read](q: Fragment): Query0[A] =
    (q ++ fr" LIMIT $size OFFSET ${(page - 1) * size}").query[A]
  def next: PageRequest = PageRequest(page + 1, size)
}
object PageRequest {
  import fantasy.util.i18n.Messages.validations._

  def fill[F[_]: ApiErrorRaise: Monad](page: Int, pageSize: Int): F[PageRequest] =
    for {
      _ <- ApiValidationError(ApiFieldError("page", InvalidValue)).raise.whenA(page < 1)
      _ <- ApiValidationError(ApiFieldError("pageSize", InvalidValue)).raise.whenA(pageSize <= 0)
      _ <- ApiValidationError(ApiFieldError("pageSize", InvalidValue)).raise.whenA(pageSize > 50)
    } yield PageRequest(page, pageSize)

  val First100: PageRequest = PageRequest(1, 100)
}
