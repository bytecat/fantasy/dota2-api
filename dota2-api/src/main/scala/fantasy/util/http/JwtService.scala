package fantasy.util.http

import java.time.{LocalDateTime, ZoneId}

import cats.Applicative
import cats.syntax.applicative._
import io.circe.{Decoder, Encoder}
import io.circe.syntax._
import io.circe.parser
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim}

class JwtService[F[_]: Applicative](secret: String) {

  def encode[C: Encoder](content: C, expAt: Option[LocalDateTime]): F[String] = {
    val claim = JwtClaim(
      content = content.asJson.noSpaces,
      expiration = expAt.map(_.atZone(ZoneId.systemDefault()).toInstant.getEpochSecond)
    )

    Jwt.encode(claim, secret, JwtAlgorithm.HS256)
  }.pure[F]

  def decode[C: Decoder](content: String): F[Option[C]] = {
    Jwt
      .decodeRaw(content, secret, Seq(JwtAlgorithm.HS256))
      .toOption
      .flatMap(parser.decode(_).toOption)
  }.pure[F]

}

object JwtService {

  object syntax {
    implicit class JwtEncode[C](val content: C) extends AnyVal {
      def asJwt[F[_]](
          expAt: Option[LocalDateTime] = None
      )(implicit C: Encoder[C], jwtService: JwtService[F]): F[String] =
        jwtService.encode(content, expAt)
    }

    implicit class JwtDecode(val token: String) extends AnyVal {
      def parseJwt[F[_], D](implicit D: Decoder[D], jwtService: JwtService[F]): F[Option[D]] =
        jwtService.decode[D](token)
    }
  }

}
