package fantasy.util.swagger

import ru.tinkoff.tschema.syntax._

object pageable {

  def page = queryParam[Int]('page) |> queryParam[Int]('pageSize)

}
