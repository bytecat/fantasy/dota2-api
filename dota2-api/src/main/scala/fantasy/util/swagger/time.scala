package fantasy.util.swagger
import java.time.LocalDateTime

import ru.tinkoff.tschema.swagger.SwaggerTypeable

object time {
  implicit val localDateTime: SwaggerTypeable[LocalDateTime] = SwaggerTypeable.swaggerTypeableDate.as[LocalDateTime]
}
