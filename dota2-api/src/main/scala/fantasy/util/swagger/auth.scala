package fantasy.util.swagger

import cats.Monad
import cats.syntax.applicative._
import cats.syntax.flatMap._
import fantasy.domain.account.{AccountId, AuthorizeService}
import ru.tinkoff.tschema.finagle.Authorization.Bearer
import ru.tinkoff.tschema.finagle.{Authorization, BearerToken, Rejection, Routed}
import ru.tinkoff.tschema.syntax._
import shapeless.Witness

object auth {

  implicit def bearerAuthenticator[F[_]: Routed: Monad](
      implicit auth: AuthorizeService[F]
  ): Authorization[Bearer, F, AccountId] = {
    case Some(BearerToken(token)) =>
      auth.authenticate(token).flatMap(_.fold(Routed.reject[F, AccountId](Rejection.unauthorized))(_.pure[F]))
    case _ => Routed.reject(Rejection.unauthorized)
  }

  def jwt[name <: Symbol](name: Witness.Aux[name]) = bearerAuth[AccountId]("Token", name)
}
