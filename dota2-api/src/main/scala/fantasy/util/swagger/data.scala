package fantasy.util.swagger
import cats.data.NonEmptyList
import ru.tinkoff.tschema.swagger.SwaggerTypeable

object data {
  object Implicits {
    final implicit def nonEmptyListSwagger[T: SwaggerTypeable]: SwaggerTypeable[NonEmptyList[T]] =
      SwaggerTypeable.seq[NonEmptyList, T]
  }
}
