package fantasy.util.swagger
import ru.tinkoff.tschema.swagger.MkSwagger

object error {
  final implicit class MkSwaggerSyntax[U](val sw: MkSwagger[U]) extends AnyVal {
    def withServerError: MkSwagger[U] = sw /*
      sw.addResponse[ErrorResponse](
        StatusCodes.BadRequest
      )*/
  }
}
