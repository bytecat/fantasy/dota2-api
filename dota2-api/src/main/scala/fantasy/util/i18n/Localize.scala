package fantasy.util.i18n

import java.util.ResourceBundle

import cats.effect.Sync
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import fantasy.ThrowApplicative

sealed trait Type
object Type {
  case object ErrorsT extends Type
  type Errors = ErrorsT.type
  case object MessagesT extends Type
  type Messages = MessagesT.type
}

trait LocalizeProvider[F[_], T <: Type] {
  def provide(lang: Language): F[Localize[F]]
}

trait Localize[F[_]] {
  def fill(msg: String): F[String]
}

class ResourceBundleProvider[F[_]: ThrowApplicative, T <: Type](bundles: Map[Language, ResourceBundle])
    extends LocalizeProvider[F, T] {

  override def provide(lang: Language): F[Localize[F]] = bundles.get(lang) match {
    case None    => new IllegalStateException(s"not found lang $lang").raiseError[F, Localize[F]]
    case Some(r) => (new ResourceBundleLocalize[F](r): Localize[F]).pure[F]
  }
}

class ResourceBundleLocalize[F[_]: ThrowApplicative](val bundle: ResourceBundle) extends Localize[F] {
  override def fill(msg: String): F[String] = bundle.getString(msg).pure[F]
}

object ResourceBundleProvider {
  def create[I[_]: Sync, F[_]: ThrowApplicative, T <: Type](bundleName: String): I[LocalizeProvider[F, T]] =
    Sync[I].delay {
      val bundles = Language.values
        .map(l => l -> ResourceBundle.getBundle(bundleName, l.javaLocale, new UTF8Control)) // unsafe. can trhow error
        .toMap
      new ResourceBundleProvider[F, T](bundles)
    }
}
