package fantasy.util.i18n
import java.util.Locale

import doobie.util.Meta
import enumeratum._
import fantasy.util.http.HttpEnum
import io.circe.{KeyDecoder, KeyEncoder}
import tofu.logging.Loggable

import scala.collection.immutable

sealed trait Language extends EnumEntry {
  def javaLocale: Locale
  def enabled: Boolean = true
}
object Language extends HttpEnum[Language] {

  def default: Language = EN

  override def values: immutable.IndexedSeq[Language] = findValues.filter(_.enabled)

  case object RU extends Language {
    override def javaLocale: Locale = new Locale("ru")
  }
  case object EN extends Language {
    override def javaLocale: Locale = Locale.ENGLISH
  }

  implicit val doobie: Meta[Language] = Meta[String].timap(Language.withName)(_.entryName)

  implicit val keyEncoder: KeyEncoder[Language] = KeyEncoder[String].contramap(_.entryName)
  implicit val keyDecoder: KeyDecoder[Language] = KeyDecoder[String].map(Language.withNameOption(_).orNull)
  implicit val loggable: Loggable[Language]     = Loggable[String].contramap(_.entryName)
}
