package fantasy.util.i18n

object Messages {

  object validations {
    val RequiredField   = "validations.required"
    val InvalidEmail    = "validations.invalid_email"
    val InvalidPassword = "validations.invalid_password"
    val EULARequired    = "validations.EULA_required"

    val InvalidValue = "validations.invalid_field_value"
  }

  object errors {
    val EmailAlreadyExists = "accounts.emailAlreadyExists"
    val NotFound           = "errors.not_found"
  }
}
