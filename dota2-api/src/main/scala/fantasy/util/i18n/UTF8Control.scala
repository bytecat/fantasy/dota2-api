package fantasy.util.i18n
import java.io.{InputStream, InputStreamReader}
import java.util.{Locale, PropertyResourceBundle, ResourceBundle}

import scala.util.Try

class UTF8Control extends ResourceBundle.Control {
  override def newBundle(
      baseName: String,
      locale: Locale,
      format: String,
      loader: ClassLoader,
      reloadFlag: Boolean
  ): ResourceBundle =
    format match {
      case "java.class" =>
        super.newBundle(baseName, locale, format, loader, reloadFlag)
      case "java.properties" =>
        Option(toResourceName(toBundleName(baseName, locale), "properties"))
          .map(
            resourceName =>
              if (reloadFlag) reload(resourceName, loader)
              else loader.getResourceAsStream(resourceName)
          )
          .flatMap { stream =>
            val reader = new InputStreamReader(stream, "UTF-8")
            val bundle = Try(new PropertyResourceBundle(reader)).toOption
            reader.close()
            bundle
          }
          .orNull
      case _ => throw new IllegalArgumentException("Unknown format: " + format)
    }

  def reload(resourceName: String, classLoader: ClassLoader): InputStream = {
    Option(classLoader.getResource(resourceName))
      .map(url => url.openConnection)
      .map { connection =>
        connection.setUseCaches(false)
        connection.getInputStream
      }
      .orNull
  }
}
