package fantasy.util

import cats.instances.long._
import cats.{Order, Show}
import doobie.util.{Get, Put}
import io.circe.{Decoder, Encoder, KeyDecoder, KeyEncoder}
import ru.tinkoff.tschema.param.{HttpParam, Param, ParamSource}
import ru.tinkoff.tschema.swagger.SwaggerTypeable
import tofu.logging.Loggable

trait LongWrapper extends Any {
  def value: Long

  final override def toString: String = value.toString
}

trait LongWrapperCompanion[L <: LongWrapper] extends (Long => L) {
  def apply(value: Long): L

  implicit val ord: Order[L]                         = Order.by[L, Long](_.value)
  implicit val encoder: Encoder[L]                   = Encoder.encodeLong.contramap(_.value)
  implicit val decoder: Decoder[L]                   = Decoder.decodeLong.map(apply)
  implicit val show: Show[L]                         = Show.fromToString
  implicit val loggable: Loggable[L]                 = Loggable.longLoggable.contramap(_.value)
  implicit val swagger: SwaggerTypeable[L]           = SwaggerTypeable.swaggerTypeableLong.as[L]
  implicit val putDoobie: Put[L]                     = Put[Long].contramap(_.value)
  implicit val getDoobie: Get[L]                     = Get[Long].map(apply)
  implicit val qyeryParam: Param[ParamSource.All, L] = HttpParam.longParam.map(apply)

  implicit val playerKeyDecoder: KeyDecoder[L] = KeyDecoder[Long].map(apply)
  implicit val playerKeyEncoder: KeyEncoder[L] = KeyEncoder[Long].contramap(_.value)
}
