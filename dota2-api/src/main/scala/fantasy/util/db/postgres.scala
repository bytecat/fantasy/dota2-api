package fantasy.util.db

import java.sql.Timestamp
import java.time.LocalDateTime

import cats.Order
import cats.data.NonEmptyMap
import cats.syntax.either._
import doobie.util.Meta
import io.circe.{Decoder, Encoder, Json, KeyDecoder, KeyEncoder}
import io.circe.syntax._
import io.circe.parser._
import io.circe.Decoder._
import io.circe.Encoder._
import org.postgresql.util.PGobject

import scala.collection.immutable.SortedMap

object postgres {
  implicit val json: Meta[Json] = Meta.Advanced
    .other[PGobject]("json")
    .timap(pg => parse(pg.getValue).leftMap[Json](throw _).merge) { json =>
      val o = new PGobject
      o.setType("json")
      o.setValue(json.noSpaces)
      o
    }

  implicit def mapMeta[K: KeyEncoder: KeyDecoder, V: Encoder: Decoder]: Meta[Map[K, V]] =
    json.imap(j => j.as[Map[K, V]].leftMap[Map[K, V]](throw _).merge)(j => j.asJson)

  implicit def nonEmptyMapMeta[K: KeyEncoder: KeyDecoder: Order, V: Encoder: Decoder]: Meta[NonEmptyMap[K, V]] = {
    implicit val ord: Ordering[K] = Order[K].toOrdering
    mapMeta[K, V].imap(m => NonEmptyMap.fromMapUnsafe[K, V](SortedMap[K, V](m.toList: _*)))(m => m.toSortedMap)
  }

  implicit val localDateTime: Meta[LocalDateTime] = Meta[Timestamp].timap(_.toLocalDateTime)(Timestamp.valueOf)
}
