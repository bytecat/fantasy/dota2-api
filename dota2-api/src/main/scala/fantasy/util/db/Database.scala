package fantasy.util.db

import cats.effect.{Async, Blocker, ContextShift, Resource}
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import fantasy.context.DBConfig

trait Database[F[_]] {
  def transactor: Resource[F, Transactor[F]]
}

class Hikari[F[_]: Async: ContextShift](db: DBConfig) extends Database[F] {
  override def transactor: Resource[F, Transactor[F]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[F](db.maxConnects)
      te <- ExecutionContexts.cachedThreadPool[F]
      xa <- HikariTransactor.newHikariTransactor[F](db.driver, db.url, db.user, db.password, ce, Blocker.liftExecutionContext(te))
    } yield xa
}

object Hikari {
  def create[F[_]: Async: ContextShift](db: DBConfig): F[Hikari[F]] = Async[F].delay(new Hikari[F](db))
}
