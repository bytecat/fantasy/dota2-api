package fantasy.util.db

import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.traverse._
import cats.instances.list._
import cats.tagless.FunctorK
import cats.{Applicative, Eval, FlatMap, Functor, ~>}
import fs2.Chunk

case class Pageable[F[_], A](data: F[List[A]], total: F[Long], next: Eval[Pageable[F, A]]) {
  def mapK[G[_]](fk: F ~> G): Pageable[G, A] = Pageable[G, A](fk(data), fk(total), next.map(_.mapK(fk)))

  def evalMap[B](f: A => F[B])(implicit F: FlatMap[F], A: Applicative[F]): Pageable[F, B] =
    Pageable(data.flatMap(_.traverse(f)), total, next.map(_.evalMap(f)))

  def flatMap[B](f: A => List[B])(implicit F: Functor[F]): Pageable[F, B] =
    Pageable(data.map(_.flatMap(f)), total, next.map(_.flatMap(f)))

  def stream: fs2.Stream[F, A] =
    fs2.Stream
      .iterate(this)(_.next.value)
      .evalMap(_.data)
      .takeWhile(_.nonEmpty)
      .flatMap(s => fs2.Stream.chunk(Chunk(s: _*)).covary[F])
}

object Pageable {
  implicit def fk[A]: FunctorK[Pageable[*[_], A]] = new FunctorK[Pageable[*[_], A]] {
    override def mapK[F[_], G[_]](af: Pageable[F, A])(fk: F ~> G): Pageable[G, A] = af.mapK(fk)
  }
}
