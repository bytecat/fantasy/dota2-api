package fantasy

import cats.effect.ExitCode
import com.typesafe.scalalogging.StrictLogging
import fantasy.context.CoreConfig
import fantasy.util.db.Hikari
import io.sentry.Sentry
import monix.eval.{Task, TaskApp}
import scopt.OParser

object Dota2FantasyApp extends TaskApp with StrictLogging {

  override def run(args: List[String]): Task[ExitCode] =
    Task.defer {
      for {
        opt <- Task.fromEither(
                OParser.parse(RunOpt.parser, args, RunOpt()).toRight(new IllegalStateException("Invalid CLI options"))
              )
        config <- CoreConfig.load[Task](opt.profile)
        _ <- Task(
              logger.info(
                s"Start application with profile ${opt.profile}"
              )
            )
        _  <- startSentry(config)
        db <- Hikari.create[Task](config.db)
        _ <- db.transactor.use { implicit transactor =>
              for {
                _ <- new Serve(config).run()
              } yield ()
            }
      } yield ()
    } redeem (err => {
      logger.error("failed start app", err)
      ExitCode.Error
    }, _ => ExitCode.Success)

  def startSentry(config: CoreConfig) = Task {
    Sentry.init(config.sentry.dsn)
  }

}
