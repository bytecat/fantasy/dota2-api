package fantasy

import enumeratum._
import scopt.{OParser, OParserBuilder}

import scala.collection.immutable

final case class RunOpt(profile: Profile = Profile.Prod)
sealed trait Profile extends EnumEntry
object Profile extends Enum[Profile] {
  override def values: immutable.IndexedSeq[Profile] = immutable.IndexedSeq(Dev, Prod)

  case object Dev  extends Profile
  case object Prod extends Profile
}
object RunOpt {

  val parser: OParser[Unit, RunOpt] = {
    val builder: OParserBuilder[RunOpt] = OParser.builder[RunOpt]
    import builder._
    OParser.sequence(
      programName("DotaFantasyAPI"),
      opt[String]('p', "profile")
        .required()
        .valueName("<profile>")
        .validate(
          p =>
            Profile.withNameLowercaseOnlyOption(p.toLowerCase) match {
              case Some(_) => success
              case None    => failure(s"Undefined profile $p")
            }
        )
        .action((p, c) => c.copy(profile = Profile.withNameLowercaseOnly(p.toLowerCase)))
    )
  }
}
