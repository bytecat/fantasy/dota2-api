package fantasy.domain.player

import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.order._
import cats.syntax.traverse._
import cats.{Applicative, Monad, ~>}
import fantasy.ApiError.{ApiFieldError, ApiValidationError}
import fantasy.domain.`match`.{CalculatedPointValue, Point, PointId, Points}
import fantasy.domain.common.CountriesController.CountryResponse
import fantasy.domain.common.{CountriesController, CountriesRepository}
import fantasy.domain.player.PlayerController.model.{PlayerPointResponse, PlayerResponse}
import fantasy.domain.player.PlayerRepository.PlayerFilter
import fantasy.domain.player.PlayerService.PlayerCard
import fantasy.domain.series.{NextPlayerSeries, Round, RoundService, SeriesService}
import fantasy.domain.team.{Team, TeamId, TeamService}
import fantasy.typeclass.Contextual
import fantasy.typeclass.Raise.syntax._
import fantasy.util.db.Pageable
import fantasy.util.http.{Direction, Page, PageRequest, SortBy}
import fantasy.util.i18n.Language
import fantasy.{ApiErrorRaise, HasLang}
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

trait PlayerController[F[_]] {
  def find(
      name: Option[String],
      maxPrice: Option[Double],
      minPrice: Option[Double],
      team: Option[TeamId],
      position: Option[Int],
      sortBy: Option[Int],
      sortDirection: Option[Direction],
      hasMatches: Option[Boolean],
      page: Int,
      pageSize: Int
  ): F[Page[PlayerResponse]]
  def player(id: PlayerId): F[PlayerResponse]
}

class PlayerControllerF[F[_]: Monad: ApiErrorRaise: HasLang, I[_]: Monad: CountriesRepository: ApiErrorRaise, PP <: Point: PointId, P: PositionId](
    players: PlayerService[I, P, PP],
    teams: TeamService[I],
    points: Points[I, P, PP],
    price: PlayerPrice[I],
    roundService: RoundService[I],
    seriesService: SeriesService[I],
    trans: I ~> F
)(implicit C: fs2.Stream.Compiler[F, F], C2: fs2.Stream.Compiler[I, I])
    extends PlayerController[F] {

  import fantasy.util.i18n.Messages.validations._

  def find(
      name: Option[String],
      maxPrice: Option[Double],
      minPrice: Option[Double],
      team: Option[TeamId],
      position: Option[Int],
      sortBy: Option[Int],
      sortDirection: Option[Direction],
      hasMatches: Option[Boolean],
      page: Int,
      pageSize: Int
  ): F[Page[PlayerResponse]] =
    for {
      _ <- ApiValidationError(ApiFieldError("position", InvalidValue))
            .raise[F, Unit]
            .unlessA(position.forall(p => PositionId[P].fromId(p).isDefined))
      _ <- ApiValidationError(ApiFieldError("sortBy", InvalidValue))
            .raise[F, Unit]
            .unlessA(sortBy.forall(s => PointId[PP].fromId(s).isDefined))
      pageReq <- PageRequest.fill[F](page, pageSize)
      filtered <- filterPlayers(
                   name,
                   minPrice,
                   maxPrice,
                   team,
                   position.flatMap(PositionId[P].fromId),
                   hasMatches,
                   sortBy.flatMap(PointId[PP].fromId),
                   sortDirection,
                   pageReq
                 )
      transData = filtered.mapK(trans)
      res       <- Page.fromPageable(transData, pageReq)
    } yield res

  def player(id: PlayerId): F[PlayerResponse] =
    Contextual[F, Language].askF { lang =>
      trans(
        for {
          player          <- players.get(id)
          team            <- player.teamId.traverse(teams.get)
          lastRoundPoints <- points.lastRound(player, None)
          allPoints       <- points.all(player, None)
          cPrice          <- price.price(id)
          country         <- CountriesRepository.get[I](player.country)
          nextRound       <- roundService.nextRound()
          nextMatches     <- team.traverse(getNextSeries(id, _, nextRound))
        } yield
          PlayerResponse(
            id,
            player.nick,
            player.name,
            team,
            cPrice.getOrElse(0d),
            PlayerPointResponse(allPoints, lastRoundPoints, None),
            PositionId[P].toId(player.position),
            CountriesController.map(country, lang),
            player.photo,
            nextMatches.getOrElse(List())
          )
      )
    }

  private def mapPlayerCard(pk: PlayerCard[P, PP], lang: Language, nextRound: Option[Round]): I[PlayerResponse] =
    for {
      country     <- CountriesRepository.get[I](pk.player.country)
      team        <- pk.player.teamId.traverse(teams.get)
      points      = PlayerPointResponse(pk.totalPoints, pk.lastPoints, pk.pointsType.map(PointId[PP].toId))
      nextMatches <- team.traverse(getNextSeries(pk.player.id, _, nextRound))
    } yield
      PlayerResponse(
        pk.player.id,
        pk.player.nick,
        pk.player.name,
        team,
        pk.price,
        points,
        PositionId[P].toId(pk.player.position),
        CountriesController.map(country, lang),
        pk.player.photo,
        nextMatches.getOrElse(List())
      )

  private def getNextSeries(p: PlayerId, team: Team, round: Option[Round]): I[List[NextPlayerSeries]] =
    round.traverse { r =>
      seriesService
        .getByRound(r.number)
        .flatMap(
          _.filter(s => s.home.contains(team.id) || s.guest.contains(team.id))
            .traverse(
              s =>
                s.home
                  .filter(_ =!= team.id)
                  .orElse(s.guest.filter(_ =!= team.id))
                  .traverse(t => teams.get(t))
                  .map(t => NextPlayerSeries(s.id, t, s.time, s.round, s.maxGames, s.stage))
            )
        )
    }.map(_.getOrElse(List()))

  private def filterPlayers(
      name: Option[String],
      minPrice: Option[Double],
      maxPrice: Option[Double],
      team: Option[TeamId],
      position: Option[P],
      hasMatches: Option[Boolean],
      sortBy: Option[PP],
      sortDirection: Option[Direction],
      page: PageRequest
  ): F[Pageable[I, PlayerResponse]] = {
    val sort = sortBy.map(pp => SortBy(pp, sortDirection.getOrElse(Direction.DESC)))
    Contextual[F, Language].askF { lang =>
      trans(for {
        nextRound <- roundService.nextRound()
        filterd = players
          .filter(
            PlayerFilter(name, team, minPrice, maxPrice, position, hasMatches, nextRound.map(_.number)),
            sort,
            page
          )
        data = filterd.evalMap(mapPlayerCard(_, lang, nextRound))
      } yield data)
    }
  }

}
object PlayerController {

  def create[I[_]: Applicative, F[_]: Monad: ApiErrorRaise: fs2.Stream.Compiler[*[_], F]: HasLang, G[_]: Monad: CountriesRepository: ApiErrorRaise: fs2.Stream.Compiler[
    *[_],
    G
  ], PP <: Point: PointId, P: PositionId](
      players: PlayerService[G, P, PP],
      teams: TeamService[G],
      points: Points[G, P, PP],
      price: PlayerPrice[G],
      roundService: RoundService[G],
      seriesService: SeriesService[G],
      trans: G ~> F
  ): I[PlayerController[F]] = {
    (new PlayerControllerF[F, G, PP, P](players, teams, points, price, roundService, seriesService, trans): PlayerController[
      F
    ]).pure[I]
  }

  object model {

    @derive(encoder, swagger)
    final case class PlayerPointResponse(total: CalculatedPointValue, last: CalculatedPointValue, `type`: Option[Int])

    @derive(encoder, swagger)
    final case class PlayerResponse(
        id: PlayerId,
        nick: String,
        name: Option[String],
        team: Option[Team],
        price: Double,
        points: PlayerPointResponse,
        position: Int,
        country: CountryResponse,
        photo: Option[String],
        nextMatches: List[NextPlayerSeries]
    )

  }
}
