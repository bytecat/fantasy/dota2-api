package fantasy.domain.player
import cats.Order
import fantasy.domain.common.CountryId
import fantasy.domain.team.TeamId
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import tofu.logging.Loggable

final case class PlayerId(value: Long) extends AnyVal with LongWrapper
object PlayerId                        extends LongWrapperCompanion[PlayerId]

final case class Player[P: PositionId](
    id: PlayerId,
    name: Option[String],
    nick: String,
    photo: Option[String],
    teamId: Option[TeamId],
    position: P,
    country: CountryId
)

object Player {
  implicit def loggable[P](): Loggable[Player[P]] = Loggable.stringValue.contramap(p => s"player[id = ${p.id}]")
  implicit def order[P]: Order[Player[P]]         = Order.by[Player[P], PlayerId](_.id)
}
