package fantasy.domain.player

import cats.{Defer, Eval, Functor, Monad}
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.traverse._
import cats.instances.list._
import fantasy.domain.`match`.{CalculatedPointValue, Point, Points}
import fantasy.domain.common.CountryId
import fantasy.domain.player.PlayerError.PlayerNotExist
import fantasy.domain.player.PlayerRepository.PlayerFilter
import fantasy.domain.player.PlayerService.PlayerCard
import fantasy.domain.team.TeamId
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import fantasy.util.db.Pageable
import fantasy.util.http.{PageRequest, SortBy}
import tofu.syntax.logging._
import tofu.logging.{Logging, Logs}

import scala.util.control.NoStackTrace

trait PlayerService[F[_], P, PP <: Point] {

  def create(
      name: String,
      nick: String,
      photo: Option[String],
      teamId: TeamId,
      position: P,
      country: CountryId
  ): F[PlayerId]

  def get(id: PlayerId): F[Player[P]]

  def update(p: Player[P]): F[Unit]

  def filter(query: PlayerFilter[P], sortBy: Option[SortBy[PP]], page: PageRequest): Pageable[F, PlayerCard[P, PP]]

}

object PlayerService {

  case class PlayerCard[P, PP <: Point](
      player: Player[P],
      price: Double,
      lastPoints: CalculatedPointValue,
      totalPoints: CalculatedPointValue,
      pointsType: Option[PP]
  )

  def apply[F[_], P, PP <: Point](implicit ev: PlayerService[F, P, PP]): PlayerService[F, P, PP] = ev

  def create[I[_]: Functor, F[_]: Monad: Raise[*[_], PlayerError]: Defer: Logs[I, *[_]], P: PositionId, PP <: Point](
      players: PlayerRepository[F, P, PP],
      price: PlayerPrice[F],
      points: Points[F, P, PP]
  ): I[PlayerService[F, P, PP]] =
    Logs[I, F].forService[PlayerServiceF[F, P, PP]].map(implicit l => new PlayerServiceF(players, points, price))

  class PlayerServiceF[F[_]: Monad: Raise[*[_], PlayerError]: Logging, P: PositionId, PP <: Point](
      players: PlayerRepository[F, P, PP],
      points: Points[F, P, PP],
      price: PlayerPrice[F]
  ) extends PlayerService[F, P, PP] {
    override def create(
        name: String,
        nick: String,
        photo: Option[String],
        teamId: TeamId,
        position: P,
        country: CountryId
    ): F[PlayerId] =
      info"create player ($name, $teamId)" >> players.create(name, nick, photo, teamId, position, country)

    override def get(id: PlayerId): F[Player[P]] = players.get(id).flatMap(_.liftTo(PlayerNotExist(id)))

    def update(p: Player[P]): F[Unit] = players.update(p)

    override def filter(
        query: PlayerFilter[P],
        sortBy: Option[SortBy[PP]],
        page: PageRequest
    ): Pageable[F, PlayerCard[P, PP]] = {
      val value = players.filter(query, sortBy, page).flatMap(_.traverse(extraInfo(_, sortBy.map(_.by))))
      Pageable(value, players.count(query).map(_.toLong), Eval.later(filter(query, sortBy, page.next)))
    }

    private def extraInfo(p: Player[P], `type`: Option[PP]): F[PlayerCard[P, PP]] =
      for {
        all   <- points.all(p, `type`)
        last  <- points.lastRound(p, `type`)
        price <- price.price(p.id)
      } yield PlayerCard(p, price.getOrElse(0d), last, all, `type`)
  }

}

sealed trait PlayerError extends Throwable with NoStackTrace
object PlayerError {
  case class PlayerNotExist(id: PlayerId) extends Exception(s"Player with id = $id not found") with PlayerError
}
