package fantasy.domain.player

import cats.~>
import fantasy.domain.`match`.Point
import fantasy.domain.common.CountryId
import fantasy.domain.player.PlayerRepository.PlayerFilter
import fantasy.domain.series.RoundId
import fantasy.domain.team.TeamId
import fantasy.util.http.{PageRequest, SortBy}

trait PlayerRepository[F[_], P, PP <: Point] {

  def create(
      name: String,
      nick: String,
      photo: Option[String],
      teamId: TeamId,
      position: P,
      country: CountryId
  ): F[PlayerId]

  def get(id: PlayerId): F[Option[Player[P]]]

  def delete(id: PlayerId): F[Unit]

  def update(player: Player[P]): F[Unit]

  def filter(query: PlayerFilter[P], sortBy: Option[SortBy[PP]], page: PageRequest): F[List[Player[P]]]

  def count(query: PlayerFilter[P]): F[Int]

}

object PlayerRepository {
  def apply[F[_], P, PP <: Point](implicit PR: PlayerRepository[F, P, PP]): PlayerRepository[F, P, PP] = PR

  def mapK[I[_], O[_], P, PP <: Point](players: PlayerRepository[I, P, PP])(exec: I ~> O): PlayerRepository[O, P, PP] =
    new PlayerRepository[O, P, PP] {
      override def create(
          name: String,
          nick: String,
          photo: Option[String],
          teamId: TeamId,
          position: P,
          country: CountryId
      ): O[PlayerId] =
        exec(players.create(name, nick, photo, teamId, position, country))

      override def get(id: PlayerId): O[Option[Player[P]]] = exec(players.get(id))

      override def delete(id: PlayerId): O[Unit] = exec(players.delete(id))

      override def update(player: Player[P]): O[Unit] = exec(players.update(player))

      override def filter(query: PlayerFilter[P], sortBy: Option[SortBy[PP]], page: PageRequest): O[List[Player[P]]] =
        exec(players.filter(query, sortBy, page))

      override def count(query: PlayerFilter[P]): O[Int] = exec(players.count(query))
    }

  case class PlayerFilter[P](
      name: Option[String] = None,
      team: Option[TeamId] = None,
      minPrice: Option[Double] = None,
      maxPrice: Option[Double] = None,
      position: Option[P] = None,
      hasMatches: Option[Boolean] = None,
      currentRound: Option[RoundId] = None
  )
}
