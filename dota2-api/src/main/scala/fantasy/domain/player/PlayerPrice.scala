package fantasy.domain.player

import cats.~>
import fantasy.domain.player.PlayerPrice.RoundPrice
import fantasy.domain.series.RoundId
import simulacrum.typeclass

@typeclass
trait PlayerPrice[F[_]] {
  def price(p: PlayerId): F[Option[Double]]
  def priceHistory(p: PlayerId): fs2.Stream[F, RoundPrice]
}

object PlayerPrice {
  case class RoundPrice(round: RoundId, price: Double)

  def mapK[I[_], O[_]](
      pp: PlayerPrice[I]
  )(exec: I ~> O, execStream: fs2.Stream[I, ?] ~> fs2.Stream[O, ?]): PlayerPrice[O] =
    new PlayerPrice[O] {
      override def price(p: PlayerId): O[Option[Double]] = exec(pp.price(p))

      override def priceHistory(p: PlayerId): fs2.Stream[O, RoundPrice] = execStream(pp.priceHistory(p))
    }
}
