package fantasy.domain.player
import simulacrum.typeclass

@typeclass
trait PositionId[P] {
  def toId(position: P): Int
  def fromId(id: Int): Option[P]
}

object PositionId {

  object syntax extends PositionId.ToPositionIdOps

  def instance[P](to: P => Int, from: Int => Option[P]): PositionId[P] = new PositionId[P] {
    override def toId(position: P): Int     = to(position)
    override def fromId(id: Int): Option[P] = from(id)
  }
}
