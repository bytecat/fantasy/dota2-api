package fantasy.domain.common

import fantasy.util.i18n.Language
import fantasy.util.{LongWrapper, LongWrapperCompanion}

final case class CountryId(value: Long) extends AnyVal with LongWrapper
object CountryId                        extends LongWrapperCompanion[CountryId]

final case class Country(id: CountryId, name: Map[Language, String], code: String)
