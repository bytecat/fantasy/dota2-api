package fantasy.domain.common

import cats.Monad
import cats.syntax.functor._
import cats.syntax.flatMap._
import fantasy.ApiError.NotFound
import fantasy.ApiErrorRaise
import fantasy.typeclass.syntax.raise._
import fantasy.util.i18n.Language
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

class CountriesController[F[_]: Monad: ApiErrorRaise](countriesRepository: CountriesRepository[F]) {

  import CountriesController._

  def countries(lang: Language): F[List[CountryResponse]] = countriesRepository.all.map(_.map(map(_, lang)))

  def country(id: CountryId, lang: Language): F[CountryResponse] =
    countriesRepository
      .get(id)
      .flatMap(_.liftTo[F](NotFound))
      .map(map(_, lang))

}

object CountriesController {

  @derive(swagger, encoder)
  case class CountryResponse(id: CountryId, name: String, code: String)

  def map(c: Country, lang: Language) =
    CountryResponse(c.id, c.name.getOrElse(lang, c.name.getOrElse(Language.default, "")), c.code)
}
