package fantasy.domain.common

import java.util.concurrent.TimeUnit

import cats.effect.Timer
import cats.effect.concurrent.Ref
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.order._
import cats.tagless.FunctorK
import cats.{Functor, Monad}
import fantasy.ApiError.InternalError
import fantasy.ApiErrorRaise
import fantasy.domain.common.CountyCache.Cache
import fantasy.typeclass.syntax.raise._
import simulacrum.typeclass
import tofu.concurrent.MakeRef

import scala.concurrent.duration.Duration

@typeclass
trait CountriesRepository[F[_]] {
  def all: F[List[Country]]
  def get(id: CountryId): F[Option[Country]]
}

class CountyCache[F[_]: Monad: Timer](
    repository: CountriesRepository[F],
    ttl: Duration,
    private val cache: Ref[F, Cache]
) extends CountriesRepository[F] {

  override def all: F[List[Country]] = cache.get.flatMap { c =>
    Timer[F].clock.realTime(TimeUnit.SECONDS).flatMap { time =>
      if (c.updated + ttl.toSeconds <= time) {
        repository.all.flatMap(res => cache.set(Cache(res, time)).map(_ => res))
      } else {
        c.countries.pure[F]
      }
    }
  }

  override def get(id: CountryId): F[Option[Country]] = all.map(_.find(_.id === id))
}

object CountyCache {
  protected[common] case class Cache(countries: List[Country], updated: Long)
}

object CountriesRepository {
  implicit val fk: FunctorK[CountriesRepository] = cats.tagless.Derive.functorK

  def cached[I[_]: MakeRef[*[_], F]: Functor, F[_]: Monad: Timer](
      rep: CountriesRepository[F],
      ttl: Duration
  ): I[CountriesRepository[F]] =
    MakeRef[I, F].of(Cache(List(), 0)).map(c => new CountyCache[F](rep, ttl, c))

  def get[F[_]: Monad: ApiErrorRaise: CountriesRepository](id: CountryId): F[Country] =
    CountriesRepository[F]
      .get(id)
      .flatMap(_.liftTo[F](InternalError(new IllegalStateException(s"not found country with id = $id"))))
}
