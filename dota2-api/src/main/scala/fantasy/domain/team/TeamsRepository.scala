package fantasy.domain.team

import cats.tagless.FunctorK
import fantasy.util.http.PageRequest
import simulacrum.typeclass

@typeclass
trait TeamsRepository[F[_]] {
  def get(id: TeamId): F[Option[Team]]
  def create(name: String, tag: String, region: Region, logoUrl: Option[String]): F[TeamId]
  def update(team: Team): F[Unit]
  def delete(id: TeamId): F[Unit]
  def find(name: Option[String], page: PageRequest): F[List[Team]]
  def count(name: Option[String]): F[Int]
}

object TeamsRepository {
  implicit val fk: FunctorK[TeamsRepository] = cats.tagless.Derive.functorK
}
