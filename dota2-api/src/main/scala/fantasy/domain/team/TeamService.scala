package fantasy.domain.team

import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.{Defer, Eval, Functor, Monad}
import fantasy.ApiError
import fantasy.ApiError.ThrowableApiError
import fantasy.domain.team.TeamsError.TeamNotExists
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import fantasy.util.db.Pageable
import fantasy.util.http.PageRequest
import simulacrum.typeclass
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

@typeclass
trait TeamService[F[_]] {

  def get(id: TeamId): F[Team]

  def create(name: String, tag: String, region: Region, logoUrl: Option[String]): F[TeamId]

  def update(team: Team): F[Unit]

  def delete(id: TeamId): F[Unit]

  def find(name: Option[String], page: PageRequest): Pageable[F, Team]

}

object TeamService {

  def create[I[_]: Functor, F[_]: Monad: Defer: Raise[*[_], TeamsError]: Logs[I, *[_]]](
      teams: TeamsRepository[F]
  ): I[TeamService[F]] =
    Logs[I, F].forService[TeamServiceF[F]].map(implicit l => new TeamServiceF[F](teams))

  class TeamServiceF[F[_]: Monad: Raise[*[_], TeamsError]: Logging](teams: TeamsRepository[F]) extends TeamService[F] {

    override def get(id: TeamId): F[Team] = teams.get(id).flatMap(_.liftTo(TeamNotExists(id)))

    override def create(name: String, tag: String, region: Region, logoUrl: Option[String]): F[TeamId] =
      teams.create(name, tag, region, logoUrl)

    override def update(team: Team): F[Unit] = teams.update(team)

    override def delete(id: TeamId): F[Unit] = info"delete team $id" >> teams.delete(id)

    override def find(name: Option[String], page: PageRequest): Pageable[F, Team] =
      Pageable(teams.find(name, page), teams.count(name).map(_.toLong), Eval.later(find(name, page.next)))
  }
}

trait TeamsError extends Throwable with ApiError
object TeamsError {
  final case class TeamNotExists(id: TeamId)
      extends ThrowableApiError(404, s"team with id $id not exists") with TeamsError
}
