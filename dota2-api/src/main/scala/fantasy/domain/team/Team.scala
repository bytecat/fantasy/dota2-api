package fantasy.domain.team

import doobie.util.Meta
import enumeratum._
import fantasy.util.http.HttpEnum
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

import scala.collection.immutable

final case class TeamId(value: Long) extends AnyVal with LongWrapper
object TeamId                        extends LongWrapperCompanion[TeamId]

@derive(encoder, decoder, swagger)
final case class Team(id: TeamId, logoUrl: Option[String], name: String, tag: String, region: Region)

sealed trait Region extends EnumEntry
object Region extends HttpEnum[Region] {
  override def values: immutable.IndexedSeq[Region] = findValues

  //todo подумать над списком регионов
  case object SouthAmerica  extends Region
  case object NorthAmerica  extends Region
  case object Europa        extends Region
  case object China         extends Region
  case object SoutheastAsia extends Region
  case object CIS           extends Region

  implicit val meta: Meta[Region] =
    Meta[String].timap(n => Region.withNameLowercaseOnlyOption(n.toLowerCase).orNull)(_.entryName)
}
