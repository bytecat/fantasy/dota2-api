package fantasy.domain.team

import cats.{Monad, ~>}
import cats.syntax.flatMap._
import fantasy.ApiErrorRaise
import fantasy.util.http.{Page, PageRequest}

trait TeamController[F[_]] {
  def find(name: Option[String], page: Int, pageSize: Int): F[Page[Team]]
  def team(id: TeamId): F[Team]
}

class TeamControllerF[F[_]: Monad: ApiErrorRaise, I[_]: Monad](
    teamsService: TeamService[I],
    exec: I ~> F,
    execStream: fs2.Stream[I, ?] ~> fs2.Stream[F, ?]
)(
    implicit C: fs2.Stream.Compiler[F, F]
) extends TeamController[F] {

  def find(name: Option[String], page: Int, pageSize: Int): F[Page[Team]] =
    PageRequest.fill[F](page, pageSize).flatMap(findTeam(name, _))

  def team(id: TeamId): F[Team] = exec(teamsService.get(id))

  private def findTeam(name: Option[String], page: PageRequest) = {
    val res = teamsService.find(name, page).mapK(exec)
    Page.fromPageable(res, page)
  }
}
