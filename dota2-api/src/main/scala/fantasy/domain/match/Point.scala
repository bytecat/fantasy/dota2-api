package fantasy.domain.`match`

import cats.data.NonEmptyMap
import cats.~>
import fantasy.domain.player.{Player, PlayerId}
import fantasy.domain.series.RoundId
import simulacrum.typeclass

trait Point {
  def isPositive: Boolean
  def isBonus: Boolean
  def searchAvailable: Boolean
  def code: String
}

@typeclass
trait TotalPoints[P <: Point] {
  def total(map: Map[P, CalculatedPointValue]): CalculatedPointValue
}

object TotalPoints {
  object syntax {
    implicit class MapOps[P <: Point](val m: Map[P, CalculatedPointValue]) extends AnyVal {
      def total(implicit T: TotalPoints[P]): CalculatedPointValue = T.total(m)
    }
    implicit class NEMapOps[P <: Point](val m: NonEmptyMap[P, CalculatedPointValue]) extends AnyVal {
      def total(implicit T: TotalPoints[P]): CalculatedPointValue = T.total(m.toSortedMap)
    }
  }
}

trait Points[F[_], P, PP <: Point] {
  def saveRound(r: RoundId, p: PlayerId, points: Map[PP, CalculatedPointValue]): F[Unit]
  def lastRound(p: Player[P], `type`: Option[PP]): F[CalculatedPointValue]
  def all(p: Player[P], `type`: Option[PP]): F[CalculatedPointValue]
}

object Points {

  def apply[F[_], P, PP <: Point](implicit P: Points[F, P, PP]): Points[F, P, PP] = P

  def mapK[I[_], O[_], P, PP <: Point](points: Points[I, P, PP])(exec: I ~> O): Points[O, P, PP] =
    new Points[O, P, PP] {
      override def lastRound(p: Player[P], `type`: Option[PP]): O[CalculatedPointValue] =
        exec(points.lastRound(p, `type`))

      override def all(p: Player[P], `type`: Option[PP]): O[CalculatedPointValue] = exec(points.all(p, `type`))

      override def saveRound(r: RoundId, p: PlayerId, pp: Map[PP, CalculatedPointValue]): O[Unit] =
        exec(points.saveRound(r, p, pp))
    }
}

@typeclass
trait PointId[P] {
  def toId(point: P): Int
  def fromId(id: Int): Option[P]
}
