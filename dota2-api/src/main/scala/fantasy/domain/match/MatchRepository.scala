package fantasy.domain.`match`

import java.time.LocalDateTime

import cats.tagless.FunctorK
import fantasy.domain.series.SeriesId
import fantasy.domain.team.TeamId

trait MatchRepository[F[_]] {
  def create(seriesId: SeriesId, date: LocalDateTime, leftTeam: TeamId, rightTeam: TeamId, winLeft: Boolean): F[MatchId]

  def update(m: Match): F[Unit]

  def delete(id: MatchId): F[Unit]

  def get(id: MatchId): F[Option[Match]]

  def findBySeries(id: SeriesId): F[List[Match]]
}

object MatchRepository {
  implicit val functorK: FunctorK[MatchRepository] = cats.tagless.Derive.functorK
}
