package fantasy.domain.`match`

import cats.instances.bigDecimal._
import cats.kernel.Monoid
import cats.{Order, Show}
import doobie.util.Meta
import io.circe.{Decoder, Encoder, KeyDecoder, KeyEncoder}
import ru.tinkoff.tschema.param.{HttpParam, Param, ParamSource}
import ru.tinkoff.tschema.swagger.SwaggerTypeable
import tofu.logging.Loggable

import scala.util.Try

trait PointWrapper extends Any {
  def value: BigDecimal
}
trait PointWrapperCompanion[P <: PointWrapper] {

  val Scale = 2

  def apply(i: Int): P

  def apply(d: Double): P

  protected[`match`] def apply(p: BigDecimal): P

  implicit val ord: Order[P] = Order.by(_.value)
  implicit val encoder: Encoder[P] =
    Encoder.encodeBigDecimal.contramap(_.value.setScale(Scale, BigDecimal.RoundingMode.HALF_UP))
  implicit val decoder: Decoder[P]         = Decoder.decodeBigDecimal.map(apply)
  implicit val show: Show[P]               = Show.show(_.value.toString())
  implicit val loggable: Loggable[P]       = Loggable.bigDecimalLoggable.contramap(_.value)
  implicit val swagger: SwaggerTypeable[P] = SwaggerTypeable.swaggerTypeableBigDecimal.as[P]
  implicit val doobie: Meta[P] =
    Meta[BigDecimal].imap(apply)(_.value.setScale(Scale, BigDecimal.RoundingMode.HALF_UP))
  implicit val queryParam: Param[ParamSource.Query, P] = HttpParam.doubleParam.map(apply)

  implicit val keyEncoder: KeyEncoder[P] = KeyEncoder[String].contramap(_.value.toDouble.toString)
  implicit val keyDecoder: KeyDecoder[P] = (key: String) => Try(apply(key.toDouble)).toOption

  implicit val monoid: Monoid[P] = new Monoid[P] {
    override def empty: P = apply(0)

    override def combine(x: P, y: P): P = apply(x.value + y.value)
  }

  implicit val numberic: Numeric[P] = new Numeric[P] {
    override def plus(x: P, y: P): P = apply(x.value + y.value)

    override def minus(x: P, y: P): P = apply(x.value - y.value)

    override def times(x: P, y: P): P = apply(x.value * y.value)

    override def negate(x: P): P = apply(-x.value)

    override def fromInt(x: Int): P = apply(x)

    override def toInt(x: P): Int = x.value.toInt

    override def toLong(x: P): Long = x.value.toLong

    override def toFloat(x: P): Float = x.value.toFloat

    override def toDouble(x: P): Double = x.value.toDouble

    override def compare(x: P, y: P): Int = x.value.compare(y.value)
  }

}

case class CalculatedPointValue private (value: BigDecimal) extends AnyVal with PointWrapper
object CalculatedPointValue extends PointWrapperCompanion[CalculatedPointValue] {
  override def apply(i: Int): CalculatedPointValue = new CalculatedPointValue(BigDecimal(i))

  override def apply(d: Double): CalculatedPointValue = new CalculatedPointValue(BigDecimal(d))

  object syntax {
    implicit class IntOps(val i: Int) extends AnyVal {
      def asPoint: CalculatedPointValue = apply(i)
    }

    implicit class DoubleOps(val d: Double) extends AnyVal {
      def asPoint: CalculatedPointValue = apply(d)
    }
  }
}

case class RawPointValue private (value: BigDecimal) extends AnyVal with PointWrapper {
  def toCalculated(factor: BigDecimal = BigDecimal(1)): CalculatedPointValue = CalculatedPointValue(value * factor)
}
object RawPointValue extends PointWrapperCompanion[RawPointValue] {
  override def apply(i: Int): RawPointValue = new RawPointValue(BigDecimal(i))

  override def apply(d: Double): RawPointValue = new RawPointValue(BigDecimal(d))

  object syntax {
    implicit class IntOps(val i: Int) extends AnyVal {
      def asRaw: RawPointValue = apply(i)
    }

    implicit class DoubleOps(val d: Double) extends AnyVal {
      def asRaw: RawPointValue = apply(d)
    }
  }
}
