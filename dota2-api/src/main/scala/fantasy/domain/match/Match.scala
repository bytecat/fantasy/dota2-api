package fantasy.domain.`match`

import java.time.LocalDateTime

import fantasy.domain.player.PlayerId
import fantasy.domain.series.SeriesId
import fantasy.domain.team.TeamId
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import fantasy.util.swagger.time._
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

case class MatchId(value: Long) extends AnyVal with LongWrapper
object MatchId                  extends LongWrapperCompanion[MatchId]

@derive(swagger, encoder)
case class Match(
    id: MatchId,
    seriesId: SeriesId,
    date: LocalDateTime,
    leftTeam: TeamId,
    rightTeam: TeamId,
    winLeft: Boolean
)
case class MatchPoints[P <: Point](
    id: Long,
    matchId: MatchId,
    playerId: PlayerId,
    teamId: TeamId,
    points: Map[P, CalculatedPointValue],
    raw: Map[P, RawPointValue]
)
