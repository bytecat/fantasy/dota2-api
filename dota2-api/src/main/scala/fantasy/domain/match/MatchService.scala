package fantasy.domain.`match`
import java.time.LocalDateTime

import cats.data.NonEmptyList
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.{Defer, Functor, Monad}
import fantasy.ApiError
import fantasy.ApiError.ThrowableDetailedApiError
import fantasy.domain.`match`.MatchError.{MatchNotExists, SeriesMatchesNotFound}
import fantasy.domain.series.SeriesId
import fantasy.domain.team.TeamId
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import simulacrum.typeclass
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

@typeclass
trait MatchService[F[_]] {
  def create(seriesId: SeriesId, date: LocalDateTime, leftTeam: TeamId, rightTeam: TeamId, winLeft: Boolean): F[MatchId]

  def update(m: Match): F[Unit]

  def delete(id: MatchId): F[Unit]

  def get(id: MatchId): F[Match]

  def getBySeries(seriesId: SeriesId): F[NonEmptyList[Match]]

}

class MatchServiceF[F[_]: Monad: Raise[*[_], MatchError]: Logging](matchRepository: MatchRepository[F])
    extends MatchService[F] {
  override def create(
      seriesId: SeriesId,
      date: LocalDateTime,
      leftTeam: TeamId,
      rightTeam: TeamId,
      winLeft: Boolean
  ): F[MatchId] =
    info"create match on $seriesId series. $leftTeam VS $rightTeam" >> matchRepository.create(
      seriesId,
      date,
      leftTeam,
      rightTeam,
      winLeft
    )

  override def update(m: Match): F[Unit] = matchRepository.update(m)

  override def delete(id: MatchId): F[Unit] = info"delete match $id" >> matchRepository.delete(id)

  override def get(id: MatchId): F[Match] = matchRepository.get(id).flatMap(_.liftTo[F](MatchNotExists(id)))

  override def getBySeries(seriesId: SeriesId): F[NonEmptyList[Match]] =
    matchRepository.findBySeries(seriesId).flatMap {
      case Nil     => SeriesMatchesNotFound(seriesId).raise
      case x :: xs => NonEmptyList.of(x, xs: _*).pure
    }
}

object MatchService {
  def create[I[_]: Functor, F[_]: Defer: Monad: Raise[*[_], MatchError]: Logs[I, *[_]]](
      matchRepository: MatchRepository[F]
  ): I[MatchService[F]] =
    Logs[I, F].forService[MatchServiceF[F]].map(implicit l => new MatchServiceF[F](matchRepository))
}

sealed trait MatchError extends ApiError
object MatchError {
  import fantasy.util.i18n.Messages._

  case class MatchNotExists(id: MatchId)
      extends ThrowableDetailedApiError(404, s"match with $id not found", errors.NotFound) with MatchError
  case class SeriesMatchesNotFound(id: SeriesId)
      extends ThrowableDetailedApiError(404, s"matches by $id sereies not found", errors.NotFound) with MatchError
}
