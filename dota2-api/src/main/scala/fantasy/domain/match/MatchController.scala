package fantasy.domain.`match`

import java.time.LocalDateTime

import cats.{Monad, ~>}
import cats.syntax.flatMap._
import cats.syntax.functor._
import fantasy.domain.`match`.MatchController.{MatchPlayerResponse, MatchPointsResponse, MatchResponse}
import fantasy.domain.common.CountryId
import fantasy.domain.player.{Player, PlayerId, PlayerService, PositionId}
import fantasy.domain.series.SeriesId
import fantasy.domain.team.{Team, TeamId, TeamService}
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import ru.tinkoff.tschema.swagger.SwaggerMapKey

trait MatchController[F[_]] {
  def getMatch(id: MatchId): F[MatchResponse]
  def matchPoints(id: MatchId): F[List[MatchPointsResponse]]
  def playerPoints(id: PlayerId, matchId: MatchId): F[MatchPointsResponse]
  def bySeries(id: SeriesId): F[List[Match]]
}

class MatchControllerF[F[_]: Monad, I[_]: Monad, P: PositionId, PP <: Point: PointId](
    matchService: MatchService[I],
    teamService: TeamService[I],
    matchPointService: MatchPointService[I, PP],
    playerService: PlayerService[I, P, PP],
    exec: I ~> F
) extends MatchController[F] {

  def getMatch(id: MatchId): F[MatchResponse] =
    exec(for {
      m         <- matchService.get(id)
      leftTeam  <- teamService.get(m.leftTeam)
      rightTeam <- teamService.get(m.rightTeam)
    } yield MatchResponse(m.id, m.seriesId, m.date, leftTeam, rightTeam, m.winLeft))

  def matchPoints(id: MatchId): F[List[MatchPointsResponse]] =
    exec(for {
      points <- matchPointService.findByMatch(id)
      mpr    <- points.traverse(p => playerService.get(p.playerId).map(mapToResponse(_, p)))
    } yield mpr.toList)

  def playerPoints(id: PlayerId, matchId: MatchId): F[MatchPointsResponse] =
    exec(for {
      point  <- matchPointService.get(id, matchId)
      player <- playerService.get(id)
    } yield mapToResponse(player, point))

  def bySeries(id: SeriesId): F[List[Match]] = exec(matchService.getBySeries(id)).map(_.toList)

  private def mapToResponse(p: Player[P], mp: MatchPoints[PP]): MatchPointsResponse = {
    val playerResponse =
      MatchPlayerResponse(p.id, p.name, p.nick, p.photo, PositionId[P].toId(p.position), mp.teamId, p.country)
    val points = mp.points.map { case (k, v) => PointId[PP].toId(k) -> v }
    val raw    = mp.raw.map { case (k, v)    => PointId[PP].toId(k) -> v }
    MatchPointsResponse(playerResponse, points, raw)
  }

}

object MatchController {
  import fantasy.util.swagger.time._

  implicit val intkey: SwaggerMapKey[Int] = new SwaggerMapKey[Int]

  @derive(encoder, swagger)
  final case class MatchPlayerResponse(
      id: PlayerId,
      name: Option[String],
      nick: String,
      photo: Option[String],
      position: Int,
      teamId: TeamId,
      country: CountryId
  )

  @derive(encoder, swagger)
  final case class MatchResponse(
      id: MatchId,
      seriesId: SeriesId,
      date: LocalDateTime,
      left: Team,
      right: Team,
      winLeft: Boolean
  )

  @derive(encoder, swagger)
  final case class MatchPointsResponse(
      player: MatchPlayerResponse,
      points: Map[Int, CalculatedPointValue],
      raw: Map[Int, RawPointValue]
  )
}
