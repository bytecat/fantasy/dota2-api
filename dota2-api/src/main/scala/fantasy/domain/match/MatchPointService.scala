package fantasy.domain.`match`
import cats.data.NonEmptyList
import cats.~>
import fantasy.domain.`match`.MatchPointService.GroupBySeries
import fantasy.domain.player.PlayerId
import fantasy.domain.series.SeriesId
import fantasy.domain.team.TeamId
import fantasy.util.http.PageRequest

trait MatchPointService[F[_], P <: Point] {
  def create(
      gameId: MatchId,
      playerId: PlayerId,
      teamId: TeamId,
      points: Map[P, CalculatedPointValue],
      raw: Map[P, RawPointValue]
  ): F[Unit]
  def get(id: Long): F[MatchPoints[P]]
  def get(playerId: PlayerId, matchId: MatchId): F[MatchPoints[P]]
  def findByMatch(matchId: MatchId): F[NonEmptyList[MatchPoints[P]]]
  def findByPlayer(playerId: PlayerId, page: PageRequest): F[List[MatchPoints[P]]]
  def findByPlayerAndGroupBySeries(playerId: PlayerId): fs2.Stream[F, GroupBySeries[P]]
  def update(p: MatchPoints[P]): F[Unit]
  def delete(id: Long): F[Unit]
}

object MatchPointService {

  def apply[F[_], P <: Point](implicit MP: MatchPointService[F, P]): MatchPointService[F, P] = MP

  final case class GroupBySeries[P <: Point](series: SeriesId, points: NonEmptyList[MatchPoints[P]])

  def mapK[I[_], O[_], P <: Point](
      mp: MatchPointService[I, P]
  )(exec: I ~> O, execStream: fs2.Stream[I, ?] ~> fs2.Stream[O, ?]): MatchPointService[O, P] =
    new MatchPointService[O, P] {
      override def create(
          gameId: MatchId,
          playerId: PlayerId,
          teamId: TeamId,
          points: Map[P, CalculatedPointValue],
          raw: Map[P, RawPointValue]
      ): O[Unit] = exec(mp.create(gameId, playerId, teamId, points, raw))

      override def get(id: Long): O[MatchPoints[P]] = exec(mp.get(id))

      override def get(playerId: PlayerId, matchId: MatchId): O[MatchPoints[P]] = exec(mp.get(playerId, matchId))

      override def findByMatch(matchId: MatchId): O[NonEmptyList[MatchPoints[P]]] = exec(mp.findByMatch(matchId))

      override def findByPlayer(playerId: PlayerId, page: PageRequest): O[List[MatchPoints[P]]] =
        exec(mp.findByPlayer(playerId, page))

      override def findByPlayerAndGroupBySeries(playerId: PlayerId): fs2.Stream[O, GroupBySeries[P]] =
        execStream(mp.findByPlayerAndGroupBySeries(playerId))

      override def update(p: MatchPoints[P]): O[Unit] = exec(mp.update(p))

      override def delete(id: Long): O[Unit] = exec(mp.delete(id))
    }
}
