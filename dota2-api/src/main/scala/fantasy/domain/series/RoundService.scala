package fantasy.domain.series

import java.time.LocalDateTime

import cats.Monad
import cats.data.OptionT
import cats.syntax.applicative._
import cats.syntax.flatMap._
import fantasy.ApiError
import fantasy.ApiError.ThrowableApiError
import fantasy.domain.series.RoundService.{CurrentNotExists, NextRoundNotExists, RoundErrors, RoundNotExists}
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._

class RoundService[F[_]: Raise[*[_], RoundErrors]: Monad](roundRepository: RoundRepository[F]) {
  def get(round: RoundId): F[Round] = roundRepository.get(round).flatMap(_.liftTo[F](RoundNotExists(round)))
  def current(): F[Round] =
    roundRepository.current().flatMap {
      case None        => roundRepository.lastCompleted().flatMap(_.liftTo[F](CurrentNotExists))
      case Some(value) => value.pure[F]
    }

  def next(): F[Round] = nextRound().flatMap(_.liftTo[F](NextRoundNotExists))

  def update(r: Round): F[Unit] = roundRepository.update(r)

  def lastCompleted: F[Option[Round]] = roundRepository.lastCompleted()

  def nextRound(): F[Option[Round]] =
    OptionT(roundRepository.current())
      .orElseF(roundRepository.lastCompleted())
      .flatMapF(r => roundRepository.get(RoundId(r.number.value + 1)))
      .orElseF(roundRepository.get(RoundId(1)))
      .filter(_.from.isAfter(LocalDateTime.now()))
      .value
}

object RoundService {
  sealed trait RoundErrors               extends ApiError
  case object NextRoundNotExists         extends ThrowableApiError(404, s"next round not exists") with RoundErrors
  case class RoundNotExists(id: RoundId) extends ThrowableApiError(404, s"round $id not exists") with RoundErrors
  case object CurrentNotExists           extends ThrowableApiError(404, "completed rounds not exists") with RoundErrors

  def apply[F[_]](implicit R: RoundService[F]): RoundService[F] = R
}
