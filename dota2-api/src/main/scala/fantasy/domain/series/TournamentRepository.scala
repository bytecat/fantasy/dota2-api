package fantasy.domain.series
import java.time.{LocalDate, LocalDateTime}

import cats.tagless.FunctorK
import fantasy.domain.common.CountryId
import fantasy.util.http.PageRequest
import simulacrum.typeclass

@typeclass
trait TournamentRepository[F[_]] {
  def create(name: String, startDate: LocalDate, endDate: LocalDate, country: CountryId, logo: Option[String]): F[Unit]
  def get(id: TournamentId): F[Option[Tournament]]
  def update(tournament: Tournament): F[Unit]
  def delete(id: TournamentId): F[Unit]
  def get(page: PageRequest): F[List[Tournament]]
  def count: F[Int]
}

object TournamentRepository {
  implicit val fk: FunctorK[TournamentRepository] = cats.tagless.Derive.functorK
}

@typeclass
trait RoundRepository[F[_]] {
  def create(from: LocalDateTime, to: LocalDateTime, tournamentId: TournamentId): F[Unit]
  def get(number: RoundId): F[Option[Round]]
  def delete(number: RoundId): F[Unit]
  def update(round: Round): F[Unit]
  def current(): F[Option[Round]]
  def lastCompleted(): F[Option[Round]]
}

object RoundRepository {
  implicit val funktorK: FunctorK[RoundRepository] = cats.tagless.Derive.functorK[RoundRepository]
}

@typeclass
trait SeriesScore[F[_]] {

  def findBySeries(id: SeriesId): F[Option[Score]]

}

object SeriesScore {
  implicit val fk: FunctorK[SeriesScore] = cats.tagless.Derive.functorK
}
