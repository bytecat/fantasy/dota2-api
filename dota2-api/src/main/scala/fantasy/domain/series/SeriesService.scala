package fantasy.domain.series

import java.time.LocalDateTime

import cats.{Applicative, Eval, Monad}
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import fantasy.ApiError
import fantasy.ApiError.ThrowableApiError
import fantasy.domain.`match`.MatchService
import fantasy.domain.series.SeriesError._
import fantasy.domain.team.TeamId
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import fantasy.util.db.Pageable
import fantasy.util.http.PageRequest
import fs2.{Stream => FS2Stream}
import simulacrum.typeclass

@typeclass
trait SeriesService[F[_]] {

  def create(homeId: TeamId, guestId: TeamId, time: LocalDateTime, round: Int, maxGames: Int, stage: Stage): F[SeriesId]
  def get(id: SeriesId): F[Series]
  def update(series: Series): F[Unit]
  def delete(id: SeriesId): F[Unit]
  def getWithScore(id: SeriesId): F[(Series, Option[Score])]
  def getByRound(round: RoundId): F[List[Series]]

  def getByTeam(id: TeamId, page: PageRequest): Pageable[F, Series]

  def all(page: PageRequest): Pageable[F, Series]

}

object SeriesService {

  class SeriesServiceF[F[_]: Monad: Raise[*[_], SeriesError]: FS2Stream.Compiler[*[_], F]](
      seriesRepository: SeriesRepository[F],
      matchServices: MatchService[F],
      scores: SeriesScore[F]
  ) extends SeriesService[F] {

    override def create(
        homeId: TeamId,
        guestId: TeamId,
        time: LocalDateTime,
        round: Int,
        maxGames: Int,
        stage: Stage
    ): F[SeriesId] =
      seriesRepository.create(homeId, guestId, time, round, maxGames, stage)
    override def get(id: SeriesId): F[Series]    = seriesRepository.get(id).flatMap(_.liftTo[F](SeriesNotExist(id)))
    override def update(series: Series): F[Unit] = seriesRepository.update(series)
    override def delete(id: SeriesId): F[Unit]   = seriesRepository.delete(id)
    override def getWithScore(id: SeriesId): F[(Series, Option[Score])] = {
      val value = for {
        m     <- seriesRepository.get(id)
        score <- scores.findBySeries(id)
      } yield (m, score)
      value.flatMap {
        case (None, _)        => SeriesNotExist(id).raise[F, (Series, Option[Score])]
        case (Some(m), score) => (m, score).pure[F]
      }
    }
    override def getByRound(round: RoundId): F[List[Series]] =
      seriesRepository
        .findByRound(round)

    override def getByTeam(id: TeamId, page: PageRequest): Pageable[F, Series] =
      Pageable(
        seriesRepository.findByTeam(id, page),
        seriesRepository.countByTeam(id).map(_.toLong),
        Eval.later(getByTeam(id, page.next))
      )

    override def all(page: PageRequest): Pageable[F, Series] =
      Pageable(seriesRepository.get(page), seriesRepository.count.map(_.toLong), Eval.later(all(page.next)))
  }

  def create[I[_]: Applicative, F[_]: Monad: Raise[*[_], SeriesError]: FS2Stream.Compiler[*[_], F]](
      m: SeriesRepository[F],
      sc: SeriesScore[F],
      matchServices: MatchService[F]
  ): I[SeriesService[F]] =
    (new SeriesServiceF[F](m, matchServices, sc): SeriesService[F]).pure[I]
}

sealed trait SeriesError extends Throwable with ApiError
object SeriesError {
  case class SeriesNotExist(id: SeriesId) extends ThrowableApiError(404, s"series $id not exist") with SeriesError
}
