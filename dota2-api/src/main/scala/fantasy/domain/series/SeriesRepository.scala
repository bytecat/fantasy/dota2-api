package fantasy.domain.series

import java.time.LocalDateTime

import cats.tagless.FunctorK
import fantasy.domain.team.TeamId
import fantasy.util.http.PageRequest

trait SeriesRepository[F[_]] {
  def create(homeId: TeamId, guestId: TeamId, time: LocalDateTime, round: Int, maxGames: Int, stage: Stage): F[SeriesId]
  def get(id: SeriesId): F[Option[Series]]
  def update(`match`: Series): F[Unit]
  def delete(id: SeriesId): F[Unit]
  def findByRound(round: RoundId): F[List[Series]]
  def findByTeam(id: TeamId, page: PageRequest): F[List[Series]]
  def countByTeam(id: TeamId): F[Int]
  def get(page: PageRequest): F[List[Series]]
  def count: F[Int]
}

object SeriesRepository {
  implicit val fk: FunctorK[SeriesRepository] = cats.tagless.Derive.functorK
}
