package fantasy.domain.series

import java.time.{LocalDate, LocalDateTime}

import doobie.util.Meta
import enumeratum._
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.common.CountryId
import fantasy.domain.roster.RosterId
import fantasy.domain.team.TeamId
import fantasy.util.http.HttpEnum
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import fantasy.util.swagger.time._
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

import scala.collection.immutable

case class SeriesId(value: Long)     extends AnyVal with LongWrapper
object SeriesId                      extends LongWrapperCompanion[SeriesId]
case class TournamentId(value: Long) extends AnyVal with LongWrapper
object TournamentId                  extends LongWrapperCompanion[TournamentId]
case class RoundId(value: Long)      extends AnyVal with LongWrapper
object RoundId                       extends LongWrapperCompanion[RoundId]

@derive(swagger, encoder)
case class Series(
    id: SeriesId,
    home: Option[TeamId],
    guest: Option[TeamId],
    time: LocalDateTime,
    round: RoundId,
    maxGames: Int,
    stage: Stage
)

@derive(swagger, encoder)
case class Score(id: SeriesId, home: Int, guest: Int)

@derive(swagger, encoder)
case class Round(
    number: RoundId,
    from: LocalDateTime,
    to: LocalDateTime,
    tournament: TournamentId,
    points: Option[RoundPoints]
)

@derive(swagger, encoder)
case class RoundPoints(average: CalculatedPointValue, top: CalculatedPointValue, topRoster: RosterId)

@derive(swagger, encoder)
case class Tournament(
    id: TournamentId,
    name: String,
    startDate: LocalDate,
    endDate: LocalDate,
    country: CountryId,
    logo: Option[String]
)

sealed trait Stage extends EnumEntry
object Stage extends HttpEnum[Stage] {
  override def values: immutable.IndexedSeq[Stage] = findValues
  case object Qualifications extends Stage
  case object Group          extends Stage
  case object PlayOff        extends Stage
  case object Final          extends Stage

  implicit val meta: Meta[Stage] =
    Meta[String].timap(s => Stage.withNameLowercaseOnlyOption(s.toLowerCase).orNull)(_.entryName)
}
