package fantasy.domain.series

import java.time.LocalDateTime

import cats.instances.int._
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.option._
import cats.syntax.reducible._
import cats.syntax.traverse._
import cats.{Defer, Functor}
import fantasy.domain.`match`._
import fantasy.domain.player.PlayerId
import fantasy.domain.series.SeriesController.{PlayerSeries, SeriesResponse, TeamSeries}
import fantasy.domain.team.{Team, TeamId, TeamService}
import fantasy.util.http.{Page, PageRequest}
import fantasy.{ApiErrorRaise, ThrowMonad}
import fs2.Stream
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

trait SeriesController[F[_]] {
  def playerSeries(id: PlayerId, page: Int, pageSize: Int): F[Page[PlayerSeries]]
  def series(id: SeriesId): F[SeriesResponse]
  def allSeries(page: Int, pageSize: Int): F[Page[SeriesResponse]]
  def roundSeries(round: RoundId): F[List[SeriesResponse]]
  def teamSeries(id: TeamId, page: Int, pageSize: Int): F[Page[TeamSeries]]
}

class SeriesControllerImpl[F[_]: ThrowMonad: Logging: Stream.Compiler[F, *[_]]: ApiErrorRaise, P <: Point: TotalPoints](
    seriesService: SeriesService[F],
    teamService: TeamService[F],
    roundService: RoundService[F],
    tournamentService: TournamentService[F],
    scoreService: SeriesScore[F],
    points: MatchPointService[F, P],
    matchService: MatchService[F]
) extends SeriesController[F] {

  def playerSeries(id: PlayerId, page: Int, pageSize: Int): F[Page[PlayerSeries]] =
    for {
      pageReq <- PageRequest.fill[F](page, pageSize)
      result  <- findByPlayer(id, pageReq)
    } yield result

  def series(id: SeriesId): F[SeriesResponse] = seriesService.get(id).flatMap(toResponse)

  def allSeries(page: Int, pageSize: Int): F[Page[SeriesResponse]] =
    for {
      pageReq  <- PageRequest.fill[F](page, pageSize)
      pageable = seriesService.all(pageReq).evalMap(toResponse)
      res      <- Page.fromPageable(pageable, pageReq)
    } yield res

  def roundSeries(round: RoundId): F[List[SeriesResponse]] =
    seriesService.getByRound(round).flatMap(_.traverse(toResponse))

  def teamSeries(id: TeamId, page: Int, pageSize: Int): F[Page[TeamSeries]] =
    for {
      pageReq <- PageRequest.fill[F](page, pageSize)
      result  <- findByTeam(id, pageReq)
    } yield result

  private def findByTeam(id: TeamId, page: PageRequest): F[Page[TeamSeries]] = {
    val data = seriesService
      .getByTeam(id, page)
      .evalMap { series =>
        val value = for {
          round      <- roundService.get(series.round)
          tournament <- tournamentService.get(round.tournament)
          score      <- scoreService.findBySeries(series.id)
          seriesResp <- toResponse(series)
        } yield score.map(TeamSeries(seriesResp, tournament, _))
        value
          .handleErrorWith(
            err => s"failed create `TeamSeries` to team = $id, series = ${series.id}".cause(err) >> none.pure[F]
          )
          .map(_.toList)
      }
      .flatMap(identity)
    Page.fromPageable(data, page)
  }

  private def findByPlayer(id: PlayerId, page: PageRequest): F[Page[PlayerSeries]] = {
    val stream =
      points
        .findByPlayerAndGroupBySeries(id)
        .evalMap { point =>
          val value = for {
            series     <- seriesService.get(point.series)
            round      <- roundService.get(series.round)
            tournament <- tournamentService.get(round.tournament)
            score      <- scoreService.findBySeries(point.series)
            _ <- error"failed build `PlayerService` for player $id on ${point.series} series (not found score)".whenA(
                  score.isEmpty
                )
            seriesResp  <- toResponse(series)
            totalPoints = point.points.reduceMap(p => TotalPoints[P].total(p.points))
          } yield score.map(PlayerSeries(seriesResp, tournament, _, totalPoints))
          value.onError {
            case err =>
              s"failed create `PlayerSeries` to player = $id, series = ${point.series}".cause(err)
          }
        }
        .collect { case Some(teamSeries) => teamSeries }
    page
      .take(stream)
      .compile
      .toList
      .flatMap(
        data =>
          points
            .findByPlayerAndGroupBySeries(id)
            .foldMap(_ => 1)
            .compile
            .last
            .map(_.getOrElse(0))
            .map(cnt => Page.create(data, page, cnt.toLong))
      )
  }

  private def toResponse(series: Series): F[SeriesResponse] =
    for {
      guest <- series.guest.traverse(teamService.get)
      home  <- series.home.traverse(teamService.get)
    } yield SeriesResponse(series.id, home, guest, series.time, series.round, series.maxGames, series.stage)

}

object SeriesController {

  import fantasy.util.swagger.time._

  @derive(swagger, encoder)
  final case class PlayerSeries(
      series: SeriesResponse,
      tournament: Tournament,
      score: Score,
      playerPoints: CalculatedPointValue
  )

  @derive(swagger, encoder)
  final case class TeamSeries(series: SeriesResponse, tournament: Tournament, score: Score)

  @derive(swagger, encoder)
  final case class SeriesResponse(
      id: SeriesId,
      home: Option[Team],
      guest: Option[Team],
      time: LocalDateTime,
      round: RoundId,
      maxGames: Int,
      stage: Stage
  )

  def create[I[_]: Functor, F[_]: Defer: ThrowMonad: Logs[I, *[_]]: Stream.Compiler[F, *[_]]: ApiErrorRaise, P <: Point: TotalPoints](
      seriesService: SeriesService[F],
      teamService: TeamService[F],
      roundService: RoundService[F],
      tournamentService: TournamentService[F],
      scoreService: SeriesScore[F],
      points: MatchPointService[F, P],
      matchService: MatchService[F]
  ): I[SeriesController[F]] = {
    Logs[I, F].forService[SeriesControllerImpl[F, P]].map { implicit logger =>
      new SeriesControllerImpl[F, P](
        seriesService,
        teamService,
        roundService,
        tournamentService,
        scoreService,
        points,
        matchService
      ): SeriesController[F]
    }
  }

}
