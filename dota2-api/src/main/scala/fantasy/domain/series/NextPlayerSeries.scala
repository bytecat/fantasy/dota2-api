package fantasy.domain.series

import java.time.LocalDateTime

import fantasy.domain.team.Team
import fantasy.util.swagger.time._
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

@derive(encoder, swagger)
case class NextPlayerSeries(
    id: SeriesId,
    versus: Option[Team],
    time: LocalDateTime,
    round: RoundId,
    maxGames: Int,
    stage: Stage
)
