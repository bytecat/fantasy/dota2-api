package fantasy.domain.series

import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.{Applicative, Monad}
import fantasy.ApiError.ThrowableApiError
import fantasy.domain.common.{CountriesController, CountriesRepository}
import fantasy.domain.series.TournamentService.TournamentNotExists
import fantasy.modules.TournamentsModule.TournamentResponse
import fantasy.typeclass.Contextual
import fantasy.typeclass.Raise.syntax._
import fantasy.util.http.{Page, PageRequest}
import fantasy.util.i18n.Language
import fantasy.{ApiErrorRaise, HasLang}
import fs2.Stream

class TournamentService[F[_]: Monad: ApiErrorRaise: Stream.Compiler[*[_], F]](repository: TournamentRepository[F]) {
  def get(id: TournamentId): F[Tournament] = repository.get(id).flatMap(_.liftTo(TournamentNotExists(id)))
  def all(page: Int, pageSize: Int): F[Page[Tournament]] =
    for {
      pagReq <- PageRequest.fill(page, pageSize)
      data   <- repository.get(pagReq)
      cnt    <- repository.count
    } yield Page.create(data, pagReq, cnt.toLong)
}

object TournamentService {
  case class TournamentNotExists(id: TournamentId) extends ThrowableApiError(404, s"tournament $id not exists")
}

class TournamentController[F[_]: HasLang: Monad: ApiErrorRaise: CountriesRepository](
    service: TournamentService[F]
) {
  def get(id: TournamentId): F[TournamentResponse] =
    for {
      lang       <- Contextual[F, Language].ask
      tournament <- service.get(id)
      country    <- CountriesRepository.get[F](tournament.country)
    } yield
      TournamentResponse(
        tournament.id,
        tournament.name,
        tournament.startDate,
        tournament.endDate,
        CountriesController.map(country, lang),
        tournament.logo
      )
  def all(page: Int, pageSize: Int): F[Page[TournamentResponse]] =
    for {
      lang        <- Contextual[F, Language].ask
      tournaments <- service.all(page, pageSize)
      resp <- Page.traverse(tournaments) { t =>
               CountriesRepository
                 .get[F](t.country)
                 .map(
                   c =>
                     TournamentResponse(t.id, t.name, t.startDate, t.endDate, CountriesController.map(c, lang), t.logo)
                 )
             }
    } yield resp
}

object TournamentController {
  def create[I[_]: Applicative, F[_]: Monad: ApiErrorRaise: CountriesRepository: HasLang](
      service: TournamentService[F]
  ): I[TournamentController[F]] = {
    new TournamentController[F](service)
  }.pure[I]
}
