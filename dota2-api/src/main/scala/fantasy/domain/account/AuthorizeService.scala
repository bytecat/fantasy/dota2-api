package fantasy.domain.account
import java.time.LocalDateTime

import cats.{Applicative, Defer, Monad}
import cats.data.OptionT
import cats.syntax.functor._
import cats.syntax.eq._
import cats.syntax.applicative._
import cats.syntax.option._
import cats.syntax.flatMap._
import cats.tagless.FunctorK
import fantasy.domain.account.AuthorizeService.{RefreshTokenPayload, Token, TokenPayload}
import fantasy.domain.account.HashedPass.Implicits._
import fantasy.util.http.JwtService
import fantasy.util.http.JwtService.syntax._
import fantasy.util.swagger.time._
import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import simulacrum.typeclass

import scala.concurrent.duration._

trait AuthorizeService[F[_]] {
  def authorize(email: String, pass: String): F[Option[Token]]
  def authenticate(token: String): F[Option[AccountId]]
}

@typeclass
trait TokenService[F[_]] {
  def createToken(acc: Account): F[Token]
  def readToken(token: String): F[Option[TokenPayload]]
}

object TokenService {
  def jwt[F[_]: Defer: Monad](secret: String): TokenService[F] = new TokenService[F] {

    private val ttl                             = 1.hour
    private implicit val service: JwtService[F] = new JwtService[F](secret)

    override def createToken(acc: Account): F[Token] =
      for {
        exp     <- Defer[F].defer(expAt.pure)
        token   <- TokenPayload(acc.id).asJwt(exp.some)
        refresh <- RefreshTokenPayload(acc.id, exp.hashCode()).asJwt()
      } yield Token(token, refresh, exp)

    override def readToken(token: String): F[Option[TokenPayload]] = token.parseJwt[F, TokenPayload]

    private def expAt = LocalDateTime.now().plusMinutes(ttl.toMinutes)
  }

  implicit val fk: FunctorK[TokenService] = cats.tagless.Derive.functorK
}

object AuthorizeService {

  implicit val functorK: FunctorK[AuthorizeService] = cats.tagless.Derive.functorK

  final class FromJwt[F[_]: Monad: Defer](accounts: AccountRepository[F], tokenService: TokenService[F])
      extends AuthorizeService[F] {

    override def authorize(email: String, pass: String): F[Option[Token]] = {
      OptionT(accounts.get(email))
        .filter(_.password === pass.hashed)
        .semiflatMap(tokenService.createToken)
        .value
    }
    override def authenticate(token: String): F[Option[AccountId]] =
      tokenService.readToken(token).map(_.map(_.id))

  }

  def create[I[_]: Applicative, F[_]: Defer: Monad](
      accounts: AccountRepository[F],
      tokenService: TokenService[F]
  ): I[AuthorizeService[F]] = {
    new FromJwt[F](accounts, tokenService): AuthorizeService[F]
  }.pure[I]

  @derive(encoder, swagger)
  case class Token(token: String, refresh: String, expireAt: LocalDateTime)

  @derive(encoder, decoder)
  case class TokenPayload(id: AccountId)
  @derive(encoder, decoder)
  case class RefreshTokenPayload(id: AccountId, hash: Int)
}
