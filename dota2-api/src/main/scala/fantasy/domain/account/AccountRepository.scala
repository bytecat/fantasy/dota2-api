package fantasy.domain.account

import cats.tagless.FunctorK
import fantasy.domain.common.CountryId
import fantasy.util.i18n.Language
import simulacrum.typeclass

import scala.language.higherKinds

@typeclass
trait AccountRepository[F[_]] {

  def create(
      name: String,
      email: String,
      language: Language,
      countryId: CountryId,
      password: HashedPass,
      EULA: Boolean
  ): F[Unit]

  def get(id: AccountId): F[Option[Account]]

  def get(email: String): F[Option[Account]]

  def update(account: Account): F[Unit]

}

object AccountRepository {
  implicit val functorK: FunctorK[AccountRepository] = cats.tagless.Derive.functorK
}
