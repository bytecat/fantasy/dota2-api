package fantasy.domain.account

import cats.{Eq, Show}
import cats.instances.string._
import doobie.util.Meta
import fantasy.domain.common.CountryId
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import fantasy.util.i18n.Language
import io.circe.Encoder
import org.manatki.derevo.derive
import ru.tinkoff.tschema.swagger.SwaggerTypeable
import tofu.logging.Loggable
import tofu.logging.derivation.loggable

case class AccountId(value: Long) extends AnyVal with LongWrapper
object AccountId                  extends LongWrapperCompanion[AccountId]

@derive(loggable)
final case class Account(
    id: AccountId,
    name: String,
    email: String,
    language: Language,
    password: HashedPass,
    countryId: CountryId
)

case class HashedPass private (self: String)

object HashedPass {
  val Mask = "[hashed pass]"
  import com.roundeights.hasher.Implicits._

  def create(raw: String) = HashedPass(hash(raw))

  def empty = HashedPass("")

  implicit val eq: Eq[HashedPass]                   = Eq.by(_.self)
  implicit val show: Show[HashedPass]               = Show.show(_ => Mask)
  implicit val loggable: Loggable[HashedPass]       = Loggable.stringValue.contramap(_ => Mask)
  implicit val encoder: Encoder[HashedPass]         = Encoder.encodeString.contramap(_ => "")
  implicit val swagger: SwaggerTypeable[HashedPass] = SwaggerTypeable.swaggerTypeableString.as[HashedPass]
  implicit val doobie: Meta[HashedPass]             = Meta[String].timap(HashedPass(_))(_.self)

  private def hash(raw: String) = raw.sha256.hex

  object Implicits {
    implicit class HashedOps(val self: String) extends AnyVal {
      def hashed: HashedPass = create(self)
    }
  }
}
