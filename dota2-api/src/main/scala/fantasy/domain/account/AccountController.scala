package fantasy.domain.account

import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.{Applicative, Monad}
import fantasy.ApiError.{ApiFieldError, BadRequest, InternalError => FInternal}
import fantasy.domain.account.AuthorizeService.Token
import fantasy.domain.common.CountriesController.CountryResponse
import fantasy.domain.common.{CountriesController, CountriesRepository, CountryId}
import fantasy.domain.roster.{RosterId, RostersRepository, UserRoster}
import fantasy.http.Validator
import fantasy.typeclass.Contextual
import fantasy.typeclass.Raise.syntax._
import fantasy.util.i18n.Language
import fantasy.{ApiErrorRaise, HasLang}
import org.manatki.derevo.circeDerivation._
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances._

class AccountController[F[_]: Monad: ApiErrorRaise: UserRoster: RostersRepository: HasLang: CountriesRepository](
    accounts: AccountService[F],
    authorizeService: AuthorizeService[F]
) {
  import AccountController._
  import Validator.syntax._

  def create(body: SignUpRequest): F[Token] =
    for {
      req   <- body.valid
      _     <- accounts.create(req.name, req.email, req.lang, req.countryId, req.password, req.EULA)
      token <- signIn(SignInRequest(body.email, body.password))
    } yield token

  def signIn(body: SignInRequest): F[Token] =
    authorizeService.authorize(body.email, body.password).flatMap(_.liftTo(BadRequest))

  def get(id: AccountId): F[AccountResponse] =
    for {
      acc      <- accounts.get(id)
      rosterId <- UserRoster[F].id(id)
      roster   <- RostersRepository[F].get(rosterId)
      lang     <- Contextual[F, Language].ctx(l => l)
      country <- CountriesRepository[F]
                  .get(acc.countryId)
                  .flatMap(
                    _.liftTo(FInternal(new IllegalStateException(s"Not found country with id ${acc.countryId}")))
                  )
    } yield
      AccountResponse(
        acc.id,
        acc.name,
        acc.email,
        acc.language,
        HashedPass.empty,
        CountriesController.map(country, lang),
        roster.map(_.id)
      )
}

object AccountController {

  import Validator.helper._
  import fantasy.util.i18n.Messages.validations._

  def create[I[_]: Applicative, F[_]: Monad: ApiErrorRaise: UserRoster: RostersRepository: CountriesRepository: HasLang](
      accounts: AccountService[F],
      authorizeService: AuthorizeService[F]
  ): I[AccountController[F]] =
    new AccountController[F](accounts, authorizeService).pure[I]

  @derive(decoder, swagger)
  case class SignUpRequest(
      name: String,
      email: String,
      countryId: CountryId,
      lang: Language,
      password: String,
      EULA: Boolean
  )

  @derive(decoder, swagger)
  case class SignInRequest(email: String, password: String)

  @derive(encoder, swagger)
  case class AccountResponse(
      id: AccountId,
      name: String,
      email: String,
      language: Language,
      password: HashedPass,
      country: CountryResponse,
      rosterId: Option[RosterId]
  )

  implicit def signUpRequestValidator[F[_]: Monad]: Validator[F, SignUpRequest] = build[F, SignUpRequest].apply(
    r => r.name.nonEmpty.error(ApiFieldError("name", RequiredField)),
    r => r.email.nonEmpty.error(ApiFieldError("email", RequiredField)),
    r => r.email.matches(".*@.*\\..*").error(ApiFieldError("email", InvalidEmail)),
    r =>
      r.password
        .matches("^(?=.*[a-z])(?=.*[A-Z]).{8,}$")
        .error(ApiFieldError("password", InvalidPassword)),
    r => r.EULA.error(ApiFieldError("EULA", EULARequired))
  )

}
