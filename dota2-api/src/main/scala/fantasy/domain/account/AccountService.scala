package fantasy.domain.account

import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.{Defer, Functor, Monad}
import fantasy.ApiError
import fantasy.ApiError.{ThrowableApiError, ThrowableDetailedApiError}
import fantasy.domain.account.AccountError.AccountNotExists
import fantasy.domain.account.HashedPass.Implicits._
import fantasy.domain.common.CountryId
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import fantasy.util.i18n.Language
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

import scala.language.higherKinds

trait AccountService[F[_]] {

  def create(
      name: String,
      email: String,
      language: Language,
      countryId: CountryId,
      password: String,
      EULA: Boolean
  ): F[Unit]

  def update(account: Account): F[Unit]

  def get(id: AccountId): F[Account]

}

object AccountService {

  def apply[F[_]](implicit U: AccountService[F]): AccountService[F] = U

  def create[I[_]: Functor, F[_]: Defer: Monad: Raise[*[_], AccountError]: Logs[I, *[_]]](
      repository: AccountRepository[F]
  ): I[AccountService[F]] =
    Logs[I, F].forService[AccountsF[F]].map(implicit log => new AccountsF[F](repository))

  class AccountsF[F[_]: Monad: Raise[*[_], AccountError]: Logging](users: AccountRepository[F])
      extends AccountService[F] {

    override def create(
        name: String,
        email: String,
        language: Language,
        countryId: CountryId,
        password: String,
        EULA: Boolean
    ): F[Unit] =
      info"create user name = $name email = $email)" >>
        users.create(
          name = name,
          email = email,
          language = language,
          countryId = countryId,
          password = password.hashed,
          EULA = EULA
        )

    override def get(id: AccountId): F[Account] = users.get(id).flatMap(_.liftTo(AccountNotExists(id.toString)))

    override def update(account: Account): F[Unit] = info"update account $account" >> users.update(account)

  }

}

trait AccountError extends Throwable with ApiError

object AccountError {
  import fantasy.util.i18n.Messages._

  final case class AccountNotExists(id: String)
      extends ThrowableApiError(404, s"account with $id does not exists") with AccountError
  final case class EmailAlreadyExists(email: String)
      extends ThrowableDetailedApiError(400, s"account with $email already registered", errors.EmailAlreadyExists)
      with AccountError
}
