package fantasy.domain.dreamteam

import cats.data.{NonEmptyList, NonEmptyMap}
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.player.PlayerId
import fantasy.domain.series.RoundId
import fantasy.domain.team.TeamId

case class DreamTeam(
    round: RoundId,
    players: NonEmptyList[PlayerId],
    points: NonEmptyMap[PlayerId, CalculatedPointValue],
    teams: Map[PlayerId, TeamId]
)
