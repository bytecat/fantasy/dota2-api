package fantasy.domain.dreamteam

import cats.instances.list._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.order._
import cats.syntax.traverse._
import cats.{Applicative, ApplicativeError, MonadError}
import fantasy.ApiError.NotFound
import fantasy.domain.`match`.{CalculatedPointValue, Point}
import fantasy.domain.common.{CountriesController, CountriesRepository}
import fantasy.domain.dreamteam.DreamTeamController.DreamTeamResponse
import fantasy.domain.player.{Player, PlayerService, PositionId}
import fantasy.domain.series.RoundId
import fantasy.domain.team.{Team, TeamService}
import fantasy.modules.RosterModule.RosterHistoryPlayer
import fantasy.typeclass.Contextual
import fantasy.typeclass.syntax.raise._
import fantasy.util.i18n.Language
import fantasy.{ApiErrorRaise, HasLang}
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

trait DreamTeamController[F[_]] {
  def get(round: RoundId): F[DreamTeamResponse]
}

class DreamTeamControllerF[F[_]: DreamTeamRepository: PlayerService[*[_], P, PP]: TeamService: CountriesRepository: MonadError[
  *[_],
  Throwable
]: HasLang: ApiErrorRaise, P: PositionId, PP <: Point]
    extends DreamTeamController[F] {
  override def get(round: RoundId): F[DreamTeamResponse] =
    for {
      team <- DreamTeamRepository[F].get(round).flatMap(_.liftTo[F](NotFound))
      resp <- map(team)
    } yield resp

  private def map(d: DreamTeam): F[DreamTeamResponse] =
    for {
      players <- d.players.traverse(PlayerService[F, P, PP].get)
      teams   <- d.teams.values.toList.traverse(TeamService[F].get)
      res     <- players.traverse(mapPlayer(_, teams, d))
    } yield DreamTeamResponse(d.round, res.toList)

  private def mapPlayer(p: Player[P], teams: List[Team], d: DreamTeam): F[RosterHistoryPlayer] =
    for {
      country <- CountriesRepository[F]
                  .get(p.country)
                  .flatMap(
                    ApplicativeError
                      .liftFromOption(_, new IllegalStateException(s"Not found country with id ${p.country}"))
                  )
      lang        <- Contextual[F, Language].ask
      countryResp = CountriesController.map(country, lang)
      team        = d.teams.get(p.id).flatMap(id => teams.find(_.id === id))
    } yield
      RosterHistoryPlayer(
        p.id,
        p.nick,
        team,
        PositionId[P].toId(p.position),
        d.points.lookup(p.id).getOrElse(CalculatedPointValue(0)),
        countryResp,
        inDreamTeam = true
      )

}

object DreamTeamController {

  @derive(swagger, encoder)
  case class DreamTeamResponse(round: RoundId, players: List[RosterHistoryPlayer])

  def create[I[_]: Applicative, F[_]: DreamTeamRepository: PlayerService[*[_], P, PP]: TeamService: CountriesRepository: MonadError[
    *[_],
    Throwable
  ]: HasLang, P: PositionId, PP <: Point]: I[DreamTeamController[F]] = {

    new DreamTeamControllerF[F, P, PP]: DreamTeamController[F]
  }.pure[I]
}
