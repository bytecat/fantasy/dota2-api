package fantasy.domain.dreamteam

import cats.tagless.FunctorK
import fantasy.domain.player.PlayerId
import fantasy.domain.series.RoundId
import simulacrum.typeclass

@typeclass
trait DreamTeamRepository[F[_]] {
  def create(d: DreamTeam): F[Unit]

  def update(d: DreamTeam): F[Unit]

  def delete(d: DreamTeam): F[Unit]

  def get(round: RoundId): F[Option[DreamTeam]]

  def inDreamTeam(playerId: PlayerId, roundId: RoundId): F[Boolean]
}

object DreamTeamRepository {
  implicit val functorK: FunctorK[DreamTeamRepository] = cats.tagless.Derive.functorK
}
