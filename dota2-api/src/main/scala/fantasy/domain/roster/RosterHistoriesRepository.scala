package fantasy.domain.roster

import cats.tagless.FunctorK
import fantasy.domain.series.RoundId
import simulacrum.typeclass

@typeclass
trait RosterHistoriesRepository[F[_]] {
  def get(roster: RosterId): F[List[RosterHistory]]
  def get(roster: RosterId, round: RoundId): F[Option[RosterHistory]]
  def create(roster: RosterHistory): F[Unit]
  def update(roster: RosterHistory): F[Unit]
  def delete(roster: RosterHistory): F[Unit]
  def delete(roster: RosterId): F[Unit]
}
object RosterHistoriesRepository {
  implicit val fk: FunctorK[RosterHistoriesRepository] = cats.tagless.Derive.functorK
}
