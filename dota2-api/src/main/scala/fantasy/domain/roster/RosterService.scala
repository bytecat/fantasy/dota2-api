package fantasy.domain.roster

import cats.{Applicative, Monad}
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.syntax.order._
import cats.syntax.flatMap._
import cats.data.NonEmptyList
import cats.instances.double._
import fantasy.ApiError
import fantasy.ApiError.{ThrowableApiError, ThrowableDetailedApiError}
import fantasy.domain.account.AccountId
import fantasy.domain.player.{PlayerId, PlayerPrice}
import fantasy.domain.roster.RosterService.{
  CaptainInNotRoster,
  InternalRosterError,
  RosterError,
  RosterHistoryNotExists,
  RosterNotExists
}
import fantasy.domain.roster.RosterValidator.ValidationError
import fantasy.domain.series.RoundId
import fantasy.domain.team.TeamId
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import simulacrum.typeclass

@typeclass
trait RosterService[F[_]] {
  def get(id: RosterId): F[Roster]
  def exists(id: RosterId): F[Boolean]
  def history(id: RosterId, round: RoundId): F[RosterHistory]
  def create(
      accountId: AccountId,
      name: String,
      players: NonEmptyList[PlayerId],
      captain: PlayerId,
      fanTeam: TeamId
  ): F[RosterId]
  def makeTransfers(id: RosterId, players: NonEmptyList[PlayerId], captain: PlayerId): F[Unit]
  def update(id: RosterId, name: String): F[Unit]
}

class RosterServiceF[F[_]: Raise[*[_], RosterError]: Raise[*[_], ValidationError]: Monad: RostersRepository: RosterHistoriesRepository: RosterValidator: PlayerPrice](
    initialMoney: Double
) extends RosterService[F] {
  override def get(id: RosterId): F[Roster] = RostersRepository[F].get(id).flatMap(_.liftTo[F](RosterNotExists(id)))

  override def history(id: RosterId, round: RoundId): F[RosterHistory] =
    RosterHistoriesRepository[F].get(id, round).flatMap(_.liftTo[F](RosterHistoryNotExists(id, round)))

  override def create(
      accountId: AccountId,
      name: String,
      players: NonEmptyList[PlayerId],
      captain: PlayerId,
      fanTeam: TeamId
  ): F[RosterId] =
    for {
      _     <- RosterValidator[F].validateNewRoster(initialMoney, players).flatMap(_.liftTo[F])
      _     <- CaptainInNotRoster(captain).raise[F, RosterId].unlessA(players.exists(_ === captain))
      price <- priceOfSquad(players)
      id    <- RostersRepository[F].create(accountId, initialMoney - price, name, players, captain, fanTeam)
    } yield id

  override def makeTransfers(id: RosterId, players: NonEmptyList[PlayerId], captain: PlayerId): F[Unit] =
    for {
      roster        <- get(id)
      _             <- RosterValidator[F].validate(roster, players).flatMap(_.liftTo[F])
      _             <- CaptainInNotRoster(captain).raise[F, RosterId].unlessA(players.exists(_ === captain))
      oldSquadPrice <- priceOfSquad(roster.players)
      newSquadPrice <- priceOfSquad(players)
      newRoster = roster.copy(
        availableMoney = roster.availableMoney + oldSquadPrice - newSquadPrice,
        captain = captain,
        players = players
      )
      _ <- RostersRepository[F].update(newRoster)
    } yield ()

  private def priceOfSquad(players: NonEmptyList[PlayerId]) =
    players
      .traverse(
        p => PlayerPrice[F].price(p).flatMap(_.liftTo[F](InternalRosterError(s"Not found price for player $p")))
      )
      .map(_.reduce)

  override def update(id: RosterId, name: String): F[Unit] =
    for {
      roster <- get(id)
      _      <- RostersRepository[F].update(roster.copy(name = name))
    } yield ()

  override def exists(id: RosterId): F[Boolean] = RostersRepository[F].get(id).map(_.isDefined)
}

object RosterService {

  def create[I[_]: Applicative, F[_]: Raise[*[_], RosterError]: Raise[*[_], ValidationError]: Monad: RostersRepository: RosterHistoriesRepository: RosterValidator: PlayerPrice](
      initialMoney: Double
  ): I[RosterService[F]] = {
    (new RosterServiceF[F](initialMoney): RosterService[F]).pure[I]
  }

  trait RosterError extends ApiError
  case class RosterNotExists(id: RosterId)
      extends ThrowableApiError(404, s"roster with id $id not exists") with RosterError

  case class RosterHistoryNotExists(id: RosterId, round: RoundId)
      extends ThrowableApiError(404, s"roster history with id $id in round $round not exists") with RosterError

  case class CaptainInNotRoster(playerId: PlayerId)
      extends ThrowableDetailedApiError(
        400,
        s"player $playerId can't be captain because he is not in roster",
        "roster.badCaptain"
      ) with RosterError

  case class InternalRosterError(msg: String) extends ThrowableApiError(500, msg) with RosterError
}
