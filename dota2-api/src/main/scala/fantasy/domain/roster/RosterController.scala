package fantasy.domain.roster

import cats.data.NonEmptyList
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.order._
import cats.syntax.traverse._
import cats.tagless.FunctorK
import cats.{Applicative, Monad}
import fantasy.ApiError.{Forbidden, ThrowableDetailedApiError}
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.account.AccountId
import fantasy.domain.common.{CountriesController, CountriesRepository}
import fantasy.domain.dreamteam.DreamTeamRepository
import fantasy.domain.league.LeagueService
import fantasy.domain.player.{PlayerId, PlayerPrice, PlayerService, PositionId}
import fantasy.domain.roster.RosterController.BadRoster
import fantasy.domain.series._
import fantasy.domain.team.{Team, TeamId, TeamService}
import fantasy.dota2.D2Point
import fantasy.modules.RosterModule._
import fantasy.typeclass.Contextual
import fantasy.typeclass.Raise.syntax._
import fantasy.util.i18n.Language
import fantasy.{ApiErrorRaise, HasLang}

trait RosterController[F[_]] {
  def create(accountId: AccountId, body: CreateRosterRequest): F[Unit]

  def squad(id: RosterId, accountId: AccountId, body: UpdateRosterRequest): F[Unit]

  def updateSettings(id: RosterId, accountId: AccountId, body: UpdateSettingsRequest): F[Unit]

  def histories(id: RosterId, round: RoundId): F[RosterHistoryResponse]

  def get(id: RosterId, accountId: AccountId): F[RosterResponse]
}

class RosterControllerF[F[_]: ApiErrorRaise: Monad: UserRoster: HasLang: CountriesRepository: PlayerPrice, P: PositionId](
    rosterService: RosterService[F],
    playerService: PlayerService[F, P, D2Point],
    teamService: TeamService[F],
    leagueService: LeagueService[F],
    roundSerivce: RoundService[F],
    seriesService: SeriesService[F],
    dreamTeamRepository: DreamTeamRepository[F]
)(implicit C: fs2.Stream.Compiler[F, F])
    extends RosterController[F] {

  override def create(accountId: AccountId, body: CreateRosterRequest): F[Unit] =
    for {
      players <- NonEmptyList.fromList(body.players).liftTo[F](BadRoster)
      id      <- rosterService.create(accountId, body.name, players, body.captain, body.fanTeam)
      leagues <- leagueService.availableSystemLeagues(id)
      _       <- leagues.map(_.id).traverse(leagueService.joinToLeague(_, accountId))
    } yield ()

  override def squad(id: RosterId, accountId: AccountId, body: UpdateRosterRequest): F[Unit] =
    for {
      players <- NonEmptyList.fromList(body.players).liftTo[F](BadRoster)
      _       <- rosterService.makeTransfers(id, players, body.captain)
    } yield ()

  override def updateSettings(id: RosterId, accountId: AccountId, body: UpdateSettingsRequest): F[Unit] =
    for {
      playerRoster <- UserRoster[F].id(accountId)
      _            <- Forbidden.raise.whenA(playerRoster =!= id)
      _            <- rosterService.update(id, body.name)
    } yield ()

  override def histories(id: RosterId, round: RoundId): F[RosterHistoryResponse] =
    for {
      roster  <- rosterService.get(id)
      history <- rosterService.history(id, round)
      players <- history.players.traverse(
                  p =>
                    toRosterHistoryPlayer(
                      round,
                      p,
                      history.points.lookup(p).getOrElse(CalculatedPointValue(0)),
                      history.teams.get(p)
                    )
                )
    } yield RosterHistoryResponse(id, roster.name, round, players.toList, history.captain, history.total)

  override def get(id: RosterId, accountId: AccountId): F[RosterResponse] =
    for {
      userRoster <- UserRoster[F].id(accountId)
      _          <- Forbidden.raise.whenA(userRoster =!= id)
      roster     <- rosterService.get(id)
      nextRound  <- roundSerivce.nextRound()
      players    <- roster.players.traverse(toRosterPlayer(_, nextRound))
      team       <- teamService.get(roster.fanTeam)
    } yield RosterResponse(id, players.toList, roster.captain, roster.availableMoney, roster.name, team)

  def toRosterPlayer(p: PlayerId, nextRound: Option[Round]): F[RosterPlayer] =
    for {
      lang        <- Contextual[F, Language].ask
      player      <- playerService.get(p)
      team        <- player.teamId.traverse(teamService.get)
      country     <- CountriesRepository.get[F](player.country)
      price       <- PlayerPrice[F].price(p)
      nextMatches <- team.traverse(t => getNextSeries(p, t, nextRound)).map(_.getOrElse(List()))
    } yield
      RosterPlayer(
        p,
        player.nick,
        team,
        PositionId[P].toId(player.position),
        CountriesController.map(country, lang),
        price.getOrElse(0d),
        nextMatches
      )

  def toRosterHistoryPlayer(
      roundId: RoundId,
      p: PlayerId,
      points: CalculatedPointValue,
      teamId: Option[TeamId]
  ): F[RosterHistoryPlayer] =
    for {
      lang        <- Contextual[F, Language].ask
      player      <- playerService.get(p)
      team        <- teamId.traverse(teamService.get)
      country     <- CountriesRepository.get[F](player.country)
      inDreamTeam <- dreamTeamRepository.inDreamTeam(p, roundId)
    } yield
      RosterHistoryPlayer(
        p,
        player.nick,
        team,
        PositionId[P].toId(player.position),
        points,
        CountriesController.map(country, lang),
        inDreamTeam
      )

  private def getNextSeries(p: PlayerId, team: Team, round: Option[Round]): F[List[NextPlayerSeries]] =
    round.traverse { r =>
      seriesService
        .getByRound(r.number)
        .flatMap(
          _.filter(s => s.home.contains(team.id) || s.guest.contains(team.id))
            .traverse(
              s =>
                s.home
                  .filter(_ =!= team.id)
                  .orElse(s.guest)
                  .traverse(t => teamService.get(t))
                  .map(t => NextPlayerSeries(s.id, t, s.time, s.round, s.maxGames, s.stage))
            )
        )
    }.map(_.getOrElse(List()))
}

object RosterController {
  case object BadRoster extends ThrowableDetailedApiError(400, "bad roster", "roster.notCompleted")

  implicit val fuctorK: FunctorK[RosterController] = cats.tagless.Derive.functorK

  def create[I[_]: Applicative, F[_]: ApiErrorRaise: Monad: UserRoster: HasLang: CountriesRepository: PlayerPrice: fs2.Stream.Compiler[
    *[_],
    F
  ], P: PositionId](
      rosterService: RosterService[F],
      playerService: PlayerService[F, P, D2Point],
      teamService: TeamService[F],
      leagueService: LeagueService[F],
      roundSerivce: RoundService[F],
      seriesService: SeriesService[F],
      dreamTeamRepository: DreamTeamRepository[F]
  ): I[RosterController[F]] = {
    new RosterControllerF[F, P](
      rosterService,
      playerService,
      teamService,
      leagueService,
      roundSerivce,
      seriesService,
      dreamTeamRepository
    ): RosterController[F]
  }.pure[I]
}
