package fantasy.domain.roster

import cats.data.NonEmptyList
import fantasy.ApiError
import fantasy.ApiError.ThrowableDetailedApiError
import fantasy.domain.player.PlayerId
import fantasy.domain.roster.RosterValidator.ValidationError
import fantasy.domain.team.TeamId
import simulacrum.typeclass

@typeclass
trait RosterValidator[F[_]] {
  def validate(roster: Roster, transfer: NonEmptyList[PlayerId]): F[Either[ValidationError, Unit]]
  def validateNewRoster(availableMoney: Double, players: NonEmptyList[PlayerId]): F[Either[ValidationError, Unit]]
}

object RosterValidator {
  trait ValidationError extends ApiError
  case object RosterNotCompleted
      extends ThrowableDetailedApiError(400, "roster not completed", "roster.notCompleted") with ValidationError
  case class PlayerNotPickable(id: PlayerId)
      extends ThrowableDetailedApiError(400, s"player $id not pickable", "roster.notPickablePlayer")
      with ValidationError
  case class TeamLimit(id: TeamId)
      extends ThrowableDetailedApiError(400, s"team $id limit exceeded", "roster.teamLimit") with ValidationError
  case object MoneyExceeded
      extends ThrowableDetailedApiError(400, "money exceeded", "roster.moneyExceeded") with ValidationError
}
