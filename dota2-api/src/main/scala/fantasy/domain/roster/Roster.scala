package fantasy.domain.roster

import cats.data.{NonEmptyList, NonEmptyMap}
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.account.AccountId
import fantasy.domain.player.PlayerId
import fantasy.domain.series.RoundId
import fantasy.domain.team.TeamId
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import simulacrum.typeclass

case class RosterId(value: Long) extends AnyVal with LongWrapper
object RosterId                  extends LongWrapperCompanion[RosterId]

case class Roster(
    id: RosterId,
    accountId: AccountId,
    availableMoney: Double,
    name: String,
    players: NonEmptyList[PlayerId],
    captain: PlayerId,
    fanTeam: TeamId
)
case class RosterHistory(
    id: RosterId,
    round: RoundId,
    players: NonEmptyList[PlayerId],
    captain: PlayerId,
    points: NonEmptyMap[PlayerId, CalculatedPointValue],
    total: CalculatedPointValue,
    teams: Map[PlayerId, TeamId]
)

@typeclass
trait UserRoster[F[_]] {
  def id(id: AccountId): F[RosterId]
}
