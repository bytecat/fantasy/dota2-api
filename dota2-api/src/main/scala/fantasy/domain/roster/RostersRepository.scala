package fantasy.domain.roster

import cats.data.NonEmptyList
import cats.~>
import fantasy.domain.account.AccountId
import fantasy.domain.player.PlayerId
import fantasy.domain.team.TeamId
import simulacrum.typeclass

@typeclass
trait RostersRepository[F[_]] {
  def get(rosterId: RosterId): F[Option[Roster]]
  def create(
      account: AccountId,
      availableMoney: Double,
      name: String,
      players: NonEmptyList[PlayerId],
      captain: PlayerId,
      fanTeam: TeamId
  ): F[RosterId]
  def delete(rosterId: RosterId): F[Unit]
  def update(roster: Roster): F[Unit]
  def all: fs2.Stream[F, Roster]
}

object RostersRepository {

  def mapK[I[_], O[_]](
      rp: RostersRepository[I]
  )(exec: I ~> O, execStream: fs2.Stream[I, ?] ~> fs2.Stream[O, ?]): RostersRepository[O] =
    new RostersRepository[O] {
      override def get(rosterId: RosterId): O[Option[Roster]] = exec(rp.get(rosterId))

      override def create(
          account: AccountId,
          availableMoney: Double,
          name: String,
          players: NonEmptyList[PlayerId],
          captain: PlayerId,
          fanTeam: TeamId
      ): O[RosterId] =
        exec(rp.create(account, availableMoney, name, players, captain, fanTeam))

      override def delete(rosterId: RosterId): O[Unit] = exec(rp.delete(rosterId))

      override def update(roster: Roster): O[Unit] = exec(rp.update(roster))

      override def all: fs2.Stream[O, Roster] = execStream(rp.all)
    }
}
