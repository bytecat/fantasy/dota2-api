package fantasy.domain.league

import cats.tagless.FunctorK
import fantasy.domain.account.AccountId
import fantasy.domain.series.RoundId
import simulacrum.typeclass

@typeclass
trait LeagueRepository[F[_]] {
  def create(name: String, started: RoundId, `type`: LeagueType, author: Option[AccountId]): F[LeagueId]
  def get(id: LeagueId): F[Option[League]]
  def update(league: League): F[Unit]
  def delete(id: LeagueId): F[Unit]
  def byType(`type`: LeagueType): F[List[League]]
}

object LeagueRepository {
  implicit val fk: FunctorK[LeagueRepository] = cats.tagless.Derive.functorK
}
