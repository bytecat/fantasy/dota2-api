package fantasy.domain.league

import doobie.util.Meta
import enumeratum.EnumEntry
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.account.AccountId
import fantasy.domain.roster.RosterId
import fantasy.domain.series.RoundId
import fantasy.util.http.HttpEnum
import fantasy.util.{LongWrapper, LongWrapperCompanion}

import scala.collection.immutable

case class LeagueId(value: Long) extends LongWrapper
object LeagueId                  extends LongWrapperCompanion[LeagueId]

case class League(id: LeagueId, name: String, started: RoundId, `type`: LeagueType, author: Option[AccountId])

object League {

  val OverallLeague            = "Overall"
  def roundLeague(id: RoundId) = s"Round ${id.value}"

}

sealed trait LeagueType extends EnumEntry
object LeagueType extends HttpEnum[LeagueType] {
  case object System  extends LeagueType
  case object Public  extends LeagueType
  case object Private extends LeagueType

  override def values: immutable.IndexedSeq[LeagueType] = findValues

  implicit val meta: Meta[LeagueType] =
    Meta[String].timap(k => LeagueType.withNameLowercaseOnlyOption(k.toLowerCase).orNull)(_.entryName)
}

case class LeagueTableRow(
    roster: RosterId,
    league: LeagueId,
    places: Map[RoundId, Long],
    points: Map[RoundId, CalculatedPointValue],
    totalPoints: CalculatedPointValue
)
