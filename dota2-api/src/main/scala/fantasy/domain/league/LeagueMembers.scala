package fantasy.domain.league

import cats.~>
import fantasy.domain.league.LeagueMembers.Member
import fantasy.domain.roster.RosterId
import fantasy.domain.series.RoundId
import simulacrum.typeclass

@typeclass
trait LeagueMembers[F[_]] {
  def join(roster: RosterId, league: LeagueId, from: RoundId): F[Unit]
  def membersCount(leagueId: LeagueId): F[Long]
  def consists(roster: RosterId): F[List[Member]]
}

object LeagueMembers {

  case class Member(id: RosterId, from: RoundId, league: LeagueId)

  def mapK[I[_], O[_]](
      lm: LeagueMembers[I]
  )(exec: I ~> O, execStream: fs2.Stream[I, ?] ~> fs2.Stream[O, ?]): LeagueMembers[O] =
    new LeagueMembers[O] {
      override def join(roster: RosterId, league: LeagueId, from: RoundId): O[Unit] =
        exec(lm.join(roster, league, from))

      override def membersCount(leagueId: LeagueId): O[Long] = exec(lm.membersCount(leagueId))

      override def consists(roster: RosterId): O[List[Member]] = exec(lm.consists(roster))
    }
}
