package fantasy.domain.league

import cats.Monad
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.applicative._
import fantasy.ApiError.{ApiFieldError, ApiValidationError, ThrowableDetailedApiError}
import fantasy.{ApiError, ApiErrorRaise}
import fantasy.domain.account.AccountId
import fantasy.domain.league.LeagueController.LeagueError
import fantasy.domain.league.LeagueService.{LeagueInfo, LeagueRow, UserLeague}
import fantasy.domain.roster.{RosterId, UserRoster}
import fantasy.domain.series.{RoundId, RoundService}
import fantasy.http.Validator
import fantasy.typeclass.Raise
import fantasy.util.http.{Page, PageRequest}
import fantasy.typeclass.Raise.syntax._
import fs2.Stream

class LeagueController[F[_]: Stream.Compiler[*[_], F]: Monad: ApiErrorRaise: Raise[*[_], LeagueError]: UserRoster](
    leagueService: LeagueService[F],
    roundService: RoundService[F]
) {

  import Validator.syntax._
  import LeagueController._
  import fantasy.util.i18n.Messages.validations._

  def info(id: LeagueId, accountId: AccountId): F[LeagueInfo] =
    for {
      roster <- UserRoster[F].id(accountId)
      league <- leagueService.info(id)
    } yield league

  def join(id: LeagueId, accountId: AccountId): F[Unit] =
    for {
      roster   <- UserRoster[F].id(accountId)
      membered <- leagueService.isMember(id, roster)
      _        <- AlreadyMember.raise.whenA(membered)
      _        <- leagueService.joinToLeague(id, accountId)
    } yield ()

  def table(
      id: LeagueId,
      accountId: AccountId,
      rosterId: Option[RosterId],
      page: Int,
      pageSize: Int
  ): F[Page[LeagueRow]] =
    for {
      league  <- leagueService.info(id)
      roster  <- UserRoster[F].id(accountId)
      pagereq <- PageRequest.fill[F](page, pageSize)
      rows    <- rosterId.map(userAsRows(_, id)).getOrElse(leagueTable(id, pagereq))
    } yield rows

  def available(accountId: AccountId, page: Int, pageSize: Int): F[Page[LeagueInfo]] =
    for {
      pageReq <- PageRequest.fill(page, pageSize)
      leagues <- leagueService.availableLeagues(accountId)
      res     = Page.create(pageReq.take(leagues), pageReq, leagues.size.toLong)
    } yield res

  def create(accountId: AccountId, body: CreateLeagueRequest): F[Unit] =
    for {
      _         <- body.valid
      nextRound <- roundService.nextRound()
      _ <- ApiValidationError(ApiFieldError("round", InvalidValue)).raise
            .whenA(body.started.value <= nextRound.map(_.number.value).getOrElse(0L))
      _ <- ApiValidationError(ApiFieldError("type", InvalidValue)).raise
            .whenA(body.`type` == LeagueType.System)
      _ <- leagueService.create(Some(accountId), body.name, body.started, body.`type`)
    } yield ()

  def rosterLeagues(accountId: AccountId, id: RosterId): F[List[UserLeague]] = leagueService.userLeagues(id)

  def userAsRows(id: RosterId, league: LeagueId): F[Page[LeagueRow]] =
    leagueService.userInTable(league, id).map(Page.single)

  def leagueTable(id: LeagueId, pageRequest: PageRequest): F[Page[LeagueRow]] = {
    val res = leagueService.leagueTable(id, pageRequest)
    Page.fromPageable(res, pageRequest)
  }

}

object LeagueController {

  import Validator.helper._
  import fantasy.util.i18n.Messages.validations._

  case class CreateLeagueRequest(name: String, started: RoundId, `type`: LeagueType)

  sealed trait LeagueError  extends ApiError
  case object AlreadyMember extends ThrowableDetailedApiError(400, "already member", "league.memberAlready")

  implicit def createLaegueRequestValidator[F[_]: Monad]: Validator[F, CreateLeagueRequest] =
    build[F, CreateLeagueRequest].apply(
      r => r.name.nonEmpty.error(ApiFieldError("name", RequiredField)),
      r => (r.started.value > 0).error(ApiFieldError("started", InvalidValue))
    )

}
