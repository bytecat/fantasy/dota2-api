package fantasy.domain.league

import cats.instances.list._
import cats.instances.string._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.order._
import cats.syntax.traverse._
import cats.{Applicative, Defer, Eval, Functor, Monad}
import fantasy.ApiError.ThrowableApiError
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.account.{AccountId, AccountService}
import fantasy.domain.common.CountriesRepository
import fantasy.domain.league.LeagueService.LeagueError
import fantasy.domain.roster.{RosterId, RostersRepository, UserRoster}
import fantasy.domain.series.{RoundId, RoundService}
import fantasy.domain.team.TeamsRepository
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import fantasy.util.db.Pageable
import fantasy.util.http.PageRequest
import fantasy.util.i18n.Language
import fantasy.{ApiError, ApiErrorRaise}
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

trait LeagueService[F[_]] {
  import fantasy.domain.league.LeagueService._

  def availableLeagues(id: AccountId): F[List[LeagueInfo]]
  def userLeagues(id: RosterId): F[List[UserLeague]]
  def leagueTable(id: LeagueId, page: PageRequest): Pageable[F, LeagueRow]
  def userInTable(id: LeagueId, accountId: RosterId): F[LeagueRow]
  def joinToLeague(id: LeagueId, accountId: AccountId): F[Unit]
  def info(id: LeagueId): F[LeagueInfo]
  def isMember(id: LeagueId, rosterId: RosterId): F[Boolean]
  def create(owner: Option[AccountId], name: String, started: RoundId, `type`: LeagueType): F[LeagueId]
  def availableSystemLeagues(id: RosterId): F[List[League]]

}

class LeagueServiceF[F[_]: Logging: Monad: Raise[*[_], LeagueError]: ApiErrorRaise: UserRoster: CountriesRepository](
    leagues: LeagueRepository[F],
    table: LeagueTableRepository[F],
    members: LeagueMembers[F],
    roundService: RoundService[F],
    rosterService: RostersRepository[F],
    accountService: AccountService[F],
    teamRepository: TeamsRepository[F]
) extends LeagueService[F] {

  import fantasy.domain.league.LeagueService._

  override def availableLeagues(id: AccountId): F[List[LeagueInfo]] =
    for {
      publicLeagues <- leagues.byType(LeagueType.Public)
      roster        <- UserRoster[F].id(id)
      member        <- members.consists(roster).map(_.map(_.league))
      current       <- roundService.current()
      filtered      = publicLeagues.filter(l => !member.contains(l.id) && l.started > current.number)
      info <- filtered.traverse(
               l => members.membersCount(l.id).map(cnt => LeagueInfo(l.id, l.name, l.started, l.`type`, cnt, l.author))
             )
    } yield info

  override def userLeagues(id: RosterId): F[List[UserLeague]] =
    for {
      leaguesList <- members.consists(id).map(_.map(_.league))
      userRow     <- leaguesList.traverse(lid => table.get(id, lid).map(lid -> _))
      userLeagues <- userRow.traverse {
                      case (leagueId, row) =>
                        for {
                          league       <- leagues.get(leagueId)
                          _            <- error"not found league with id $leagueId".whenA(league.isEmpty)
                          membersCount <- members.membersCount(leagueId)
                          lastRound    = row.flatMap(lastRoundInRow)
                          prevRound = row.flatMap(
                            r => lastRound.flatMap(lr => lastRoundInRow(r.copy(places = r.places - lr)))
                          )
                          place     = row.flatMap(r => lastRound.flatMap(r.places.get))
                          prevPlace = row.flatMap(r => prevRound.flatMap(r.places.get))
                          change    = prevPlace.flatMap(prev => place.map(prev - _)).filterNot(_ == 0)
                          info      = league.map(l => LeagueInfo(l.id, l.name, l.started, l.`type`, membersCount, l.author))
                        } yield info.map(UserLeague(_, place, change))
                    }
    } yield userLeagues.flatten

  override def leagueTable(id: LeagueId, page: PageRequest): Pageable[F, LeagueRow] =
    Pageable(
      table
        .get(id, page)
        .flatMap(_.traverse { row =>
          for {
            roster    <- rosterService.get(row.roster).flatMap(_.liftTo[F](NotFoundRoster))
            account   <- accountService.get(roster.accountId)
            lastRound = lastRoundInRow(row)
          } yield
            LeagueRow(
              lastRound.flatMap(row.places.get),
              LeagueRoster(roster.id, account.name, roster.name),
              lastRound.flatMap(row.points.get).getOrElse(CalculatedPointValue(0)),
              row.totalPoints
            )
        }),
      table.count(id),
      Eval.later(leagueTable(id, page.next))
    )

  override def userInTable(id: LeagueId, rosterId: RosterId): F[LeagueRow] =
    for {
      row       <- table.get(rosterId, id).flatMap(_.liftTo[F](UserIsNotInLeague))
      roster    <- rosterService.get(rosterId).flatMap(_.liftTo[F](NotFoundRoster))
      account   <- accountService.get(roster.accountId)
      lastRound = lastRoundInRow(row)
    } yield
      LeagueRow(
        lastRound.flatMap(row.places.get),
        LeagueRoster(roster.id, account.name, roster.name),
        lastRound.flatMap(row.points.get).getOrElse(CalculatedPointValue(0)),
        row.totalPoints
      )

  override def joinToLeague(id: LeagueId, accountId: AccountId): F[Unit] =
    for {
      _         <- info"user $accountId connected to $id league"
      roster    <- UserRoster[F].id(accountId)
      nextRound <- roundService.nextRound().flatMap(_.liftTo[F](CantJoinToLeague(id, roster)))
      _         <- members.join(roster, id, nextRound.number)
      _         <- table.create(LeagueTableRow(roster, id, Map(), Map(), CalculatedPointValue(0)))
    } yield ()

  override def info(id: LeagueId): F[LeagueInfo] =
    for {
      league       <- leagues.get(id).flatMap(_.liftTo[F](LeagueNotFound(id)))
      membersCount <- members.membersCount(id)
    } yield LeagueInfo(league.id, league.name, league.started, league.`type`, membersCount, league.author)

  override def isMember(id: LeagueId, rosterId: RosterId): F[Boolean] =
    members.consists(rosterId).map(_.map(_.league).contains(id))

  private def lastRoundInRow(row: LeagueTableRow) =
    if (row.places.isEmpty) {
      None
    } else {
      Some(row.places.keys.maxBy(_.value))
    }

  override def create(owner: Option[AccountId], name: String, started: RoundId, `type`: LeagueType): F[LeagueId] =
    for {
      id <- leagues.create(name, started, `type`, owner)
      _  <- owner.map(joinToLeague(id, _)).getOrElse(Applicative[F].unit)
    } yield id

  override def availableSystemLeagues(id: RosterId): F[List[League]] =
    for {
      leagues         <- leagues.byType(LeagueType.System)
      nextRound       <- roundService.nextRound()
      nextRoundLeague = nextRound.map(_.number).flatMap(id => leagues.find(_.name === League.roundLeague(id)))
      overall         = leagues.find(_.name === League.OverallLeague)
      roster          <- rosterService.get(id).flatMap(_.liftTo[F](NotFoundRoster))
      account         <- accountService.get(roster.accountId)
      country         <- CountriesRepository.get[F](account.countryId)
      countryLeague   = country.name.get(Language.EN).flatMap(n => leagues.find(_.name === n))
      team            <- teamRepository.get(roster.fanTeam)
      teamLeague      = team.flatMap(t => leagues.find(_.name === t.name))
    } yield List(nextRoundLeague, overall, countryLeague, teamLeague).flatten
}

object LeagueService {

  @derive(encoder, swagger)
  case class UserLeague(league: LeagueInfo, place: Option[Long], changePlace: Option[Long])

  @derive(encoder, swagger)
  case class LeagueRoster(id: RosterId, account: String, name: String)

  @derive(encoder, swagger)
  case class LeagueRow(
      place: Option[Long],
      roster: LeagueRoster,
      lastRoundPoint: CalculatedPointValue,
      totalPoints: CalculatedPointValue
  )

  @derive(encoder, swagger)
  case class LeagueInfo(
      id: LeagueId,
      name: String,
      started: RoundId,
      `type`: LeagueType,
      participants: Long,
      owner: Option[AccountId]
  )

  trait LeagueError extends ApiError

  case object UserIsNotInLeague           extends ThrowableApiError(404, "User is not in league") with LeagueError
  case class LeagueNotFound(id: LeagueId) extends ThrowableApiError(404, s"League $id not found") with LeagueError
  case object NotFoundRoster              extends ThrowableApiError(404, "User not create league") with LeagueError
  case class CantJoinToLeague(id: LeagueId, roster: RosterId)
      extends ThrowableApiError(500, "Not found next round for joining")

  def create[I[_]: Functor, F[_]: Defer: Monad: Raise[*[_], LeagueError]: ApiErrorRaise: UserRoster: CountriesRepository: Logs[
    I,
    *[_]
  ]](
      leagues: LeagueRepository[F],
      table: LeagueTableRepository[F],
      members: LeagueMembers[F],
      roundService: RoundService[F],
      rosterService: RostersRepository[F],
      accountService: AccountService[F],
      teamsRepository: TeamsRepository[F]
  ): I[LeagueService[F]] =
    Logs[I, F]
      .forService[LeagueServiceF[F]]
      .map(
        implicit l =>
          new LeagueServiceF[F](leagues, table, members, roundService, rosterService, accountService, teamsRepository)
      )

}
