package fantasy.domain.league

import cats.tagless.FunctorK
import fantasy.domain.roster.RosterId
import fantasy.util.http.PageRequest
import simulacrum.typeclass

@typeclass
trait LeagueTableRepository[F[_]] {
  def get(roster: RosterId, league: LeagueId): F[Option[LeagueTableRow]]
  def get(roster: RosterId): F[List[LeagueTableRow]]
  def get(leagueId: LeagueId, page: PageRequest): F[List[LeagueTableRow]]
  def all(leagueId: LeagueId): F[List[LeagueTableRow]]
  def count(leagueId: LeagueId): F[Long]
  def delete(roster: RosterId, league: LeagueId): F[Unit]
  def create(row: LeagueTableRow): F[Unit]
  def update(row: LeagueTableRow): F[Unit]
}

object LeagueTableRepository {
  implicit val fk: FunctorK[LeagueTableRepository] = cats.tagless.Derive.functorK
}
