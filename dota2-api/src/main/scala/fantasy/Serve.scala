package fantasy

import cats.data.NonEmptyList
import cats.tagless.implicits._
import com.typesafe.scalalogging.StrictLogging
import doobie.free.connection.ConnectionIO
import doobie.syntax.connectionio._
import doobie.syntax.stream._
import doobie.util.transactor.Transactor
import fantasy.context.{CoreConfig, ProcessContext}
import fantasy.domain.account._
import fantasy.dota2._
import fantasy.dota2.boot.{Arbiter, Repositories, Services}
import fantasy.engine.GameArbiter
import fantasy.http._
import fantasy.opendota.DeferFinagleFuture
import fantasy.util.i18n.{Language, LocalizeProvider, ResourceBundleProvider, Type}
import monix.eval.Task
import monix.execution.Scheduler
import tofu.env.Env
import tofu.lift.Lift
import tofu.logging.{LoggableContext, Logging, Logs}

import scala.concurrent.Promise

class Serve(config: CoreConfig)(implicit db: Transactor[Task]) extends StrictLogging {

  type DFR = Repositories[FantasyTask, D2Position, D2Point]
  type DFS = Services[FantasyTask, D2Position, D2Point]

  import Serve._
  import runnable._
  implicit val tokenService: TokenService[Task] = TokenService.jwt[Task](config.secret)

  def run(): Task[Unit] =
    for {
      mods                                                <- modules
      implicit0(s: Scheduler)                             = Scheduler.forkJoin(Runtime.getRuntime.availableProcessors() * 2,
        Runtime.getRuntime.availableProcessors() * 4, "main-scheduler")
      implicit0(l: Logging[Task])                         <- Logs.sync[Task, Task].forService[Serve]
      implicit0(err: LocalizeProvider[Task, Type.Errors]) <- ResourceBundleProvider.create[Task, Task, Type.Errors](config.http.errorsBundle)
      server                                              = new HttpSchema(schema(config), mods)
      _                                                   <- server.run[Task].forkAndForget
      _                                                   <- Task.delay(logger.info("Startup server on {}:{}", config.http.host, config.http.port))
      _                                                   <- Task.never[Unit]
    } yield ()

  def modules: Task[NonEmptyList[FantasyModule[Http]]] = {
    implicit val logs: Logs[Task, FantasyTask] = Logs.withContext[Task, FantasyTask]

    val modulesList = for {
      implicit0(reps: DFR)    <- repositories
      implicit0(service: DFS) <- Services.create[Task, FantasyTask](config)
      m <- boot
            .modules[Task, FantasyTask, Http]
            .build(config, reps, service, tokenService.mapK(Lift[Task, FantasyTask].liftF))
      _ <- startGame
    } yield m

    modulesList
  }

  def schema(config: CoreConfig): HttpSchema.Config =
    HttpSchema.Config(config.http.host, config.http.port, "Dota 2 API", "0.1")

  def startGame(
      implicit reps: DFR,
      service: DFS,
      L: Logs[Task, FantasyTask]
  ): Task[GameArbiter[D2Position, D2Point, FantasyTask]] = {
    implicit val lift: Lift[FantasyTask, Task] = new Lift[FantasyTask, Task] {
      override def lift[A](fa: FantasyTask[A]): Task[A] = fa.run(ProcessContext.create(Language.default))
    }
    Arbiter.run[Task, FantasyTask](config)
  }

  def repositories: Task[DFR] =
    Repositories
      .create[Task]
      .map(
        _.mapK(`Lift[ConnectionIO, FantasyTask]`.liftF, `Lift[fs2.Stream[ConnectionIO], fs2.Stream[FantasyTask]`.liftF)
      )

  implicit val `Lift[ConnectionIO, FantasyTask]` : Lift[ConnectionIO, FantasyTask] =
    new Lift[ConnectionIO, FantasyTask] {
      override def lift[A](fa: ConnectionIO[A]): FantasyTask[A] = FantasyTask.deferTask(fa.transact(db))
    }

  implicit val `Lift[FantasyTask, Http]` : Lift[FantasyTask, Http] = new Lift[FantasyTask, Http] {
    override def lift[A](fa: FantasyTask[A]): Http[A] = Http.context.flatMap(c => Http.deferTask(fa.run(c.embedded)))
  }

  implicit val `Lift[fs2.Stream[ConnectionIO], fs2.Stream[FantasyTask]` : LiftStream[ConnectionIO, FantasyTask] =
    new LiftStream[ConnectionIO, FantasyTask] {
      override def lift[A](fa: fs2.Stream[ConnectionIO, A]): fs2.Stream[FantasyTask, A] =
        fa.transact(db).translate(Lift[Task, FantasyTask].liftF)
    }

}

object Serve {

  implicit val finagleToFantasy: DeferFinagleFuture[FantasyTask] = new DeferFinagleFuture[FantasyTask] {
    override def defer[T](future: => com.twitter.util.Future[T]): FantasyTask[T] =
      Env.deferFuture {
        val promise = Promise[T]
        future.respond(promise complete _.asScala)
        promise.future
      }
  }

  implicit val loggableContext: LoggableContext[FantasyTask] = LoggableContext.of[FantasyTask].instance

}
