package fantasy.modules

import cats.{Functor, Monad}
import fantasy.http.FantasyModule
import fantasy.util.i18n.Language
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class SystemModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: SystemController[G])
    extends FantasyModule[F] {

  import SystemModule._

  override def route = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

trait SystemController[F[_]] {
  import SystemModule._
  def points(language: Language): F[List[PointResponse]]
  def languages(): F[List[Language]]
  def positions(): F[List[PositioResponse]]
}

object SystemModule {
  def api = tagPrefix('system) |> (points <|> langs <|> positions)

  def points    = keyPrefix('points) |> get |> queryParam[Language]('language) |> $$[List[PointResponse]]
  def langs     = keyPrefix('languages) |> get |> $$[List[Language]]
  def positions = keyPrefix('positions) |> get |> $$[List[PositioResponse]]

  @derive(encoder, swagger)
  final case class PointResponse(id: Int, positve: Boolean, bonus: Boolean, name: String)

  @derive(encoder, swagger)
  final case class PositioResponse(id: Int, name: String)
}
