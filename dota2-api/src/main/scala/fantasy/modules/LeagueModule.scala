package fantasy.modules

import cats.{Functor, Monad}
import com.twitter.finagle.http.Response
import fantasy.domain.account.AuthorizeService
import fantasy.domain.league.LeagueController.CreateLeagueRequest
import fantasy.domain.league.LeagueService.{LeagueInfo, LeagueRow, UserLeague}
import fantasy.domain.league.{LeagueController, LeagueId}
import fantasy.domain.roster.RosterId
import fantasy.http.FantasyModule
import fantasy.util.http.Page
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class LeagueModule[F[_]: Monad: RoutedPlus: AuthorizeService, G[_]: LiftHttp[F, *[_]]: Functor](
    controller: LeagueController[G]
) extends FantasyModule[F] {

  import LeagueModule._
  import fantasy.util.swagger.auth._

  override def route: F[Response] = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object LeagueModule {

  import fantasy.util.swagger.auth._
  import fantasy.util.swagger.pageable._

  def api = leagues <> rosterLeagues

  def leagues = tagPrefix('leagues) |> (info <> join <> table <> available)

  def info = key('info) |> capture[LeagueId]('id) |> jwt('accountId) |> get |> $$[LeagueInfo]
  def join = capture[LeagueId]('id) |> jwt('accountId) |> operation('join) |> post |> $$[Unit]
  def table =
    capture[LeagueId]('id) |> jwt('accountId) |> operation('table) |> queryParam[Option[RosterId]]('rosterId) |> page |> get |> $$[
      Page[LeagueRow]
    ]

  def available = jwt('accountId) |> operation('available) |> page |> get |> $$[Page[LeagueInfo]]

  def create = key('create) |> jwt('accountId) |> reqBody[CreateLeagueRequest] |> post |> $$[Unit]

  def rosterLeagues =
    tagPrefix('rosters) |> jwt('accountId) |> capture[RosterId]('id) |> prefix('leagues) |> key('rosterLeagues) |> get |> $$[
      List[UserLeague]
    ]
}
