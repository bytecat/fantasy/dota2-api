package fantasy.modules

import cats.{Functor, Monad}
import fantasy.domain.team.{Team, TeamController, TeamId}
import fantasy.http.FantasyModule
import fantasy.util.swagger.pageable._
import fantasy.util.http.Page
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class TeamsModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: TeamController[G])
    extends FantasyModule[F] {

  import TeamsModule._

  override def route = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object TeamsModule {

  def api = tagPrefix('teams) |> (getTeam <> findTeams)

  def findTeams = key('find) |> queryParam[Option[String]]('name) |> page |> get |> $$[Page[Team]]

  def getTeam = key('team) |> capture[TeamId]('id) |> get |> $$[Team]
}
