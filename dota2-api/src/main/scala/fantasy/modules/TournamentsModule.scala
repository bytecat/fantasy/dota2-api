package fantasy.modules

import java.time.LocalDate

import cats.{Functor, Monad}
import fantasy.domain.common.CountriesController.CountryResponse
import fantasy.domain.series.{TournamentController, TournamentId}
import fantasy.http.FantasyModule
import fantasy.util.http.Page
import fantasy.util.swagger.pageable._
import org.manatki.derevo.circeDerivation.encoder
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger.{SwaggerBuilder, _}
import ru.tinkoff.tschema.syntax._

class TournamentsModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](service: TournamentController[G])
    extends FantasyModule[F] {
  import TournamentsModule._

  override def route = MkService[F](api)(service)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object TournamentsModule {
  def api         = tournaments
  def tournaments = tagPrefix('tournaments) |> (byId <> pageable)
  def byId        = key('get) |> capture[TournamentId]('id) |> get |> $$[TournamentResponse]
  def pageable    = key('all) |> page |> get |> $$[Page[TournamentResponse]]

  @derive(swagger, encoder)
  case class TournamentResponse(
      id: TournamentId,
      name: String,
      startDate: LocalDate,
      endDate: LocalDate,
      country: CountryResponse,
      logo: Option[String]
  )
}
