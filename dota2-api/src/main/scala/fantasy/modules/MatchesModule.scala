package fantasy.modules

import cats.{Functor, Monad}
import fantasy.domain.`match`.MatchController.{MatchPointsResponse, MatchResponse}
import fantasy.domain.`match`.{Match, MatchController, MatchId}
import fantasy.domain.player.PlayerId
import fantasy.domain.series.SeriesId
import fantasy.http.FantasyModule
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class MatchesModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: MatchController[G])
    extends FantasyModule[F] {

  import MatchesModule._

  override def route = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object MatchesModule {

  def api = matches <> playerPoints <> bySeries

  def matches = tagPrefix('matches) |> (getMatch <> matchPoints)

  def getMatch = capture[MatchId]('id) |> key('getMatch) |> get |> $$[MatchResponse]

  def matchPoints =
    capture[MatchId]('id) |> prefix('points) |> key('matchPoints) |> get |> $$[List[MatchPointsResponse]]

  def playerPoints =
    tagPrefix('players) |> capture[PlayerId]('id) |> prefix('matches) |> capture[MatchId]('matchId) |> prefix('points) |> key(
      'playerPoints
    ) |> get |> $$[MatchPointsResponse]

  def bySeries =
    tagPrefix('series) |> capture[SeriesId]('id) |> prefix('matches) |> key('bySeries) |> get |> $$[List[Match]]

}
