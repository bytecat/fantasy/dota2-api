package fantasy.modules

import cats.{Functor, Monad}
import fantasy.domain.player.PlayerId
import fantasy.domain.series.SeriesController.{PlayerSeries, SeriesResponse, TeamSeries}
import fantasy.domain.series.{RoundId, SeriesController, SeriesId}
import fantasy.domain.team.TeamId
import fantasy.http.FantasyModule
import fantasy.util.http.Page
import fantasy.util.swagger.pageable._
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger.{SwaggerBuilder, _}
import ru.tinkoff.tschema.syntax._

class SeriesModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: SeriesController[G])
    extends FantasyModule[F] {

  import SeriesModule._

  override def route = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object SeriesModule {

  def api = playerLastSeries <> series <> byRound <> teamSeries

  def series = tagPrefix('series) |> (getSeries <> allSeries)

  def byRound =
    tagPrefix('rounds) |> capture[RoundId]('round) |> prefix('series) |> key('roundSeries) |> get |> $$[List[
      SeriesResponse
    ]]

  def playerLastSeries =
    tagPrefix('players) |> capture[PlayerId]('id) |> prefix('series) |> key('playerSeries) |> page |> get |> $$[Page[
      PlayerSeries
    ]]

  def teamSeries =
    tagPrefix('teams) |> capture[TeamId]('id) |> prefix('series) |> key('teamSeries) |> page |> get |> $$[Page[
      TeamSeries
    ]]

  def getSeries = capture[SeriesId]('id) |> key('series) |> get |> $$[SeriesResponse]

  def allSeries = key('allSeries) |> get |> page |> $$[Page[SeriesResponse]]

}
