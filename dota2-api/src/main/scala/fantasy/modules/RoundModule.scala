package fantasy.modules

import cats.{Functor, Monad}
import fantasy.domain.series.{Round, RoundId, RoundService}
import fantasy.http.FantasyModule
import ru.tinkoff.tschema.swagger.SwaggerBuilder
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class RoundModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](rounds: RoundService[G])
    extends FantasyModule[F] {

  import RoundModule._

  override def route = MkService[F](api)(rounds)

  override def swagger: SwaggerBuilder = api.mkSwagger

}

object RoundModule {
  def api    = rounds
  def rounds = tagPrefix('rounds) |> (getById <> current <> next)

  def getById = key('get) |> capture[RoundId]('round) |> get |> $$[Round]

  def current = operation('current) |> get |> $$[Round]

  def next = operation('next) |> get |> $$[Round]
}
