package fantasy.modules

import cats.{Functor, Monad}
import fantasy.domain.player.{PlayerController, PlayerId}
import fantasy.domain.player.PlayerController.model.PlayerResponse
import fantasy.domain.team.TeamId
import fantasy.http.FantasyModule
import fantasy.util.swagger.pageable._
import fantasy.util.http.{Direction, Page}
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class PlayerModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: PlayerController[G])
    extends FantasyModule[F] {

  import PlayerModule._

  override def route = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object PlayerModule {

  def api = players

  def players = tagPrefix('players) |> (findPlayers <> player)

  def findPlayers =
    key('find) |> queryParam[Option[String]]('name) |> queryParam[Option[Double]]('maxPrice) |> queryParam[Option[
      Double
    ]]('minPrice) |> queryParam[Option[TeamId]]('team) |> queryParam[Option[Int]]('position) |> queryParam[Option[Int]](
      'sortBy
    ) |> queryParam[Option[Direction]]('sortDirection) |> queryParam[Option[Boolean]]('hasMatches) |> page |> get |> $$[
      Page[PlayerResponse]
    ]

  def player = key('player) |> capture[PlayerId]('id) |> get |> $$[PlayerResponse]

}
