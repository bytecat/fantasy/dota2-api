package fantasy.modules

import cats.{Functor, Monad}
import com.twitter.finagle.http.Response
import fantasy.domain.account.{AccountController, AuthorizeService}
import fantasy.domain.account.AccountController._
import fantasy.domain.account.AuthorizeService.Token
import fantasy.http.FantasyModule
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

final class AccountModule[F[_]: Monad: RoutedPlus: AuthorizeService, G[_]: LiftHttp[F, *[_]]: Functor](
    controller: AccountController[G]
) extends FantasyModule[F] {
  import AccountModule._
  import fantasy.util.swagger.auth._

  override def route: F[Response] = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object AccountModule {
  import fantasy.util.swagger.auth._

  def api = accounts <|> auth

  def accounts = tagPrefix('accounts) |> (create <|> getMe)

  def auth = tagPrefix('authorize) |> signIn

  def signIn = prefix("sign-in") |> key('signIn) |> reqBody[SignInRequest] |> post |> $$[Token]

  def create = key('create) |> reqBody[SignUpRequest] |> post |> $$[Token]

  def getMe = prefix("me") |> key('get) |> jwt('id) |> get |> $$[AccountResponse]

}
