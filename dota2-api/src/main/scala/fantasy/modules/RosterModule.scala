package fantasy.modules

import cats.{Functor, Monad}
import com.twitter.finagle.http.Response
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.account.AuthorizeService
import fantasy.domain.common.CountriesController.CountryResponse
import fantasy.domain.player.PlayerId
import fantasy.domain.roster.{RosterController, RosterId}
import fantasy.domain.series.{NextPlayerSeries, RoundId}
import fantasy.domain.team.{Team, TeamId}
import fantasy.http.FantasyModule
import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class RosterModule[F[_]: Monad: RoutedPlus: AuthorizeService, G[_]: LiftHttp[F, *[_]]: Functor](
    controller: RosterController[G]
) extends FantasyModule[F] {
  import RosterModule._
  import fantasy.util.swagger.auth._

  override def route: F[Response] = MkService[F](api)(controller)

  override def swagger: SwaggerBuilder = api.mkSwagger
}

object RosterModule {

  import fantasy.util.swagger.auth._

  @derive(swagger, decoder)
  case class CreateRosterRequest(name: String, players: List[PlayerId], captain: PlayerId, fanTeam: TeamId)

  @derive(swagger, decoder)
  case class UpdateRosterRequest(players: List[PlayerId], captain: PlayerId)

  @derive(swagger, decoder)
  case class UpdateSettingsRequest(name: String)

  @derive(swagger, encoder)
  case class RosterHistoryPlayer(
      id: PlayerId,
      nick: String,
      team: Option[Team],
      position: Int,
      points: CalculatedPointValue,
      country: CountryResponse,
      inDreamTeam: Boolean
  )

  @derive(swagger, encoder)
  case class RosterPlayer(
      id: PlayerId,
      nick: String,
      team: Option[Team],
      position: Int,
      country: CountryResponse,
      price: Double,
      nextMatches: List[NextPlayerSeries]
  )

  @derive(swagger, encoder)
  case class RosterHistoryResponse(
      id: RosterId,
      name: String,
      round: RoundId,
      players: List[RosterHistoryPlayer],
      captain: PlayerId,
      total: CalculatedPointValue
  )

  @derive(swagger, encoder)
  case class RosterResponse(
      id: RosterId,
      players: List[RosterPlayer],
      captain: PlayerId,
      availableMoney: Double,
      name: String,
      fanTeam: Team
  )

  def api = tagPrefix('rosters) |> (create <> updateSettings <> updateSquad <> histories <> getRoster)

  def create = key('create) |> jwt('accountId) |> reqBody[CreateRosterRequest] |> post |> $$[Unit]

  def updateSquad =
    capture[RosterId]('id) |> keyPrefix('squad) |> jwt('accountId) |> reqBody[UpdateRosterRequest] |> put |> $$[Unit]

  def updateSettings =
    key('updateSettings) |> capture[RosterId]('id) |> jwt('accountId) |> reqBody[UpdateSettingsRequest] |> put |> $$[
      Unit
    ]

  def histories =
    capture[RosterId]('id) |> keyPrefix('histories) |> capture[RoundId]('round) |> get |> $$[RosterHistoryResponse]

  def getRoster = capture[RosterId]('id) |> key('get) |> jwt('accountId) |> get |> $$[RosterResponse]

}
