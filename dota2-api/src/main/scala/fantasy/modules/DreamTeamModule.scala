package fantasy.modules

import cats.{Functor, Monad}
import com.twitter.finagle.http.Response
import fantasy.domain.dreamteam.DreamTeamController
import fantasy.domain.dreamteam.DreamTeamController.DreamTeamResponse
import fantasy.domain.series.RoundId
import fantasy.http.FantasyModule
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class DreamTeamModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: DreamTeamController[G])
    extends FantasyModule[F] {

  override def route: F[Response] = MkService[F](DreamTeamModule.dreamteam)(controller)

  override def swagger: SwaggerBuilder = DreamTeamModule.dreamteam.mkSwagger
}

object DreamTeamModule {

  def dreamteam = tagPrefix('dreamteam) |> key('get) |> capture[RoundId]('round) |> get |> $$[DreamTeamResponse]

}
