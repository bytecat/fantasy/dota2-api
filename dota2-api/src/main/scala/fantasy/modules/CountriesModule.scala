package fantasy.modules

import cats.{Functor, Monad}
import com.twitter.finagle.http.Response
import fantasy.domain.common.CountriesController.CountryResponse
import fantasy.domain.common.{CountriesController, CountryId}
import fantasy.http.FantasyModule
import fantasy.util.i18n.Language
import ru.tinkoff.tschema.finagle.{LiftHttp, MkService, RoutedPlus}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

class CountriesModule[F[_]: Monad: RoutedPlus, G[_]: LiftHttp[F, *[_]]: Functor](controller: CountriesController[G])
    extends FantasyModule[F] {

  override def route: F[Response] =
    MkService[F](CountriesModule.api)(controller)

  override def swagger: SwaggerBuilder = CountriesModule.api.mkSwagger
}

object CountriesModule {
  def api = tagPrefix('countries) |> (byId <> all)

  def all = key('countries) |> queryParam[Language]('lang) |> get |> $$[List[CountryResponse]]

  def byId = key('country) |> capture[CountryId]('id) |> queryParam[Language]('lang) |> get |> $$[CountryResponse]
}
