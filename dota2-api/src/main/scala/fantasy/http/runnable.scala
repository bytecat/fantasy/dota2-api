package fantasy.http

import java.sql.SQLException

import cats.syntax.apply._
import cats.syntax.applicative._
import com.twitter.finagle.{Service, http}
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.util.Promise
import fantasy.ApiError.ApiValidationError
import fantasy.Error.FieldError
import fantasy._
import fantasy.context.{ProcessContext, TraceId}
import fantasy.domain.account.{AccountId, TokenService}
import fantasy.util.i18n.{Language, LocalizeProvider, Type}
import io.circe.Printer
import monix.eval.Task
import monix.execution.Scheduler
import ru.tinkoff.tschema.finagle.routing.EnvRouting.EnvHttp
import ru.tinkoff.tschema.finagle.routing.{EnvRouting, Rejected}
import ru.tinkoff.tschema.finagle.{Rejection, Runnable}
import ru.tinkoff.tschema.utils.SubString
import tofu.logging.Logging
import tofu.syntax.logging._
import io.circe.syntax._
import ru.tinkoff.tschema.finagle.util.message

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object runnable {
  import logHttp._

  private val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  implicit def `Runnable[Http, Task]`(
      implicit T: TokenService[Task],
      S: Scheduler,
      L: Logging[Task],
      LP: LocalizeProvider[Task, Type.Errors]
  ): Runnable[Http, Task] =
    new Runnable[Http, Task] {
      override def run(fresp: Http[Response]): Task[Service[Request, Response]] =
        Task.now((r: http.Request) => toTwitter(execResponse(fresp, r).executeOn(S).runToFuture))
    }

  private def toTwitter[A](f: scala.concurrent.Future[A])(implicit EC: ExecutionContext): com.twitter.util.Future[A] = {
    val promise: Promise[A] = new Promise[A]()
    f.onComplete {
      case Success(value)     => promise.setValue(value)
      case Failure(exception) => promise.setException(exception)
    }
    promise
  }

  private[this] def execResponse(
      envResponse: EnvHttp[ProcessContext, Response],
      request: Request
  )(
      implicit T: TokenService[Task],
      L: Logging[Task],
      LP: LocalizeProvider[Task, Type.Errors]
  ): Task[Response] =
    for {
      ctx     <- extractContext(request)
      routing = EnvRouting(request, SubString(request.path), 0, ctx)
      _       <- info"received http request $request $ctx"
      res <- envResponse
              .run(routing)
              .onErrorRecover { case Rejected(rej) => Rejection.defaultHandler(rej) }
              .onErrorHandleWith(recoverError(LP, ctx, L)))
      _ <- info"processed with $res $ctx"
    } yield res

  private[this] def extractContext(request: Request)(implicit T: TokenService[Task]): Task[ProcessContext] =
    (extractTrace(request), extractLang(request), extractAccount(request)).mapN(ProcessContext.apply)

  private val EMPTY_TOKEN = Task.now(Option.empty[AccountId])

  private[this] def extractAccount(request: Request)(implicit T: TokenService[Task]): Task[Option[AccountId]] =
    request.headerMap
      .get("Authorization")
      .fold(EMPTY_TOKEN)(t => T.readToken(t).map(_.map(_.id)))

  private[this] def extractTrace(request: Request): Task[TraceId] =
    Task.delay(
      request.headerMap
        .get(HttpSchema.`X-Trace-Id`)
        .map(TraceId)
        .getOrElse(TraceId.create)
    )TraceId

  private val DEFAULT_LANG   = Task.now(Language.default)
  private val DEFAULT_HEADER = "Accept-Language"
  private val SPECIAL_HEADER = "WF-Accept-Language"

  private[this] def extractLang(request: Request): Task[Language] =
    request.headerMap
      .get(SPECIAL_HEADER)
      .fold(extractFromDefaultHeader(request))(
        v => Language.withNameInsensitiveOption(v).map(Task.now).getOrElse(extractFromDefaultHeader(request))
      )

  private[this] def extractFromDefaultHeader(request: Request): Task[Language] =
    request.headerMap
      .get(DEFAULT_HEADER)
      .fold(DEFAULT_LANG)(
        v =>
          Task.delay {
            v.split(",")
              .map(_.split(";"))
              .sortBy(q)
              .reverse
              .find(arr => langFromHeader(arr(0)).isDefined)
              .flatMap(arr => langFromHeader(arr(0)))
              .getOrElse(Language.default)
        }
      )

  private def langFromHeader(str: String) = {
    val index = str.indexOf('-')
    Language.withNameInsensitiveOption(if (index != -1) str.substring(0, index) else str)
  }

  private def q(arr: Array[String]): Double = {
    if (arr.length == 2) {
      val arr2 = arr(1).split(";")
      if (arr2.length == 2) {
        val arr3 = arr2(1).split("=")
        arr3(1).toDouble
      } else 1.0
    } else 1.0
  }

  private val InternalError = Task.pure(Response(Status.InternalServerError))

  private def recoverError(
      localize: LocalizeProvider[Task, Type.Errors],
      ctx: ProcessContext,
      logger: Logging[Task]
  )(
      err: Throwable
  ): Task[Response] = {
    implicit val L = logger
    err match {
      case e: DetailedApiError   => toPlainError(e, localize, ctx)
      case e: ApiValidationError => toValidationError(e, localize, ctx)
      case e: ApiError =>
        warn"failed processed request $ctx ${Logging.errorvalue(e)}".whenA(e.code >= 500) *> Response(Status(e.code))
          .pure[Task]
      case e: SQLException =>
        error"Sqlexception: ${e.getSQLState} $ctx ${Logging.errorvalue(e)}" *> InternalError
      case _ =>
        error"Not handled error ${Logging.errorvalue(err)} $ctx" *> InternalError
    }
  }

  def toPlainError(
      err: DetailedApiError,
      localize: LocalizeProvider[Task, Type.Errors],
      ctx: ProcessContext
  ): Task[Response] =
    localize.provide(ctx.language).flatMap { locale =>
      locale.fill(err.messageCode).map { msg =>
        val json = ErrorResponse.plain(msg, ctx.traceId).asJson
        val resp = message.jsonResponse(printer.pretty(json))
        resp.statusCode(err.code)
        resp
      }
    }

  def toValidationError[F[_]](
      err: ApiValidationError,
      localize: LocalizeProvider[Task, Type.Errors],
      ctx: ProcessContext
  ): Task[Response] =
    localize.provide(ctx.language).flatMap { locale =>
      err.errors.traverse { err =>
        locale.fill(err.messageCode).map(FieldError(err.field, _))
      }.map { fer =>
        val json = ErrorResponse(fer, ctx.traceId).asJson
        val resp = message.jsonResponse(printer.pretty(json))
        resp.statusCode(err.code)
        resp
      }
    }

}
