package fantasy.http

import cats.{Applicative, Functor, Monad}
import com.twitter.finagle.http.Response
import io.circe.{Decoder, Encoder, Printer}
import io.circe.parser.decode
import io.circe.syntax._
import ru.tinkoff.tschema.finagle.util.message
import ru.tinkoff.tschema.swagger.{MkSwagger, SwaggerBuilder}
import ru.tinkoff.tschema.finagle.{Complete, LiftHttp, ParseBody, Routed => FinRouted}
import ru.tinkoff.tschema.finagle.util.message.{jsonBodyParse, jsonComplete}

trait FantasyModule[F[_]] {

  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  implicit def circeEncodeComplete[A: Encoder](
      implicit F: Applicative[F]
  ): Complete[F, A, A] =
    jsonComplete(_.asJson.pretty(printer))

  implicit def circeEncodeCompleteF[G[_]: Functor, A: Encoder](
      implicit runnable: LiftHttp[F, G]
  ): Complete[F, A, G[A]] =
    message.fjsonComplete(_.asJson.pretty(printer))

  implicit def circeDecodeParseBody[A: Decoder](implicit F: Monad[F], R: FinRouted[F]): ParseBody[F, A] =
    jsonBodyParse(decode[A])

  def route: F[Response]
  def swagger: SwaggerBuilder = MkSwagger.empty
}
