package fantasy.http

import cats.Monad
import cats.data.NonEmptyList
import cats.syntax.either._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.applicative._
import fantasy.ApiError.{ApiFieldError, ApiValidationError}
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._

trait Validator[F[_], T] {
  def validate(t: T): F[Either[ApiValidationError, Unit]]
}

object Validator {

  object helper {
    type Rule[T] = T => Option[ApiFieldError]

    def build[F[_]: Monad, T]: ValidatorVia[F, T] = new ValidatorVia[F, T]

    implicit class RuleOps(val valid: Boolean) extends AnyVal {
      def error(err: => ApiFieldError): Option[ApiFieldError] =
        if (valid) {
          None
        } else Some(err)
    }

    class ValidatorVia[F[_]: Monad, T] {
      def apply(rule: Rule[T], rules: Rule[T]*): Validator[F, T] =
        SeqValidator(NonEmptyList.of(rule, rules: _*))
    }

    final case class SeqValidator[F[_]: Monad, T](rules: NonEmptyList[Rule[T]]) extends Validator[F, T] {
      override def validate(t: T): F[Either[ApiValidationError, Unit]] =
        rules.toList
          .map(r => r(t))
          .pure[F]
          .map(
            list =>
              list.flatten match {
                case e :: errors => ApiValidationError(NonEmptyList.of(e, errors: _*)).asLeft
                case _           => ().asRight
              }
          )

    }
  }

  object syntax {
    implicit class ValidatorOps[T](val self: T) extends AnyVal {
      def valid[F[_]: Monad](implicit R: Raise[F, ApiValidationError], V: Validator[F, T]): F[T] =
        V.validate(self).flatMap {
          case Left(err) => err.raise[F, T]
          case Right(_)  => self.pure[F]
        }
    }
  }
}
