package fantasy.http

import com.twitter.finagle.http.{Request, Response}
import cats.syntax.semigroup._
import tofu.logging.{DictLoggable, LogRenderer, Loggable}
import tofu.syntax.logRenderer._

object logHttp {

  implicit val `Loggable[Request]` : Loggable[Request] = new DictLoggable[Request] {
    override def fields[I, V, R, S](a: Request, i: I)(implicit r: LogRenderer[I, V, R, S]): R =
      i.addString("method", a.method.name) |+| i.addString("uri", a.uri) |+|
        i.subDict("headers") { ih =>
          a.headerMap.foldLeft(ih.noop)((acc, h) => acc |+| ih.addString(h._1, h._2))
        }

    override def logShow(a: Request): String = a.uri
  }

  implicit val `Loggable[Response]` : Loggable[Response] = new DictLoggable[Response] {
    override def fields[I, V, R, S](a: Response, i: I)(implicit r: LogRenderer[I, V, R, S]): R =
      i.addInt("status", a.statusCode.toLong) |+| i.subDict("headers") { ih =>
        a.headerMap.foldLeft(ih.noop)((acc, h) => acc |+| ih.addString(h._1, h._2))
      }

    override def logShow(a: Response): String = s"status = ${a.statusCode}"
  }
}
