package fantasy.http

import cats.data.NonEmptyList
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.foldable._
import cats.syntax.functor._
import cats.syntax.semigroupk._
import cats.{Monad, SemigroupK}
import com.twitter.finagle.http.Response
import io.circe.Printer
import io.circe.syntax._
import monix.eval.Task
import monix.execution.Scheduler
import ru.tinkoff.tschema.finagle.routing.Rejected
import ru.tinkoff.tschema.finagle.util.message
import ru.tinkoff.tschema.finagle.{Rejection, Routed => FRouted}
import ru.tinkoff.tschema.swagger.OpenApiInfo
import scalatags.Text.all._
import tofu.lift.Lift

object Swagger {

  val IO = Scheduler.io("resources")

  private implicit val printer: Printer = Printer.spaces2.copy(dropNullValues = true)

  private def swaggerHttp[F[_]: FRouted: Monad]: F[Response] = {
    val response = message.stringResponse(swaggerUI.index("/swagger").render)
    response.setContentType("text/html(UTF-8)")
    FRouted.checkPath[F, Response]("/swagger.php", response.pure[F])
  }

  private def resource[F[_]: Lift[Task, *[_]]](name: String): F[Response] =
    Lift[Task, F].lift(
      Task.delay {
        val BufSize  = 1024
        val response = Response()
        val stream   = getClass.getResourceAsStream(name)
        val arr      = Array.ofDim[Byte](BufSize)
        @scala.annotation.tailrec
        def readAll(): Unit =
          stream.read(arr) match {
            case BufSize =>
              response.write(arr)
              readAll()
            case size if size > 0 =>
              response.write(arr.slice(0, size))
              readAll()
            case _ =>
          }
        readAll()
        response
      }.executeOn(IO)
        .onErrorHandleWith(_ => Task.raiseError(Rejected(Rejection.notFound)))
    )

  private def swaggerResources[F[_]: FRouted: Monad: Lift[Task, *[_]]]: F[Response] =
    FRouted.path[F].map(_.toString).flatMap {
      case s if s.startsWith("/webjars") => resource("/META-INF/resources" + s)
      case _                             => FRouted.reject[F, Response](Rejection.notFound)
    }

  private def swaggerJson[F[_]: FRouted: Monad](modules: NonEmptyList[FantasyModule[F]]): F[Response] = {
    val swagger = modules.foldMap(_.swagger)
//    val descriptions =
//      PathDescription.utf8I18n("swagger", Locale.forLanguageTag("ru"))
    val json     = swagger /*.describe(descriptions)*/.make(OpenApiInfo()).asJson.pretty(printer)
    val response = message.jsonResponse(json)
    FRouted.checkPath[F, Response]("/swagger", response.pure[F])
  }

  def route[F[_]: SemigroupK: Monad: FRouted: Lift[Task, *[_]]](modules: NonEmptyList[FantasyModule[F]]): F[Response] =
    swaggerResources <+> swaggerHttp <+> swaggerJson(modules)

  object swaggerUI {

    def cssref(s: String) = link(href := s, rel := "stylesheet")
    def js(s: String)     = script(src := s)
    val version           = "3.20.5"
    def webjar(s: String) = s"/webjars/swagger-ui-dist/$version/$s"

    def index(schema: String) = html(
      meta(charset := "UTF-8"),
      tag("title")("Swagger - Dota2 Fantasy API"),
      cssref(
        "https://fonts.googleapis.com/css?family=Open+Sans:400,700|Source+Code+Pro:300,600|Titillium+Web:400,600,700"
      ),
      cssref(webjar("swagger-ui.css")),
      tag("style")(indexStyle),
      body(
        div(id := "swagger-ui"),
        js(webjar("swagger-ui-bundle.js")),
        js(webjar("swagger-ui-standalone-preset.js")),
        script(onload(schema))
      )
    )

    def indexStyle =
      raw("""
            |html{
            |      box-sizing: border-box;
            |      overflow: -moz-scrollbars-vertical;
            |      overflow-y: scroll;
            |    }
            |    *,
            |    *:before,
            |    *:after
            |    {
            |      box-sizing: inherit;
            |    }
            |
            |    body {
            |      margin:0;
            |      background: #fafafa;
            |    }""".stripPrefix("|"))

    def onload(schema: String) =
      raw(s"""
             |window.onload = function() {
             |
             |  // Build a system
             |  const ui = SwaggerUIBundle({
             |    url: "$schema",
             |    dom_id: '#swagger-ui',
             |    deepLinking: true,
             |    presets: [
             |      SwaggerUIBundle.presets.apis,
             |      SwaggerUIStandalonePreset
             |    ],
             |    plugins: [
             |      SwaggerUIBundle.plugins.DownloadUrl
             |    ],
             |    layout: "StandaloneLayout"
             |  })
             |
             |  window.ui = ui
             |}
          """.stripMargin)
  }
}
