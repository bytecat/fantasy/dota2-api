package fantasy.http

import cats.{Monad, MonoidK}
import cats.data.NonEmptyList
import cats.effect.Sync
import cats.syntax.all._
import com.twitter.finagle
import com.twitter.finagle.http.Response
import com.twitter.util.Await
import monix.eval.Task
import ru.tinkoff.tschema.finagle.{Routed, Runnable}
import tofu.lift.Lift

final class HttpSchema[F[_]: MonoidK: Monad: Routed: Lift[Task, *[_]]](
    val config: HttpSchema.Config,
    modules: NonEmptyList[FantasyModule[F]]
) {

  val route: F[Response] = modules.foldMapK(_.route) <+> Swagger.route(modules)

  def run[G[_]](implicit R: Runnable[F, G], S: Sync[G]): G[Unit] =
    for {
      srv  <- Runnable.run[G](route)
      list <- S.delay(finagle.Http.serve(s"${config.host}:${config.port}", srv))
      _    <- S.delay(Await.ready(list))
    } yield ()

}

object HttpSchema {
  case class Config(host: String, port: Int, title: String, version: String)

  val `X-Trace-Id` = "Internal-Trace-Id"

}
