package fantasy.support

trait Files[F[_], A] {
  def save(file: A): F[String]
}

object Files {
  def apply[F[_], A](implicit F: Files[F, A]): Files[F, A] = F
}
