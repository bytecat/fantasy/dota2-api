package fantasy.dota2

import cats.Applicative
import fantasy.domain.account.AccountId
import fantasy.domain.roster.{RosterId, UserRoster}

class D2PlayerRoster[F[_]: Applicative] extends UserRoster[F] {
  override def id(id: AccountId): F[RosterId] = Applicative[F].pure(RosterId(id.value))
}
