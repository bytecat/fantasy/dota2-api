package fantasy.dota2.boot

import cats.arrow.FunctionK
import cats.data.NonEmptyList
import cats.effect.{Sync, Timer}
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.tagless.syntax.functorK._
import cats.{Defer, Monad}
import fantasy.context.CoreConfig
import fantasy.domain.`match`.MatchControllerF
import fantasy.domain.account.{AccountController, AuthorizeService, TokenService}
import fantasy.domain.common.{CountriesController, CountriesRepository}
import fantasy.domain.dreamteam.DreamTeamController
import fantasy.domain.league.LeagueController
import fantasy.domain.player.PlayerController
import fantasy.domain.roster.RosterController
import fantasy.domain.series.{RoundService, SeriesController, TournamentController}
import fantasy.domain.team.TeamControllerF
import fantasy.dota2.{D2Point, D2Position, D2SystemController}
import fantasy.http.FantasyModule
import fantasy.modules._
import fantasy.util.i18n.{LocalizeProvider, ResourceBundleProvider, Type}
import fantasy.{HasLang, IdCompiler, ThrowMonad}
import ru.tinkoff.tschema.finagle.{LiftHttp, RoutedPlus}
import tofu.concurrent.MakeRef
import tofu.lift.Lift
import tofu.logging.Logs

import scala.concurrent.duration._

object modules {

  def apply[I[_]: Sync: MakeRef[*[_], F], F[_]: Logs[I, *[_]]: ThrowMonad: Defer: Timer: IdCompiler: HasLang, G[_]: Lift[
    F,
    *[_]
  ]: Monad: RoutedPlus: LiftHttp[*[_], F]] = new builder[I, F, G]

  class builder[I[_]: Sync: MakeRef[*[_], F], F[_]: Logs[I, *[_]]: ThrowMonad: Defer: Timer: IdCompiler: HasLang, G[_]: Lift[
    F,
    *[_]
  ]: Monad: RoutedPlus: LiftHttp[*[_], F]]() {
    type DFR = Repositories[F, D2Position, D2Point]
    type DFS = Services[F, D2Position, D2Point]

    def build(
        config: CoreConfig,
        rep: DFR,
        services: DFS,
        tokenService: TokenService[F]
    ): I[NonEmptyList[FantasyModule[G]]] = {
      implicit val r = rep
      implicit val s = services
      for {
        implicit0(lm: LocalizeProvider[I, Type.Messages]) <- ResourceBundleProvider.create[I, I, Type.Messages](
                                                              config.http.messagesBundle
                                                            )
        implicit0(auth: AuthorizeService[F]) <- AuthorizeService.create[I, F](rep.accounts, tokenService)
        accountsModule                       <- accounts
        playersModule                        <- playerModule
        teamsModule                          <- teams
        seriesModule                         <- series
        matchesModule                        <- matches
        rosterModule                         <- roster
        dreamTeamModule                      <- dreamTeam
        leaguesModule                        <- leagues
        countriesModule                      = countries
        tournamentsModule                    <- tournaments
        roundsModule                         = new RoundModule[G, F](new RoundService(rep.rouds))
        systemModule                         <- D2SystemController.create[I, F].map(ctrl => new SystemModule[G, F](ctrl))
      } yield
        NonEmptyList.of(
          accountsModule,
          playersModule,
          teamsModule,
          seriesModule,
          matchesModule,
          rosterModule,
          dreamTeamModule,
          leaguesModule,
          countriesModule,
          tournamentsModule,
          roundsModule,
          systemModule
        )
    }

    def accounts(
        implicit auth: AuthorizeService[F],
        services: DFS,
        reps: DFR
    ): I[AccountModule[G, F]] = {
      import reps._
      import services._
      for {
        implicit0(countries: CountriesRepository[F]) <- CountriesRepository
                                                         .cached[I, F](reps.countries, 1.hour)
        controller                         <- AccountController.create[I, F](services.account, auth)
        implicit0(au: AuthorizeService[G]) = auth.mapK(Lift[F, G].liftF)
      } yield new AccountModule[G, F](controller)
    }

    def playerModule(implicit reps: DFR, service: DFS): I[PlayerModule[G, F]] =
      for {
        implicit0(countries: CountriesRepository[F]) <- CountriesRepository
                                                         .cached[I, F](reps.countries, 1.hour)
        controller <- PlayerController.create[I, F, F, D2Point, D2Position](
                       service.player,
                       service.team,
                       reps.points,
                       reps.price,
                       service.round,
                       service.series,
                       FunctionK.id
                     )
        module = new PlayerModule[G, F](controller)
      } yield module

    def teams(implicit serv: DFS): I[TeamsModule[G, F]] = {
      new TeamsModule[G, F](
        new TeamControllerF[F, F](serv.team, FunctionK.id, FunctionK.id)
      ).pure[I]
    }

    def series(implicit serv: DFS, rep: DFR): I[SeriesModule[G, F]] =
      for {
        controller <- SeriesController
                       .create[I, F, D2Point](
                         serv.series,
                         serv.team,
                         serv.round,
                         serv.tournament,
                         rep.seriesScore,
                         serv.matchPoints,
                         serv.matchService
                       )
        module = new SeriesModule[G, F](controller)
      } yield module

    def matches(implicit service: DFS): I[MatchesModule[G, F]] = {
      val controller = new MatchControllerF[F, F, D2Position, D2Point](
        service.matchService,
        service.team,
        service.matchPoints,
        service.player,
        FunctionK.id
      )

      new MatchesModule[G, F](controller).pure[I]
    }

    def roster(
        implicit authorizeService: AuthorizeService[F],
        service: DFS,
        reps: DFR
    ): I[RosterModule[G, F]] = {
      import reps._
      import service._

      for {
        implicit0(countr: CountriesRepository[F]) <- CountriesRepository
                                                      .cached[I, F](reps.countries, 1.hour)
        controller <- RosterController
                       .create[I, F, D2Position](
                         service.roster,
                         service.player,
                         service.team,
                         service.league,
                         service.round,
                         service.series,
                         reps.dreamTeam
                       )
        implicit0(auth: AuthorizeService[G]) = authorizeService.mapK(Lift[F, G].liftF)
      } yield new RosterModule[G, F](controller)
    }

    def dreamTeam(
        implicit service: DFS,
        reps: DFR
    ): I[DreamTeamModule[G, F]] = {
      import service._

      implicit val dreamTeamRepository = reps.dreamTeam

      for {
        implicit0(countr: CountriesRepository[F]) <- CountriesRepository
                                                      .cached[I, F](reps.countries, 1.hour)
        controller <- DreamTeamController.create[I, F, D2Position, D2Point]
      } yield new DreamTeamModule[G, F](controller)
    }

    def leagues(
        implicit authorizeService: AuthorizeService[F],
        service: DFS,
        reps: DFR
    ): I[LeagueModule[G, F]] = {
      import service._

      for {
        implicit0(c: CountriesRepository[F]) <- CountriesRepository
                                                 .cached[I, F](reps.countries, 1.hour)
        implicit0(a: AuthorizeService[G]) = authorizeService.mapK(Lift[F, G].liftF)
        controller                        = new LeagueController[F](service.league, service.round)
      } yield new LeagueModule[G, F](controller)
    }

    def countries(implicit reps: DFR): CountriesModule[G, F] = {
      new CountriesModule[G, F](new CountriesController[F](reps.countries))
    }

    def tournaments(implicit reps: DFR, service: DFS): I[TournamentsModule[G, F]] =
      for {
        implicit0(c: CountriesRepository[F]) <- CountriesRepository
                                                 .cached[I, F](reps.countries, 1.hour)
        controller <- TournamentController.create[I, F](service.tournament)
      } yield new TournamentsModule[G, F](controller)
  }
}
