package fantasy.dota2.boot

import cats.effect.Timer
import cats.syntax.all._
import cats.{Defer, Monad}
import doobie.free.connection.ConnectionIO
import fantasy.context.CoreConfig
import fantasy.domain.`match`.{MatchPointService, MatchService, Point}
import fantasy.domain.account.AccountService
import fantasy.domain.common.CountriesRepository
import fantasy.domain.league.LeagueService
import fantasy.domain.player.PlayerService
import fantasy.domain.roster.{RosterService, RosterValidator, UserRoster}
import fantasy.domain.series.{RoundService, SeriesService, TournamentService}
import fantasy.domain.team.TeamService
import fantasy.dota2.data.DotaMatchPoints
import fantasy.dota2.{D2PlayerRoster, D2Point, D2Position, D2RosterValidator}
import fantasy.{IdCompiler, LiftCIO, LiftStream, ThrowMonad}
import tofu.concurrent.MakeRef
import tofu.logging.Logs

import scala.concurrent.duration._

case class Services[F[_], P, PP <: Point]()(
    implicit val account: AccountService[F],
    val league: LeagueService[F],
    val matchPoints: MatchPointService[F, PP],
    val matchService: MatchService[F],
    val player: PlayerService[F, P, PP],
    val roster: RosterService[F],
    val round: RoundService[F],
    val series: SeriesService[F],
    val tournament: TournamentService[F],
    val team: TeamService[F],
    val userRoster: UserRoster[F]
)

object Services {
  def create[I[_]: Monad, F[_]: ThrowMonad: Defer: MakeRef[I, *[_]]: Timer: IdCompiler: LiftCIO: LiftStream[
    ConnectionIO,
    *[_]
  ]](config: CoreConfig)(
      implicit rep: Repositories[F, D2Position, D2Point],
      L: Logs[I, F]
  ): I[Services[F, D2Position, D2Point]] = {
    import rep._
    for {
      accounts                           <- AccountService.create[I, F](rep.accounts)
      implicit0(rounds: RoundService[F]) = new RoundService(rep.rouds)
      implicit0(countries: CountriesRepository[F]) <- CountriesRepository
                                                       .cached[I, F](rep.countries, 1.hour)
      implicit0(userRoster: UserRoster[F]) = new D2PlayerRoster[F]
      league <- LeagueService.create[I, F](
                 rep.leagues,
                 rep.leagueTable,
                 rep.members,
                 rounds,
                 rep.rosters,
                 accounts,
                 rep.teams
               )
      matches <- MatchService.create[I, F](rep.matchRepository)
      implicit0(series: SeriesService[F]) <- SeriesService
                                              .create[I, F](rep.series, rep.seriesScore, matches)
      matchPointsService = new DotaMatchPoints(matches, series)
      players            <- PlayerService.create[I, F, D2Position, D2Point](rep.players, rep.price, rep.points)
      implicit0(validator: RosterValidator[F]) = new D2RosterValidator(
        players,
        config.game.maxPlayersFromTeam
      )
      roster      <- RosterService.create[I, F](config.game.startMoney)
      teams       <- TeamService.create[I, F](rep.teams)
      tournaments = new TournamentService(rep.tournaments)
    } yield
      Services()(
        accounts,
        league,
        matchPointsService,
        matches,
        players,
        roster,
        rounds,
        series,
        tournaments,
        teams,
        userRoster
      )
  }
}
