package fantasy.dota2.boot

import cats.effect.{Async, Sync}
import cats.{Applicative, ~>}
import fantasy.domain.`match`.{MatchRepository, Point, Points}
import fantasy.domain.account.AccountRepository
import fantasy.domain.common.CountriesRepository
import fantasy.domain.dreamteam.DreamTeamRepository
import fantasy.domain.league.{LeagueMembers, LeagueRepository, LeagueTableRepository}
import fantasy.domain.player.{PlayerPrice, PlayerRepository}
import fantasy.domain.roster.{RosterHistoriesRepository, RostersRepository}
import fantasy.domain.series.{RoundRepository, SeriesRepository, SeriesScore, TournamentRepository}
import fantasy.domain.team.TeamsRepository
import cats.tagless.syntax.functorK._
import doobie.free.connection.ConnectionIO
import fantasy.dota2.{D2PlayerRoster, D2Point, D2Position}
import fantasy.dota2.data.{
  DotaAccounts,
  DotaCountries,
  DotaDreamTeam,
  DotaLeagueMembers,
  DotaLeagueTable,
  DotaLeagues,
  DotaMatchRepository,
  DotaPlayers,
  DotaPoints,
  DotaPrice,
  DotaRosterHistories,
  DotaRosters,
  DotaRounds,
  DotaSeriesRepository,
  DotaSeriesScore,
  DotaTeams,
  DotaTournaments
}

case class Repositories[F[_], P, PP <: Point]()(
    implicit val accounts: AccountRepository[F],
    val countries: CountriesRepository[F],
    val members: LeagueMembers[F],
    val leagues: LeagueRepository[F],
    val leagueTable: LeagueTableRepository[F],
    val matchRepository: MatchRepository[F],
    val players: PlayerRepository[F, P, PP],
    val points: Points[F, P, PP],
    val price: PlayerPrice[F],
    val history: RosterHistoriesRepository[F],
    val rosters: RostersRepository[F],
    val rouds: RoundRepository[F],
    val series: SeriesRepository[F],
    val seriesScore: SeriesScore[F],
    val teams: TeamsRepository[F],
    val tournaments: TournamentRepository[F],
    val dreamTeam: DreamTeamRepository[F]
) {
  def mapK[G[_]](exec: F ~> G, execStream: fs2.Stream[F, ?] ~> fs2.Stream[G, ?]): Repositories[G, P, PP] =
    Repositories[G, P, PP]()(
      accounts.mapK(exec),
      countries.mapK(exec),
      LeagueMembers.mapK(members)(exec, execStream),
      leagues.mapK(exec),
      leagueTable.mapK(exec),
      matchRepository.mapK(exec),
      PlayerRepository.mapK(players)(exec),
      Points.mapK(points)(exec),
      PlayerPrice.mapK(price)(exec, execStream),
      history.mapK(exec),
      RostersRepository.mapK(rosters)(exec, execStream),
      rouds.mapK(exec),
      series.mapK(exec),
      seriesScore.mapK(exec),
      teams.mapK(exec),
      tournaments.mapK(exec),
      dreamTeam.mapK(exec)
    )
}

object Repositories {
  def create[I[_]: Sync]: I[Repositories[ConnectionIO, D2Position, D2Point]] = {
    implicit val A: Applicative[ConnectionIO] = Async[ConnectionIO]

    Sync[I].delay {
      Repositories()(
        DotaAccounts,
        DotaCountries,
        DotaLeagueMembers,
        DotaLeagues,
        DotaLeagueTable,
        DotaMatchRepository,
        DotaPlayers,
        DotaPoints,
        DotaPrice,
        DotaRosterHistories,
        new DotaRosters(new D2PlayerRoster[ConnectionIO]),
        DotaRounds,
        DotaSeriesRepository,
        new DotaSeriesScore(DotaSeriesRepository),
        DotaTeams,
        DotaTournaments,
        DotaDreamTeam
      )
    }
  }
}
