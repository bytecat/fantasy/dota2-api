package fantasy.dota2.boot

import cats.effect.{ContextShift, Sync, Timer}
import cats.syntax.all._
import cats.tagless.syntax.functorK._
import cats.{ApplicativeError, Monad, ~>}
import doobie.free.connection.ConnectionIO
import fantasy.context.CoreConfig
import fantasy.dota2.data.DotaValve
import fantasy.dota2.engine._
import fantasy.dota2.{D2Point, D2Position}
import fantasy.engine.{ArbiterData, DreamTeamSelector, GameArbiter, MatchFinder}
import fantasy.opendota.{DeferFinagleFuture, OpenDotaClient}
import fantasy.{IdCompiler, ThrowMonad}
import monix.execution.Scheduler
import tofu.Fire
import tofu.concurrent.MakeRef
import tofu.lift.Lift
import tofu.logging.Logs
import tofu.syntax.fire._

object Arbiter {
  def run[I[_]: Sync: Timer: Fire, F[_]: Monad: ThrowMonad: MakeRef[I, *[_]]: DeferFinagleFuture: ContextShift: IdCompiler: Lift[
    *[_],
    I
  ]](
      config: CoreConfig
  )(
      implicit reps: Repositories[F, D2Position, D2Point],
      service: Services[F, D2Position, D2Point],
      L: Logs[I, F],
      executeCIO: Lift[ConnectionIO, F]
  ): I[GameArbiter[D2Position, D2Point, F]] = {
    import cron4s.Cron
    import eu.timepit.fs2cron._
    import service._

    implicit val arbiterData: ArbiterData[F] = GameArbiterData.mapK(executeCIO.liftF)
    implicit val calc                        = new DotaPointCalculator[F]
    implicit val dts =
      DreamTeamSelector.mapK(DotaDreamTeamSelect)(new ~>[Either[Throwable, *], F[*]] {
        override def apply[A](fa: Either[Throwable, A]): F[A] =
          ApplicativeError[F, Throwable].fromEither(fa)
      })

    for {
      cron                          <- ApplicativeError[I, Throwable].fromEither(Cron.parse("0 0 * * * ?"))
      schuduler                     = Scheduler.io("game-arbiter")
      implicit0(v: ExternalData[F]) <- ExternalData.cached[I, F](DotaValve.mapK(executeCIO.liftF))
      implicit0(c: OpenDotaClient[F]) <- OpenDotaClient
                                          .create[I, F](config.odota.service, config.odota.token)
      implicit0(m: MatchFinder[F, D2Point]) <- OpenDotaFinder.create[I, F](schuduler)
      arbiter                               <- GameArbiter.create[I, D2Position, D2Point, F](service, reps)
      cronStream                            = awakeEveryCron[I](cron) >> fs2.Stream.eval(Lift[F, I].lift(arbiter.run()))
      _                                     <- cronStream.compile.drain.fireAndForget
    } yield arbiter
  }
}
