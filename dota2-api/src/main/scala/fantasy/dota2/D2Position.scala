package fantasy.dota2
import cats.Eq
import doobie.util.Meta
import enumeratum._
import fantasy.domain.player.PositionId

import scala.collection.immutable

sealed trait D2Position extends EnumEntry {
  def id: Int
}
object D2Position extends Enum[D2Position] {
  override def values: immutable.IndexedSeq[D2Position] = findValues

  case object Core extends D2Position {
    override val id: Int = 1
  }
  case object Support extends D2Position {
    override val id: Int = 2
  }

  implicit val P: PositionId[D2Position] = PositionId.instance[D2Position](_.id, id => values.find(_.id == id))
  implicit val eq: Eq[D2Position]        = Eq.instance(_.id == _.id)
  implicit val meta: Meta[D2Position]    = Meta[Int].timap[D2Position](i => D2Position.values.find(_.id == i).orNull)(_.id)
}
