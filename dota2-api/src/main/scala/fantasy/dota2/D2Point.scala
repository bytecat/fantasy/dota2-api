package fantasy.dota2

import doobie.util.Meta
import enumeratum._
import fantasy.domain.`match`.{CalculatedPointValue, Point, PointId, TotalPoints}
import fantasy.util.http.HttpEnum
import io.circe.{KeyDecoder, KeyEncoder}

import scala.collection.immutable

sealed trait D2Point extends EnumEntry with Point {
  def id: Int
}
object D2Point extends HttpEnum[D2Point] {
  override def values: immutable.IndexedSeq[D2Point] = findValues

  sealed abstract class AbstractD2Point(
      val id: Int,
      val code: String,
      val isPositive: Boolean = true,
      val isBonus: Boolean = false,
      val searchAvailable: Boolean = true
  ) extends D2Point

  case object Total      extends AbstractD2Point(0, "ponts.total")
  case object Kill       extends AbstractD2Point(1, "dota2.kill")
  case object Assist     extends AbstractD2Point(2, "dota2.assist")
  case object Death      extends AbstractD2Point(3, "dota2.death", false)
  case object GPM        extends AbstractD2Point(4, "dota2.GPM")
  case object HeroDamage extends AbstractD2Point(5, "dota2.heroDamage")
  case object Towers     extends AbstractD2Point(6, "dota2.towersBroken")
//  case object FirstBlood     extends AbstractD2Point(7, "dota2.firstBlood", isBonus = true)
//  case object SupportActions extends AbstractD2Point(8, "dota2.supportActions")
  case object Stuns        extends AbstractD2Point(9, "dota2.stuns", searchAvailable = false)
  case object Wards        extends AbstractD2Point(10, "dota2.wards", searchAvailable = false)
  case object CampsStacked extends AbstractD2Point(11, "dota2.camps", searchAvailable = false)
  case object KillStreaks  extends AbstractD2Point(12, "dota2.killsStreaks", searchAvailable = false)
  case object MultiKills   extends AbstractD2Point(13, "dota2.multiKills", searchAvailable = false)
  case object Runes        extends AbstractD2Point(14, "dota2.runes", searchAvailable = false)
  case object Win          extends AbstractD2Point(15, "dota2.win", searchAvailable = false, isBonus = true)
  case object MVP          extends AbstractD2Point(16, "dota2.MVP", searchAvailable = false, isBonus = true)
  case object Creeps       extends AbstractD2Point(17, "dota2.creeps", searchAvailable = false)
  case object Roshan       extends AbstractD2Point(18, "dota2.roshan", searchAvailable = false)
  case object HeroHill     extends AbstractD2Point(19, "dota2.hill", searchAvailable = false)

  implicit val toId: PointId[D2Point] = new PointId[D2Point] {
    override def toId(point: D2Point): Int = point.id

    override def fromId(id: Int): Option[D2Point] = values.find(_.id == id)
  }

  implicit val meta: Meta[D2Point] = Meta[Int].timap(id => values.find(_.id == id).orNull)(_.id)

  implicit val total: TotalPoints[D2Point] = (map: Map[D2Point, CalculatedPointValue]) =>
    map.getOrElse(
      Total,
      map
        .map(
          t =>
            if (t._1.isPositive) {
              CalculatedPointValue(t._2.value.abs)
            } else {
              CalculatedPointValue(-t._2.value.abs)
            }
        )
        .sum
    )

  implicit val keyEncoder: KeyEncoder[D2Point] = KeyEncoder[Int].contramap(_.id)
  implicit val keyDecoder: KeyDecoder[D2Point] = KeyDecoder[Int].map(id => values.find(_.id == id).orNull)
}
