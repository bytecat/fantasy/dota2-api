package fantasy.dota2

import cats.Applicative
import cats.effect.Sync
import cats.instances.list._
import cats.syntax.applicative._
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.traverse._
import fantasy.modules.SystemModule.{PointResponse, PositioResponse}
import fantasy.modules.{SystemController, SystemModule}
import fantasy.util.i18n.{Language, LocalizeProvider, Type}
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

class D2SystemController[F[_]: Applicative: Logging](pointLang: Map[Language, List[PointResponse]])
    extends SystemController[F] {
  override def points(language: Language): F[List[SystemModule.PointResponse]] =
    pointLang.getOrElse(language, List.empty).pure[F]

  override def languages(): F[List[Language]] =
    info"test" *> Language.values.toList.pure[F]

  override def positions(): F[List[SystemModule.PositioResponse]] =
    D2Position.values.map(d2p => PositioResponse(d2p.id, d2p.entryName)).toList.pure[F]
}

object D2SystemController {
  def create[I[_]: Sync, F[_]: Applicative: Logs[I, *[_]]](
      implicit loc: LocalizeProvider[I, Type.MessagesT.type]
  ): I[D2SystemController[F]] =
    Logs[I, F]
      .forService[D2SystemController[F]]
      .flatMap(
        implicit l =>
          Sync[I].defer {
            Language.values.toList.traverse { l =>
              loc
                .provide(l)
                .flatMap { locale =>
                  D2Point.values.toList.traverse { d2p =>
                    locale.fill(d2p.code).map(PointResponse(d2p.id, d2p.isPositive, d2p.isBonus, _))
                  }
                }
                .map(l -> _)
            }.map(_.toMap).map(m => new D2SystemController[F](m))
          }
      )
}
