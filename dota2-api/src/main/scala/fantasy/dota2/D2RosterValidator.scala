package fantasy.dota2

import cats.Monad
import cats.data.{EitherT, NonEmptyList}
import cats.instances.double._
import cats.instances.either._
import cats.syntax.applicative._
import cats.syntax.either._
import cats.syntax.flatMap._
import cats.syntax.functor._
import fantasy.domain.player.{PlayerId, PlayerPrice, PlayerService}
import fantasy.domain.roster.RosterValidator._
import fantasy.domain.roster.{Roster, RosterValidator}
import fantasy.domain.series.{RoundService, SeriesService}
import fs2.Stream

class D2RosterValidator[F[_]: RoundService: SeriesService: Monad: Stream.Compiler[*[_], F]: PlayerPrice](
    playerService: PlayerService[F, D2Position, D2Point],
    maxPlaersFromTeam: Int
) extends RosterValidator[F] {

  override def validate(
      roster: Roster,
      transfer: NonEmptyList[PlayerId]
  ): F[Either[RosterValidator.ValidationError, Unit]] =
    checkCompleted(transfer)
      .flatMapF(_ => checkPlayers(transfer))
      .flatMapF { _ =>
        for {
          currentPlayers <- roster.players.traverse(playerService.get)
          currentPlayersPrice <- currentPlayers
                                  .traverse(p => PlayerPrice[F].price(p.id).map(_.getOrElse(0d)))
                                  .map(_.reduce)
          res <- checkMoney(currentPlayersPrice + roster.availableMoney, transfer)
        } yield res
      }
      .value

  override def validateNewRoster(
      availableMoney: Double,
      players: NonEmptyList[PlayerId]
  ): F[Either[RosterValidator.ValidationError, Unit]] =
    checkCompleted(players)
      .flatMapF(_ => checkPlayers(players))
      .flatMapF(_ => checkMoney(availableMoney, players))
      .value

  private def checkCompleted(players: NonEmptyList[PlayerId]) =
    EitherT
      .fromEither[F] {
        if (players.toNes.length != 5) {
          RosterNotCompleted.asLeft[Unit]
        } else ().asRight[ValidationError]
      }

  private def checkMoney(available: Double, players: NonEmptyList[PlayerId]) =
    for {
      transferPlayerPrice <- players.traverse(p => PlayerPrice[F].price(p).map(_.getOrElse(0d))).map(_.reduce)
      money               = available - transferPlayerPrice
      res                 = (MoneyExceeded: ValidationError).asLeft[Unit].whenA(money < 0)
    } yield res

  private def checkPlayers(transfer: NonEmptyList[PlayerId]) =
    for {
      players <- transfer.traverse(playerService.get)
      fromOneTeam = players
        .traverse(p => p.teamId.toRight(PlayerNotPickable(p.id): ValidationError).map(p.id -> _))
        .map(_.groupBy(_._2).mapValues(_.map(_._1)))
      oneTeamCheck = fromOneTeam.flatMap(
        _.find(_._2.size > maxPlaersFromTeam).toLeft(players).leftMap(e => TeamLimit(e._1): ValidationError)
      )
      coresCheck = oneTeamCheck >> RosterNotCompleted.asLeft.whenA(
        players.filter(_.position == D2Position.Core).size != 3
      )
      supportCheck = coresCheck >> RosterNotCompleted.asLeft.whenA(
        players.filter(_.position == D2Position.Support).size != 2
      )
    } yield supportCheck

  /*private def maxFromOneTeam =
    OptionT(RoundService[F].nextRound()).flatMapF { r =>
      SeriesService[F].getByRound(r.number).exists(_.stage === Stage.Final).compile.last
    }.map(hasFinal => if (hasFinal) 3 else 2).getOrElse(2)*/
}
