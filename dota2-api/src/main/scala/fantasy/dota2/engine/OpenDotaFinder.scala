package fantasy.dota2.engine

import java.time.{Duration, LocalDateTime, ZoneOffset}

import cats.Functor
import cats.data.NonEmptyList
import cats.effect.ContextShift
import cats.instances.int._
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.apply._
import cats.syntax.eq._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.monoid._
import cats.syntax.traverse._
import cats.syntax.traverseFilter._
import fantasy.ThrowMonad
import fantasy.domain.`match`.RawPointValue
import fantasy.domain.player.{PlayerId, PlayerService}
import fantasy.domain.series.{RoundId, Series, SeriesId, SeriesService}
import fantasy.domain.team.TeamId
import fantasy.dota2.D2Point._
import fantasy.dota2.engine.ExternalData.{ExternalMatchId, ExternalPlayerId, ExternalTeamId}
import fantasy.dota2.engine.OpenDotaFinder.{FinderRaise, MatchNotLinked}
import fantasy.dota2.{D2Point, D2Position}
import fantasy.engine.MatchFinder
import fantasy.engine.MatchFinder.{MatchInfo, PlayerMatchInfo}
import fantasy.opendota.OpenDotaClient
import fantasy.opendota.model.{OpenDotaMatch, OpenDotaMatchInfo, OpenDotaPlayerInMatch}
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import tofu.logging.{Logging, Logs}
import tofu.syntax.logging._

import scala.concurrent.ExecutionContext
import scala.util.control.NoStackTrace

class OpenDotaFinder[F[_]: SeriesService: ThrowMonad: FinderRaise: OpenDotaClient: Logging: ContextShift: ExternalData](
    ec: ExecutionContext
) extends MatchFinder[F, D2Point] {

  private val OdotaZone = ZoneOffset.UTC

  implicit val localDateTimeOrdering: Ordering[LocalDateTime] = Ordering.by(_.toEpochSecond(OdotaZone))

  override def lookup(round: RoundId): F[List[MatchInfo[D2Point, RawPointValue]]] =
    for {
      _         <- info"start find matches for $round round"
      allSeries <- SeriesService[F].getByRound(round)
      res <- if (allSeries.isEmpty) {
              info"not found series for $round" >> List.empty[MatchInfo[D2Point, RawPointValue]].pure[F]
            } else {
              lookup(allSeries)
            }
    } yield res

  override def lookup(
      round: RoundId,
      series: Map[SeriesId, List[ExternalData.ExternalMatchId]]
  ): F[List[MatchInfo[D2Point, RawPointValue]]] =
    for {
      _          <- info"start find matches for $round round"
      seriesData <- series.keys.toList.traverse(SeriesService[F].get)
      seriesMatches <- seriesData.traverse(
                        s =>
                          series
                            .getOrElse(s.id, List())
                            .flatTraverse(m => OpenDotaClient[F].get(m.value).map(_.toList))
                            .map(s -> _)
                      )
      res <- seriesMatches.flatTraverse {
              case (s, matches) => matches.flatTraverse(link(_, List(s), Map()).map(_.toList))
            }
    } yield res

  private def lookup(series: List[Series]): F[List[MatchInfo[D2Point, RawPointValue]]] = {
    val from = series.map(_.time).min
    val to   = series.map(_.time).max.plus(Duration.ofHours(5))
    for {
      matches <- findMatches(from).map(
                  _.filter(
                    od => od.startTime >= from.toEpochSecond(OdotaZone) && od.startTime <= to.toEpochSecond(OdotaZone)
                  )
                )
      colls       = findCollisions(series)
      prefiltered <- matches.filterA(hasTeam)
      matchesInfo <- prefiltered.flatTraverse(
                      m =>
                        OpenDotaClient[F].get(m.matchId).map(_.toList).onError {
                          case err => s"failed get match ${m.matchId}".cause(err)
                        }
                    )
      data <- matchesInfo.traverse(link(_, series, colls))
    } yield data.flatten
  }

  private def link(
      m: OpenDotaMatchInfo,
      series: List[Series],
      upperTimeLimit: Map[SeriesId, LocalDateTime]
  ): F[Option[MatchInfo[D2Point, RawPointValue]]] =
    ContextShift[F].evalOn(ec) {
      val res = for {
        _ <- info"try find match ${m.matchId}"
        radiant <- m.radiantTeam
                    .flatMap(_.teamId)
                    .liftTo[F](MatchNotLinked(s"in match not filled radiant team id"))
                    .flatMap { id =>
                      ExternalData[F]
                        .team(ExternalTeamId(id))
                        .flatMap(_.liftTo[F](MatchNotLinked(s"Not found radiant team ${m.radiantTeam}")))
                    }
        dire <- m.direTeam.flatMap(_.teamId).liftTo[F](MatchNotLinked(s"in match not filled dire team id")).flatMap {
                 id =>
                   ExternalData[F]
                     .team(ExternalTeamId(id))
                     .flatMap(_.liftTo[F](MatchNotLinked(s"Not found dire team ${m.direTeam}")))
               }
        matchedSeries <- findSeries(
                          series,
                          radiant,
                          dire,
                          upperTimeLimit,
                          LocalDateTime.ofEpochSecond(m.startTime, 0, OdotaZone),
                          m.matchId
                        )
        matchPlayers <- m.players.traverse(
                         p =>
                           p.accountId
                             .flatTraverse(id => ExternalData[F].player(ExternalPlayerId(id)))
                             .flatMap(
                               _.liftTo[F](MatchNotLinked(s"Not found player ${p.name} ${p.accountId}", failure = true))
                             )
                             .map(p -> _)
                       )
        res = matchPlayers.map { case (data, id) => mapPoints(id, data, radiant = radiant, dire = dire) }
        nel <- NonEmptyList
                .fromList(res)
                .filter(_.size == 10)
                .liftTo[F](MatchNotLinked("Not found players data", failure = true))
      } yield
        MatchInfo(
          ExternalMatchId(m.matchId),
          radiant,
          dire,
          LocalDateTime.ofEpochSecond(m.startTime, 0, OdotaZone),
          LocalDateTime.ofEpochSecond(m.startTime + m.duration, 0, OdotaZone),
          m.duration,
          m.radiantWin,
          matchedSeries.id,
          nel
        )
      res.map(Option(_)).recoverWith {
        case err: MatchNotLinked if !err.failure =>
          info"match ${m.matchId} not linked by ${err.reason}" *> (None: Option[MatchInfo[D2Point, RawPointValue]])
            .pure[F]
        case err =>
          s"match ${m.matchId} not linked".cause(err) *> (None: Option[MatchInfo[D2Point, RawPointValue]]).pure[F]
      }
    }

  private def hasTeam(od: OpenDotaMatch): F[Boolean] =
    od.radiantTeamId
      .map2(od.direTeamId) { (r, d) =>
        for {
          rId <- ExternalData[F].team(ExternalTeamId(r))
          dId <- ExternalData[F].team(ExternalTeamId(d))
          _ <- info"not find teams into db: matchId: ${od.matchId}, radiant: ${od.radiantName} ${od.radiantTeamId}, dire: ${od.direName} ${od.direTeamId}"
                .unlessA(rId.isDefined && dId.isDefined)
        } yield rId.isDefined && dId.isDefined
      }
      .getOrElse(
        false
          .pure[F] <* info"nof filled teams into match ${od.matchId}: radiant: ${od.radiantName} ${od.radiantTeamId}, dire: ${od.direName} ${od.direTeamId}"
      )

  private def mapPoints(
      id: PlayerId,
      data: OpenDotaPlayerInMatch,
      radiant: TeamId,
      dire: TeamId
  ): PlayerMatchInfo[D2Point, RawPointValue] = {

    import fantasy.domain.`match`.RawPointValue.syntax._

    val raw: Map[D2Point, RawPointValue] = D2Point.values.toList.map {
      case Kill       => Kill       -> data.kills.getOrElse(0).asRaw
      case Assist     => Assist     -> data.assists.getOrElse(0).asRaw
      case Death      => Death      -> data.deaths.getOrElse(0).asRaw
      case GPM        => GPM        -> data.goldPerMin.getOrElse(0).asRaw
      case HeroDamage => HeroDamage -> data.heroDamage.getOrElse(0).asRaw
      case HeroHill   => HeroHill   -> data.heroHealing.getOrElse(0).asRaw
      case Towers     => Towers     -> data.towerKills.getOrElse(0).asRaw
//      case FirstBlood => FirstBlood -> 0D // todo fix
      case Stuns => Stuns -> data.stuns.getOrElse(0d).asRaw
      case Wards =>
        Wards -> (data.observerKills |+| data.sentryKills |+| data.obsPlaced)
          .getOrElse(0)
          .asRaw
      case CampsStacked => CampsStacked -> data.campsStacked.getOrElse(0).asRaw
      case KillStreaks =>
        KillStreaks -> data.killStreaks.foldLeft(0)((acc, kv) => acc + (kv._1.toInt * kv._2)).asRaw
      case MultiKills =>
        MultiKills -> data.multiKills.foldLeft(0)((acc, kv) => acc + (kv._1.toInt * kv._2)).asRaw
      case Runes             => Runes  -> data.runePickups.getOrElse(0).asRaw
      case Win               => Win    -> data.radiantWin.flatMap(rw => data.isRadiant.map(_ && rw)).map(_ => 1).getOrElse(0).asRaw
      case x @ (Total | MVP) => x      -> 0.asRaw
      case Creeps            => Creeps -> (data.lastHits |+| data.denies).getOrElse(0).asRaw
      case Roshan            => Roshan -> data.roshanKills.getOrElse(0).asRaw
    }.toMap

    val team = if (data.isRadiant.getOrElse(true)) radiant else dire

    PlayerMatchInfo(id, team, raw.filterKeys(k => k =!= Total && k =!= MVP))
  }

  private def findSeries(
      series: List[Series],
      radiant: TeamId,
      dire: TeamId,
      upperTimeLimit: Map[SeriesId, LocalDateTime],
      time: LocalDateTime,
      id: Long
  ) = {
    series
      .filter(
        s =>
          (s.home.contains(radiant) && s.guest.contains(dire)) || (s.home.contains(dire) && s.guest.contains(radiant))
      )
      .filter(s => upperTimeLimit.get(s.id).forall(_.isAfter(time))) match {
      case x :: Nil => x.pure[F]
      case Nil      => MatchNotLinked(s"failed find series form match $id").raise[F, Series]
      case xs       => MatchNotLinked(s"failed to establish uniqueness for match $id: ${xs.map(_.id)}").raise[F, Series]
    }
  }

  private def findCollisions(series: List[Series]): Map[SeriesId, LocalDateTime] = {
    series
      .foldLeft(Map.empty[SeriesId, List[Series]]) { (accum, id) =>
        val res: List[Series] = series.filter(
          s =>
            (s.home === id.home || s.guest === id.home)
              && (s.home === id.guest || s.guest === id.guest)
        ) match {
          case x if x.size > 1 => x
          case _               => List.empty[Series]
        }
        accum + (id.id -> res.sortBy(_.time))
      }
      .filter(_._2.nonEmpty)
      .map {
        case (id, colls) =>
          id -> colls.dropWhile(_.id =!= id).tail.headOption.map(_.time)
      }
      .collect {
        case (id, Some(time)) => id -> time
      }
  }

  private def findMatches(from: LocalDateTime): F[List[OpenDotaMatch]] =
    OpenDotaClient[F]
      .matches(None)
      .tailRecM(_.flatMap { current =>
        info"findMatches from ${from.toString}" >> (current
          .map(_.startTime)
          .map(LocalDateTime.ofEpochSecond(_, 0, OdotaZone)) match {
          case xs if xs.isEmpty => (Right(current): Either[F[List[OpenDotaMatch]], List[OpenDotaMatch]]).pure[F]
          case xs if xs.min.compareTo(from) < 0 =>
            (Right(current): Either[F[List[OpenDotaMatch]], List[OpenDotaMatch]]).pure[F]
          case _ =>
            val m = current.minBy(d => LocalDateTime.ofEpochSecond(d.startTime, 0, OdotaZone))
            (Left(OpenDotaClient[F].matches(Some(m.matchId)).map(_ ++ current)): Either[F[List[OpenDotaMatch]], List[
              OpenDotaMatch
            ]]).pure[F]
        })
      })

}

object OpenDotaFinder {
  type DotaPlayers[F[_]] = PlayerService[F, D2Position, D2Point]
  type FinderRaise[F[_]] = Raise[F, DotaFinderError]

  sealed trait DotaFinderError extends Throwable with NoStackTrace

  case class MatchNotLinked(reason: String, failure: Boolean = false) extends Throwable(reason) with DotaFinderError

  def create[I[_]: Functor, F[_]: SeriesService: ThrowMonad: OpenDotaClient: ContextShift: ExternalData: Logs[
    I,
    *[_]
  ]](
      ec: ExecutionContext
  ): I[MatchFinder[F, D2Point]] =
    Logs[I, F].forService[OpenDotaFinder[F]].map(implicit log => new OpenDotaFinder[F](ec))

}
