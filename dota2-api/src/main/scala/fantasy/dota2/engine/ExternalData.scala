package fantasy.dota2.engine

import cats.Monad
import cats.effect.concurrent.Ref
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.tagless.FunctorK
import fantasy.domain.`match`.MatchId
import fantasy.domain.player.PlayerId
import fantasy.domain.team.TeamId
import fantasy.dota2.engine.ExternalData.ExternalMatchId
import fantasy.util.{LongWrapper, LongWrapperCompanion}
import simulacrum.typeclass
import tofu.concurrent.MakeRef

@typeclass
trait ExternalData[F[_]] {
  def team(id: ExternalData.ExternalTeamId): F[Option[TeamId]]
  def player(id: ExternalData.ExternalPlayerId): F[Option[PlayerId]]
  def saveLink(valveId: ExternalMatchId, internalId: MatchId): F[Unit]
}

object ExternalData {
  case class ExternalTeamId(value: Long) extends AnyVal with LongWrapper
  object ExternalTeamId                  extends LongWrapperCompanion[ExternalTeamId]

  case class ExternalPlayerId(value: Long) extends AnyVal with LongWrapper
  object ExternalPlayerId                  extends LongWrapperCompanion[ExternalPlayerId]

  case class ExternalMatchId(value: Long) extends AnyVal with LongWrapper
  object ExternalMatchId                  extends LongWrapperCompanion[ExternalMatchId]

  def cached[I[_]: Monad, F[_]: Monad: MakeRef[I, *[_]]](dm: ExternalData[F]): I[ExternalData[F]] =
    for {
      teams   <- MakeRef[I, F].of(Map.empty[ExternalData.ExternalTeamId, Option[TeamId]])
      players <- MakeRef[I, F].of(Map.empty[ExternalData.ExternalPlayerId, Option[PlayerId]])
    } yield CachedMeta[F](teams, players, dm)

  implicit val functorK: FunctorK[ExternalData] = cats.tagless.Derive.functorK

  private case class CachedMeta[F[_]: Monad](
      teams: Ref[F, Map[ExternalData.ExternalTeamId, Option[TeamId]]],
      players: Ref[F, Map[ExternalData.ExternalPlayerId, Option[PlayerId]]],
      dm: ExternalData[F]
  ) extends ExternalData[F] {
    override def team(id: ExternalTeamId): F[Option[TeamId]] = teams.access.flatMap {
      case (v, setter) =>
        v.get(id) match {
          case Some(value) => value.pure[F]
          case None        => dm.team(id).flatMap(t => setter(v + (id -> t)).map(_ => t))
        }
    }

    override def player(id: ExternalPlayerId): F[Option[PlayerId]] = players.access.flatMap {
      case (v, setter) =>
        v.get(id) match {
          case Some(value) => value.pure[F]
          case None        => dm.player(id).flatMap(t => setter(v + (id -> t)).map(_ => t))
        }
    }

    override def saveLink(valveId: ExternalMatchId, internalId: MatchId): F[Unit] = dm.saveLink(valveId, internalId)
  }
}
