package fantasy.dota2.engine

import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import fantasy.domain.series.RoundId
import fantasy.engine.ArbiterData
import doobie.implicits._

object GameArbiterData extends ArbiterData[ConnectionIO] {
  override def lastStartedRound: ConnectionIO[Option[RoundId]] =
    sql"select last_started_round from game_arbiter_data where id = 1".query[Option[RoundId]].unique

  override def saveStarted(r: RoundId): ConnectionIO[Unit] =
    sql"update game_arbiter_data set last_started_round = $r where id = 1".update.run.void

  override def lastCompletedRound: ConnectionIO[Option[RoundId]] =
    sql"select last_completed_round from game_arbiter_data where id = 1".query[Option[RoundId]].unique

  override def saveLastCompeted(r: RoundId): ConnectionIO[Unit] =
    sql"update game_arbiter_data set last_completed_round = $r where id = 1".update.run.void
}
