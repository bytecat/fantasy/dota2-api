package fantasy.dota2.engine

import cats.Monad
import cats.syntax.functor._
import cats.syntax.eq._
import fantasy.domain.`match`.{CalculatedPointValue, RawPointValue, TotalPoints}
import fantasy.domain.player.{Player, PlayerService}
import fantasy.dota2.{D2Point, D2Position}
import fantasy.engine.MatchFinder.PlayerMatchInfo
import fantasy.engine.{MatchFinder, PointCalculus}
import CalculatedPointValue.syntax._
import cats.data.NonEmptyList

import Numeric.Implicits._

class DotaPointCalculator[F[_]: Monad: PlayerService[*[_], D2Position, D2Point]]
    extends PointCalculus[F, D2Point, D2Point] {

  import DotaPointCalculator._
  import TotalPoints.syntax._

  override def calculate(
      info: MatchFinder.MatchInfo[D2Point, RawPointValue]
  ): F[MatchFinder.MatchInfo[D2Point, CalculatedPointValue]] = {
    info.players
      .traverse(p => PlayerService[F, D2Position, D2Point].get(p.playerId).map(player => calculate(p, player)))
      .map(fillMvp)
      .map(fillTotal)
      .map(pts => info.copy(players = pts))
  }

  private def calculate(
      p: PlayerMatchInfo[D2Point, RawPointValue],
      player: Player[D2Position]
  ): PlayerMatchInfo[D2Point, CalculatedPointValue] = {
    val pts = p.points.map { case (t, v) => t -> v.toCalculated(PointTypeFactor.getOrElse(t, BigDecimal(1))) }.map {
      case (t, v) => t -> PositionFactor.get(player.position).flatMap(_.get(t)).getOrElse(1.asPoint) * v
    }
    p.copy(points = pts)
  }

  private def fillMvp(
      points: NonEmptyList[PlayerMatchInfo[D2Point, CalculatedPointValue]]
  ): NonEmptyList[PlayerMatchInfo[D2Point, CalculatedPointValue]] = {
    val mvpPlayer = points.sortBy(_.points.total).last.playerId

    points.map(p => {
      if (p.playerId === mvpPlayer) {
        p.copy(points = p.points + (D2Point.MVP -> 1.asPoint))
      } else {
        p
      }
    })
  }

  private def fillTotal(
      points: NonEmptyList[PlayerMatchInfo[D2Point, CalculatedPointValue]]
  ): NonEmptyList[PlayerMatchInfo[D2Point, CalculatedPointValue]] = {
    points.map(p => {
      p.copy(points = p.points + (D2Point.Total -> p.points.total))
    })
  }
}

object DotaPointCalculator {
  val PointTypeFactor: Map[D2Point, BigDecimal] = Map[D2Point, Double](
    D2Point.Kill         -> 0.3,
    D2Point.Assist       -> 0.15,
    D2Point.Death        -> 0.3,
    D2Point.Creeps       -> 0.003,
    D2Point.GPM          -> 0.002,
    D2Point.Wards        -> 0.1,
    D2Point.CampsStacked -> 0.5,
    D2Point.Runes        -> 0.25,
    D2Point.Stuns        -> 0.07,
    D2Point.HeroDamage   -> 0.0004,
    D2Point.HeroHill     -> 0.0004,
    D2Point.KillStreaks  -> 0.1,
    D2Point.MultiKills   -> 0.15
  ).mapValues(v => BigDecimal(v))

  val PositionFactor: Map[D2Position, Map[D2Point, CalculatedPointValue]] = Map(
    /*D2Position.Core -> Map(
      D2Point.Kill        -> BigDecimal(0.8),
      D2Point.KillStreaks -> BigDecimal(0.8),
      D2Point.MultiKills  -> BigDecimal(0.8),
      D2Point.HeroDamage  -> BigDecimal(0.9)
    ),
    D2Position.Support -> Map(
      D2Point.Kill         -> BigDecimal(1.2),
      D2Point.KillStreaks  -> BigDecimal(1.2),
      D2Point.MultiKills   -> BigDecimal(1.2),
      D2Point.HeroDamage   -> BigDecimal(1.2),
      D2Point.Wards        -> BigDecimal(0.8),
      D2Point.Stuns        -> BigDecimal(0.9),
      D2Point.CampsStacked -> BigDecimal(1.1),
      D2Point.Death        -> BigDecimal(0.6)
    )*/
  )
}
