package fantasy.dota2.engine

import cats.syntax.eq._
import cats.syntax.monadError._
import cats.instances.either._
import cats.data.{NonEmptyList, NonEmptyMap, NonEmptySet}
import cats.kernel.Order
import fantasy.domain.`match`.{CalculatedPointValue, MatchPoints, TotalPoints}
import fantasy.domain.player.{Player, PlayerId}
import fantasy.dota2.{D2Point, D2Position}
import fantasy.engine.DreamTeamSelector

import scala.collection.immutable.TreeMap

object DotaDreamTeamSelect extends DreamTeamSelector[Either[Throwable, ?], D2Point, D2Position] {
  override def select(
      points: NonEmptyList[MatchPoints[D2Point]],
      players: NonEmptySet[Player[D2Position]]
  ): Either[Throwable, NonEmptyMap[PlayerId, CalculatedPointValue]] = {
    val playerPoints = points
      .map(m => m.playerId -> TotalPoints[D2Point].total(m.points))
      .groupBy(_._1)
      .mapValues(_.map(_._2).reduce)
    val cores =
      playerPoints
        .filterKeys(id => players.find(_.id === id).exists(_.position === D2Position.Core))
        .toList
        .sortBy(_._2)
        .takeRight(3)
        .toMap
    val supports =
      playerPoints
        .filterKeys(id => players.find(_.id === id).exists(_.position === D2Position.Support))
        .toList
        .sortBy(_._2)
        .takeRight(2)
        .toMap

    implicit val ordering: Ordering[PlayerId] = Order[PlayerId].toOrdering

    NonEmptyMap
      .fromMap(TreeMap((cores ++ supports).toArray: _*))
      .toRight(new IllegalStateException("DreamTeam is empty"))
      .ensureOr(m => new IllegalStateException(s"DreamTeam not complete: expected size = 5, actual = ${m.length}"))(
        _.length == 5
      )
  }
}
