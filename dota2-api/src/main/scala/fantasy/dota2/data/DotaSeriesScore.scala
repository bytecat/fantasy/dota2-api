package fantasy.dota2.data

import cats.data.OptionT
import doobie.free.connection.ConnectionIO
import fantasy.domain.series.{Score, SeriesId, SeriesRepository, SeriesScore}
import fantasy.domain.team.TeamId

class DotaSeriesScore(seriesRepository: SeriesRepository[ConnectionIO]) extends SeriesScore[ConnectionIO] {

  import doobie.implicits._

  override def findBySeries(id: SeriesId): ConnectionIO[Option[Score]] =
    OptionT(seriesRepository.get(id)).semiflatMap { series =>
      sql"select left_team_id, right_team_id, win_left from matches where series_id = ${id}"
        .query[(TeamId, TeamId, Boolean)]
        .to[List]
        .map(_.foldLeft(Score(id, 0, 0)) {
          case (score, (left, _, winLeft)) =>
            (series.home.contains(left), winLeft) match {
              case (true, true) | (false, false) => score.copy(home = score.home + 1)
              case (true, false) | (false, true) => score.copy(guest = score.guest + 1)
            }
        })
    }.value

}
