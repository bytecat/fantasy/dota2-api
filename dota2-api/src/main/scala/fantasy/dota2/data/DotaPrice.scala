package fantasy.dota2.data

import doobie.free.connection.ConnectionIO
import fantasy.domain.player.PlayerPrice.RoundPrice
import fantasy.domain.player.{PlayerId, PlayerPrice}

object DotaPrice extends PlayerPrice[ConnectionIO] {

  import doobie.implicits._

  override def price(p: PlayerId): ConnectionIO[Option[Double]] =
    sql"select price from players where id = ${p}".query[Double].option

  override def priceHistory(p: PlayerId): fs2.Stream[ConnectionIO, PlayerPrice.RoundPrice] =
    sql"select round_id as round, value as price from prices where player_id = ${p} order by round_id desc"
      .query[RoundPrice]
      .stream
}
