package fantasy.dota2.data

import doobie.free.connection.ConnectionIO
import doobie.util.fragment.Fragment
import fantasy.domain.`match`.{CalculatedPointValue, Points}
import fantasy.domain.player.{Player, PlayerId}
import fantasy.domain.series.RoundId
import fantasy.dota2.{D2Point, D2Position}

import Numeric.Implicits._

object DotaPoints extends Points[ConnectionIO, D2Position, D2Point] {

  import doobie.implicits._
  import fantasy.util.db.postgres._

  override def lastRound(p: Player[D2Position], `type`: Option[D2Point]): ConnectionIO[CalculatedPointValue] = // todo fix, currently it work as `last match`
    (points(`type`, "points") ++ fr"where player_id = ${p.id} order by round_id desc limit 1")
      .query[Option[CalculatedPointValue]]
      .option
      .map(_.flatten.getOrElse(CalculatedPointValue(0)))

  override def all(p: Player[D2Position], `type`: Option[D2Point]): ConnectionIO[CalculatedPointValue] =
    (points(`type`, "aggregation_points") ++ fr"where player_id = ${p.id}")
      .query[Option[CalculatedPointValue]]
      .option
      .map(_.flatten.getOrElse(CalculatedPointValue(0)))

  private def points(`type`: Option[D2Point], table: String) =
    Fragment.const(s"select points->>'${`type`.map(_.id).getOrElse(D2Point.Total.id)}' from $table")

  override def saveRound(r: RoundId, p: PlayerId, points: Map[D2Point, CalculatedPointValue]): ConnectionIO[Unit] =
    sql"select points from aggregation_points where player_id = $p"
      .query[Map[D2Point, CalculatedPointValue]]
      .option
      .map(_.getOrElse(Map()))
      .flatMap { prev =>
        val crnt = points.map { case (t, v) => t -> (v + prev.getOrElse(t, CalculatedPointValue(0))) }
        sql"update aggregation_points set points = $crnt where player_id = $p".update.run.map(_ => ())
      }
}
