package fantasy.dota2.data

import java.time.LocalDateTime

import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import fantasy.domain.`match`.{Match, MatchId, MatchRepository}
import fantasy.domain.series.SeriesId
import fantasy.domain.team.TeamId

object DotaMatchRepository extends MatchRepository[ConnectionIO] {

  import fantasy.util.db.postgres._

  override def create(
      seriesId: SeriesId,
      date: LocalDateTime,
      leftTeam: TeamId,
      rightTeam: TeamId,
      winLeft: Boolean
  ): ConnectionIO[MatchId] =
    sql"""insert into matches (series_id, date, left_team_id, right_team_id, win_left) 
         | values ($seriesId, $date, $leftTeam, $rightTeam, $winLeft)""".stripMargin.update
      .withUniqueGeneratedKeys("id")

  override def update(m: Match): ConnectionIO[Unit] =
    sql"""update matches set series_id = ${m.seriesId}, date = ${m.date},
       | left_team_id = ${m.leftTeam}, right_team_id = ${m.rightTeam}, win_left = ${m.winLeft}
       | where id = ${m.id}""".stripMargin.update.run.void

  override def delete(id: MatchId): ConnectionIO[Unit] = sql"delete from matches where id = $id".update.run.void

  override def get(id: MatchId): ConnectionIO[Option[Match]] =
    sql"""select id, series_id, date, left_team_id, right_team_id, win_left
         | from matches where id = $id""".stripMargin.query[Match].option

  override def findBySeries(id: SeriesId): ConnectionIO[List[Match]] =
    sql"""select id, series_id, date, left_team_id, right_team_id, win_left
         | from matches where series_id = $id order by date""".stripMargin.query[Match].to[List]
}
