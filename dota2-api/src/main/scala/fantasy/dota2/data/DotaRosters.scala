package fantasy.dota2.data

import cats.data.NonEmptyList
import cats.syntax.apply._
import cats.syntax.functor._
import cats.syntax.flatMap._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import doobie.util.Meta
import fantasy.domain.account.AccountId
import fantasy.domain.player.PlayerId
import fantasy.domain.roster.{Roster, RosterId, RostersRepository}
import fantasy.domain.team.TeamId
import fantasy.dota2.D2PlayerRoster

class DotaRosters(userRoster: D2PlayerRoster[ConnectionIO]) extends RostersRepository[ConnectionIO] {

  import DotaRosters._

  override def get(rosterId: RosterId): ConnectionIO[Option[Roster]] =
    sql"""select id, account_id, available_money, name, players, captain, fan_team_id from rosters where id = $rosterId"""
      .query[Roster]
      .option

  override def create(
      account: AccountId,
      availableMoney: Double,
      name: String,
      players: NonEmptyList[PlayerId],
      captain: PlayerId,
      fanTeam: TeamId
  ): ConnectionIO[RosterId] =
    userRoster.id(account).flatMap { id =>
      sql"""insert into rosters (id, account_id, available_money, players, captain, name, fan_team_id)
           | values ($id, $account, $availableMoney, $players, $captain, $name, $fanTeam)
         """.stripMargin.update.run.map(_ => id) <* save(id, players)
    }

  override def delete(id: RosterId): ConnectionIO[Unit] =
    sql"delete from rosters_players where roster_id = $id".update.run >> sql"delete from rosters where id = $id".update.run.void

  override def update(roster: Roster): ConnectionIO[Unit] =
    sql""" update rosters set available_money = ${roster.availableMoney},
         | players = ${roster.players}, captain = ${roster.captain}, name = ${roster.name} where id = ${roster.id}""".stripMargin.update.run.void <* save(
      roster.id,
      roster.players
    )

  private def save(id: RosterId, players: NonEmptyList[PlayerId]) =
    sql"delete from rosters_players where roster_id = $id".update.run >> players.traverse { pid =>
      sql"insert into rosters_players (roster_id, player_id) values ($id, $pid)".update.run
    }

  override def all: fs2.Stream[ConnectionIO, Roster] =
    sql"select id, account_id, available_money, name, players, captain, fan_team_id from rosters order by id"
      .query[Roster]
      .stream
}

object DotaRosters {
  import doobie.postgres.implicits._

  implicit val playersIdMeta: Meta[NonEmptyList[PlayerId]] =
    Meta[Array[Long]]
      .timap(arr => NonEmptyList.fromListUnsafe(arr.toList.map(PlayerId)))(_.toList.map(_.value).toArray)
}
