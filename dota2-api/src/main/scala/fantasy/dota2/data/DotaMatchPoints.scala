package fantasy.dota2.data

import cats.data.NonEmptyList
import cats.syntax.traverse._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.instances.list._
import cats.instances.either._
import cats.Monad
import doobie.free.connection.ConnectionIO
import fantasy.domain.`match`.{
  CalculatedPointValue,
  MatchId,
  MatchPointService,
  MatchPoints,
  MatchService,
  PointId,
  RawPointValue,
  MatchError => Err
}
import fantasy.domain.player.PlayerId
import fantasy.domain.series.{Series, SeriesId, SeriesService}
import fantasy.dota2.D2Point
import fantasy.typeclass.Raise
import fantasy.typeclass.Raise.syntax._
import fantasy.ApiError.{InternalError => FInternal}
import fantasy.{LiftCIO, LiftStream}
import fantasy.domain.`match`.MatchPointService.GroupBySeries
import fantasy.domain.team.TeamId
import fantasy.util.http.PageRequest
import tofu.lift.Lift

class DotaMatchPoints[I[_]: Monad: LiftCIO: LiftStream[ConnectionIO, *[_]]](
    matches: MatchService[I],
    seriesService: SeriesService[I]
)(
    implicit R: Raise[I, Err]
) extends MatchPointService[I, D2Point] {

  import doobie.implicits._
  import fantasy.util.db.postgres._

  private def execStream[A] = Lift[fs2.Stream[ConnectionIO, *], fs2.Stream[I, *]].lift[A](_)

  type Raw = (Long, PlayerId, TeamId, MatchId, Map[Int, CalculatedPointValue], Map[Int, RawPointValue])

  override def create(
      gameId: MatchId,
      playerId: PlayerId,
      teamId: TeamId,
      points: Map[D2Point, CalculatedPointValue],
      raw: Map[D2Point, RawPointValue]
  ): I[Unit] = {
    for {
      series <- getSeries(gameId)
      _ <- Lift[ConnectionIO, I].lift(
            sql"""insert into points (player_id, team_id, match_id, series_id, points, raw_data, round_id)
             | values ($playerId, $teamId, $gameId, ${series.id}, $points, $raw, ${series.round})""".stripMargin.update.run
          )
    } yield ()
  }

  override def get(id: Long): I[MatchPoints[D2Point]] = Lift[ConnectionIO, I].lift {
    sql"select id, player_id, team_id, match_id, points, raw_data from points where id = $id"
      .query[Raw]
      .unique
      .flatMap(mapTuple)
  }

  override def get(playerId: PlayerId, matchId: MatchId): I[MatchPoints[D2Point]] =
    Lift[ConnectionIO, I].lift {
      sql"select id, player_id, team_id, match_id, points, raw_data from points where player_id = $playerId and match_id = $matchId"
        .query[Raw]
        .unique
        .flatMap(mapTuple)
    }

  override def findByMatch(matchId: MatchId): I[NonEmptyList[MatchPoints[D2Point]]] = Lift[ConnectionIO, I].lift {
    sql"select id, player_id, team_id, match_id, points, raw_data from points where match_id = $matchId order by id"
      .query[Raw]
      .nel
      .flatMap(_.traverse(mapTuple))
  }

  override def findByPlayer(playerId: PlayerId, page: PageRequest): I[List[MatchPoints[D2Point]]] =
    Lift[ConnectionIO, I].lift(
      sql"select id, player_id, team_id, match_id, points, raw_data from points where player_id = $playerId order by id"
        .query[Raw]
        .to[List]
        .flatMap(_.traverse(mapTuple))
    )

  override def update(p: MatchPoints[D2Point]): I[Unit] =
    for {
      series <- getSeries(p.matchId)
      _ <- Lift[ConnectionIO, I].lift(
            sql"""update points set player_id = ${p.playerId}, match_id = ${p.matchId}, series_id = ${series.id},
               | points = ${simplifyMap(p.points)}, raw_data = ${simplifyMap(p.raw)}, round_id = ${series.round}
               | where id = ${p.id}""".stripMargin.update.run
          )
    } yield ()

  override def delete(id: Long): I[Unit] =
    Lift[ConnectionIO, I].lift(sql"delete from points where id = $id".update.run.map(_ => ()))

  override def findByPlayerAndGroupBySeries(
      playerId: PlayerId
  ): fs2.Stream[I, MatchPointService.GroupBySeries[D2Point]] =
    execStream {
      sql""" select id, player_id, team_id, match_id, points, raw_data, series_id
         | from points where player_id = $playerId order by series_id desc""".stripMargin
        .query[(Long, PlayerId, TeamId, MatchId, Map[Int, CalculatedPointValue], Map[Int, RawPointValue], SeriesId)]
        .stream
        .groupAdjacentBy(_._7)
        .evalMap(t => t._2.toList.traverse(it => mapTuple((it._1, it._2, it._3, it._4, it._5, it._6))).map(t._1 -> _))
        .map { case (series, points) => GroupBySeries(series, NonEmptyList.fromListUnsafe(points)) }
    }

  private def simplifyMap[V](m: Map[D2Point, V]) = m.map { case (point, value) => (point.id, value) }

  private def mapPoints[V](points: Map[Int, V]) =
    points.filter(t => t._1 != D2Point.Total.id).toList.traverse {
      case (p, v) =>
        PointId[D2Point]
          .fromId(p)
          .toRight(FInternal(new IllegalStateException(s"can't match D2Point from $p")))
          .map(_ -> v)
    }

  private def mapTuple(t: Raw) = t match {
    case (id, playerId, teamId, matchId, pointsDb, rawDataDb) =>
      val either = for {
        points <- mapPoints(pointsDb)
        raw    <- mapPoints(rawDataDb)
      } yield MatchPoints(id, matchId, playerId, teamId, points.toMap, raw.toMap)
      either.liftTo[ConnectionIO]
  }

  private def getSeries(gameId: MatchId): I[Series] =
    for {
      m      <- matches.get(gameId)
      series <- seriesService.get(m.seriesId)
    } yield series

}
