package fantasy.dota2.data

import cats.syntax.functor._
import doobie.implicits._
import doobie.free.connection.ConnectionIO
import fantasy.domain.account.AccountId
import fantasy.domain.league.{League, LeagueId, LeagueRepository, LeagueType}
import fantasy.domain.series.RoundId

object DotaLeagues extends LeagueRepository[ConnectionIO] {
  override def create(
      name: String,
      started: RoundId,
      `type`: LeagueType,
      author: Option[AccountId]
  ): ConnectionIO[LeagueId] =
    sql"insert into leagues (name, started, type, author) values ($name, $started, ${`type`}, $author)".update
      .withUniqueGeneratedKeys("id")

  override def get(id: LeagueId): ConnectionIO[Option[League]] =
    sql"select id, name, started, type, author from leagues where id = $id".query[League].option

  override def update(league: League): ConnectionIO[Unit] =
    sql"""update leagues set name = ${league.name}, started = ${league.started}, type = ${league.`type`}, author = ${league.author}
         | where id = ${league.id}
       """.stripMargin.update.run.void

  override def delete(id: LeagueId): ConnectionIO[Unit] =
    sql"delete from leagues where id = ${id}".update.run.void

  override def byType(`type`: LeagueType): ConnectionIO[List[League]] =
    sql"select id, name, started, type, author from leagues where type = ${`type`} order by id".query[League].to[List]
}
