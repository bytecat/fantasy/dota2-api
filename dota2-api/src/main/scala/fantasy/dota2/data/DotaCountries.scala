package fantasy.dota2.data

import doobie.free.connection.ConnectionIO
import doobie.syntax.string._
import fantasy.domain.common.{CountriesRepository, Country, CountryId}

object DotaCountries extends CountriesRepository[ConnectionIO] {

  import fantasy.util.db.postgres._

  override def all: ConnectionIO[List[Country]] =
    sql"select id, name, code from countries order by id".query[Country].to[List]

  override def get(id: CountryId): ConnectionIO[Option[Country]] =
    sql"select id, name, code from countries where id = $id".query[Country].option
}
