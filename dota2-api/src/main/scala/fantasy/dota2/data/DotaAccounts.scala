package fantasy.dota2.data

import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import fantasy.domain.account.{Account, AccountId, AccountRepository, HashedPass}
import fantasy.domain.common.CountryId
import fantasy.util.i18n.Language

object DotaAccounts extends AccountRepository[ConnectionIO] {

  import doobie.implicits._

  def create(
      name: String,
      email: String,
      language: Language,
      countryId: CountryId,
      password: HashedPass,
      EULA: Boolean
  ): ConnectionIO[Unit] =
    sql"insert into accounts (email, password, name, language, EULA, country_id) values ($email, $password, $name, $language, $EULA, $countryId)".update.run.void

  def get(id: AccountId): ConnectionIO[Option[Account]] =
    sql"select id, name, email, language, password, country_id from accounts where id = $id"
      .query[Account]
      .option

  def get(email: String): ConnectionIO[Option[Account]] =
    sql"select id, name, email, language, password, country_id from accounts where email = $email"
      .query[Account]
      .option

  def update(account: Account): ConnectionIO[Unit] =
    sql"""update accounts set name = ${account.name}, email = ${account.email}, language = ${account.language},
         |password = ${account.password}, country_id = ${account.countryId} where id = ${account.id}""".update.run.void
}
