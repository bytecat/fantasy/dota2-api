package fantasy.dota2.data

import java.time.LocalDate

import cats.syntax.functor._
import doobie.implicits._
import doobie.free.connection.ConnectionIO
import fantasy.domain.common.CountryId
import fantasy.domain.series.{Tournament, TournamentId, TournamentRepository}
import fantasy.util.http.PageRequest

object DotaTournaments extends TournamentRepository[ConnectionIO] {
  override def create(
      name: String,
      startDate: LocalDate,
      endDate: LocalDate,
      country: CountryId,
      logo: Option[String]
  ): ConnectionIO[Unit] =
    sql"""insert into tournaments (name, start_date, end_date, country_id, logo)
         | values ($name, $startDate, $endDate, $country, $logo)
       """.stripMargin.update.run.void

  override def get(id: TournamentId): ConnectionIO[Option[Tournament]] =
    sql"select id, name, start_date, end_date, country_id, logo from tournaments where id = $id"
      .query[Tournament]
      .option

  override def update(tournament: Tournament): ConnectionIO[Unit] =
    sql"""update tournamets set name = ${tournament.name}, start_date = ${tournament.startDate},
         | end_date = ${tournament.endDate}, country_id = ${tournament.country}, logo = ${tournament.logo}
         | where id = ${tournament.id}
       """.stripMargin.update.run.void

  override def delete(id: TournamentId): ConnectionIO[Unit] =
    sql"delete from tournaments where id = ${id}".update.run.void

  override def get(page: PageRequest): ConnectionIO[List[Tournament]] =
    page
      .paginate[Tournament](
        sql"select id, name, start_date, end_date, country_id, logo from tournaments order by start_date desc"
      )
      .to[List]

  override def count: ConnectionIO[Int] = sql"select count(1) from tournaments".query[Int].unique
}
