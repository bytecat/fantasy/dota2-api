package fantasy.dota2.data

import cats.syntax.order._
import cats.data.NonEmptyList
import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import fantasy.domain.dreamteam.{DreamTeam, DreamTeamRepository}
import fantasy.domain.player.PlayerId
import fantasy.domain.series.RoundId

object DotaDreamTeam extends DreamTeamRepository[ConnectionIO] {

  import fantasy.util.db.postgres._
  import DotaRosters._ // meta for nel playerId

  override def create(d: DreamTeam): ConnectionIO[Unit] =
    sql"insert into dream_teams(round_id, players, points, teams) values (${d.round}, ${d.players}, ${d.points}, ${d.teams})".update.run.void

  override def update(d: DreamTeam): ConnectionIO[Unit] =
    sql"update dream_teams set players = ${d.players}, points = ${d.points}, teams = ${d.teams} where round_id = ${d.round}".update.run.void

  override def delete(d: DreamTeam): ConnectionIO[Unit] =
    sql"delete from dream_teams where round_id = ${d.round}".update.run.void

  override def get(round: RoundId): ConnectionIO[Option[DreamTeam]] =
    sql"select round_id, players, points, teams from dream_teams where round_id = $round".query[DreamTeam].option

  override def inDreamTeam(playerId: PlayerId, roundId: RoundId): ConnectionIO[Boolean] =
    sql"select players from dream_teams where round_id = $roundId"
      .query[NonEmptyList[PlayerId]]
      .option
      .map(_.exists(_.exists(_ === playerId)))
}
