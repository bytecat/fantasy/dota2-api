package fantasy.dota2.data

import java.time.LocalDateTime

import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import fantasy.domain.series.{RoundId, Series, SeriesId, SeriesRepository, Stage}
import fantasy.domain.team.TeamId
import fantasy.util.http.PageRequest

object DotaSeriesRepository extends SeriesRepository[ConnectionIO] {

  import doobie.implicits._
  import fantasy.util.db.postgres._

  override def create(
      homeId: TeamId,
      guestId: TeamId,
      time: LocalDateTime,
      round: Int,
      maxGames: Int,
      stage: Stage
  ): ConnectionIO[SeriesId] =
    sql"""insert into series(home_team_id, guest_team_id, time, round_id, max_games, stage)
         | values ($homeId, $guestId, $time, $round, $maxGames, $stage)""".stripMargin.update
      .withUniqueGeneratedKeys("id")

  override def get(id: SeriesId): ConnectionIO[Option[Series]] =
    sql"""select id, home_team_id, guest_team_id, time, round_id, max_games, stage
         | from series where id = $id
       """.stripMargin.query[Series].option

  override def update(series: Series): ConnectionIO[Unit] =
    sql"""update series set home_team_id = ${series.home}, guest_team_id = ${series.guest}, time = ${series.time},
         | round_id = ${series.round}, max_games = ${series.maxGames}, stage = ${series.stage} where id = ${series.id}
       """.stripMargin.update.run.void

  override def delete(id: SeriesId): ConnectionIO[Unit] = sql"delete from series where id = ${id}".update.run.void

  override def findByRound(round: RoundId): ConnectionIO[List[Series]] =
    sql"""select id, home_team_id, guest_team_id, time, round_id, max_games, stage
         | from series where round_id = ${round} order by time, id""".stripMargin.query[Series].to[List]

  override def findByTeam(id: TeamId, page: PageRequest): ConnectionIO[List[Series]] =
    page.paginate[Series](fr"""select id, home_team_id, guest_team_id, time, round_id, max_games, stage
         | from series where home_team_id = $id or guest_team_id = $id order by time
       """.stripMargin).to[List]

  override def countByTeam(id: TeamId): ConnectionIO[Int] =
    sql"""select count(1) from series where home_team_id = $id or guest_team_id = $id""".stripMargin.query[Int].unique

  override def get(page: PageRequest): ConnectionIO[List[Series]] =
    page.paginate[Series](fr"""select id, home_team_id, guest_team_id, time, round_id, max_games, stage
         | from series order by id desc
       """.stripMargin).to[List]

  override def count: ConnectionIO[Int] = sql"select count(1) from series".query[Int].unique
}
