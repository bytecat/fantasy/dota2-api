package fantasy.dota2.data

import cats.syntax.functor._
import doobie.implicits._
import doobie.free.connection.ConnectionIO
import fantasy.domain.league.{LeagueId, LeagueTableRepository, LeagueTableRow}
import fantasy.domain.roster.RosterId
import fantasy.util.http.PageRequest

object DotaLeagueTable extends LeagueTableRepository[ConnectionIO] {

  import fantasy.util.db.postgres._

  override def get(roster: RosterId, league: LeagueId): ConnectionIO[Option[LeagueTableRow]] =
    sql"""select roster_id, league_id, places, points, total_points
         | from leagues_table where roster_id = $roster and league_id = $league """.stripMargin
      .query[LeagueTableRow]
      .option

  override def get(roster: RosterId): ConnectionIO[List[LeagueTableRow]] =
    sql"""select roster_id, league_id, places, points, total_points
         | from leagues_table where roster_id = $roster order by league_id""".stripMargin
      .query[LeagueTableRow]
      .to[List]

  override def get(leagueId: LeagueId, page: PageRequest): ConnectionIO[List[LeagueTableRow]] =
    page
      .paginate[LeagueTableRow](
        fr"""select roster_id, league_id, places, points, total_points
         | from leagues_table where league_id = $leagueId order by total_points desc, roster_id asc""".stripMargin
      )
      .to[List]

  override def count(leagueId: LeagueId): ConnectionIO[Long] =
    sql"select count(1) from leagues_table where league_id = $leagueId".query[Long].unique

  override def delete(roster: RosterId, league: LeagueId): ConnectionIO[Unit] =
    sql"delete from leagues_table where league_id = $league and roster_id = $roster".update.run.void

  override def create(row: LeagueTableRow): ConnectionIO[Unit] =
    sql"""insert into leagues_table (roster_id, league_id, places, points, total_points)
         | values (${row.roster}, ${row.league}, ${row.places}, ${row.points}, ${row.totalPoints})""".stripMargin.update.run.void

  override def update(row: LeagueTableRow): ConnectionIO[Unit] =
    sql"""update leagues_table set places = ${row.places}, points = ${row.points}, total_points = ${row.totalPoints}
         | where roster_id = ${row.roster} and league_id = ${row.league}
       """.stripMargin.update.run.void

  override def all(leagueId: LeagueId): ConnectionIO[List[LeagueTableRow]] =
    sql"""select roster_id, league_id, places, points, total_points
         | from leagues_table where league_id = $leagueId order by total_points desc, roster_id asc""".stripMargin
      .query[LeagueTableRow]
      .to[List]
}
