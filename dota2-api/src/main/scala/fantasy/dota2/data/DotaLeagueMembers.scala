package fantasy.dota2.data

import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import fantasy.domain.league.LeagueMembers.Member
import fantasy.domain.league.{LeagueId, LeagueMembers}
import fantasy.domain.roster.RosterId
import fantasy.domain.series.RoundId

object DotaLeagueMembers extends LeagueMembers[ConnectionIO] {
  override def join(roster: RosterId, league: LeagueId, from: RoundId): ConnectionIO[Unit] =
    sql"insert into league_members (league_id, roster_id, from_round) values ($league, $roster, $from)".update.run.void

  override def membersCount(leagueId: LeagueId): ConnectionIO[Long] =
    sql"select count(1) from league_members where league_id = $leagueId".query[Long].unique

  override def consists(roster: RosterId): ConnectionIO[List[Member]] =
    sql"select roster_id, from_round, league_id from league_members where roster_id = $roster order by league_id"
      .query[Member]
      .to[List]
}
