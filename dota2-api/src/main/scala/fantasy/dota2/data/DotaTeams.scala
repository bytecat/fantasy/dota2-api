package fantasy.dota2.data

import doobie.free.connection.ConnectionIO
import fantasy.domain.team.{Region, Team, TeamId, TeamsRepository}
import fantasy.util.http.PageRequest

object DotaTeams extends TeamsRepository[ConnectionIO] {

  import doobie.implicits._
  import doobie.Fragments.whereAndOpt

  override def get(id: TeamId): ConnectionIO[Option[Team]] =
    sql"select id, logo_url as logoUrl, name, tag, region from teams where id = $id".query[Team].option

  override def create(name: String, tag: String, region: Region, logoUrl: Option[String]): ConnectionIO[TeamId] =
    sql"insert into teams(logo_url, name, tag, region) values ($name, $tag, $region, $logoUrl)".update
      .withUniqueGeneratedKeys("id")

  override def update(team: Team): ConnectionIO[Unit] =
    sql"update teams set logo_url = ${team.logoUrl}, name = ${team.name}, tag = ${team.tag}, region = ${team.region} where id = ${team.id}".update.run
      .map(_ => ())

  override def delete(id: TeamId): ConnectionIO[Unit] = sql"delete from teams where id = $id".update.run.map(_ => ())

  override def find(name: Option[String], page: PageRequest): ConnectionIO[List[Team]] =
    page
      .paginate[Team](
        (fr"select id, logo_url as logoUrl, name, tag, region from teams " ++ buildFilter(name) ++ fr" order by id")
      )
      .to[List]

  override def count(name: Option[String]): ConnectionIO[Int] =
    (fr"select count(1) from teams " ++ buildFilter(name)).query[Int].unique

  private def buildFilter(name: Option[String]) = whereAndOpt(
    name
      .filter(_.nonEmpty)
      .map(s => s"%$s%".toLowerCase)
      .map(s => fr"LOWER(NAME) like $s or LOWER(tag) like $s")
  )
}
