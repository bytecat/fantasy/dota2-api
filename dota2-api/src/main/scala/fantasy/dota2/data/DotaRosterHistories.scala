package fantasy.dota2.data

import cats.syntax.apply._
import cats.syntax.functor._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import fantasy.domain.roster.{RosterHistoriesRepository, RosterHistory, RosterId}
import fantasy.domain.series.RoundId

object DotaRosterHistories extends RosterHistoriesRepository[ConnectionIO] {

  import DotaRosters._
  import fantasy.util.db.postgres._

  override def get(roster: RosterId): ConnectionIO[List[RosterHistory]] =
    sql"""select roster_id, round_id, players, captain, points, total, teams from roster_histories
         | where roster_id = $roster order by round_id desc
       """.stripMargin.query[RosterHistory].to[List]

  override def get(roster: RosterId, round: RoundId): ConnectionIO[Option[RosterHistory]] =
    sql"""select roster_id, round_id, players, captain, points, total, teams from roster_histories
         | where roster_id = $roster and round_id = $round
       """.stripMargin.query[RosterHistory].option

  override def create(roster: RosterHistory): ConnectionIO[Unit] =
    sql""" insert into roster_histories (roster_id, round_id, players, captain, points, total, teams)
         | values (${roster.id}, ${roster.round}, ${roster.players}, ${roster.captain},
         | ${roster.points}, ${roster.total}, ${roster.teams})
       """.stripMargin.update.run *> save(roster).void

  override def update(roster: RosterHistory): ConnectionIO[Unit] =
    sql""" update roster_histories set players = ${roster.players}, captain = ${roster.captain},
         | points = ${roster.points}, teams = ${roster.teams}, total = ${roster.total}
         | where roster_id = ${roster.id} and round_id = ${roster.round}
       """.stripMargin.update.run *> sql"""delete from roster_histories_players
                                              | where roster_id = ${roster.id}""".stripMargin.update.run *> save(roster).void

  override def delete(roster: RosterHistory): ConnectionIO[Unit] =
    sql"delete from roster_histories where roster_id = ${roster.id} and round_id = ${roster.round}".update.run *>
      sql"delete from roster_histories_players where roster_id = ${roster.id} and round_id = ${roster.round}".update.run.void

  override def delete(roster: RosterId): ConnectionIO[Unit] =
    sql"delete from roster_histories where roster_id = $roster".update.run *>
      sql"""delete from roster_histories_players 
           | where roster_id = $roster""".update.run.void

  private def save(r: RosterHistory) = r.players.traverse { p =>
    sql"insert into roster_histories_players (roster_id, round_id, player_id, team_id) values (${r.id}, ${r.round}, $p, ${r.teams
      .get(p)})".update.run
  }
}
