package fantasy.dota2.data

import cats.syntax.apply._
import cats.instances.option._
import doobie.free.connection.ConnectionIO
import doobie.util.fragment.Fragment
import fantasy.domain.common.CountryId
import fantasy.domain.player.{Player, PlayerId, PlayerRepository}
import fantasy.domain.player.PlayerRepository.PlayerFilter
import fantasy.domain.team.TeamId
import fantasy.dota2.{D2Point, D2Position}
import fantasy.util.http.{PageRequest, SortBy}

object DotaPlayers extends PlayerRepository[ConnectionIO, D2Position, D2Point] {

  import doobie.implicits._
  import doobie.Fragments.whereAndOpt

  override def create(
      name: String,
      nick: String,
      photo: Option[String],
      teamId: TeamId,
      position: D2Position,
      country: CountryId
  ): ConnectionIO[PlayerId] =
    sql"""insert into players (name, nick, photo, team_id, position, country_id)
         | values ($name, $nick, $photo, $teamId, country)""".stripMargin.update.withUniqueGeneratedKeys("id")

  override def get(id: PlayerId): ConnectionIO[Option[Player[D2Position]]] =
    sql"""select id, name, nick, photo, team_id as teamId, position, country_id as coutry
         | from players where id = $id
       """.stripMargin.query[Player[D2Position]].option

  override def delete(id: PlayerId): ConnectionIO[Unit] =
    sql"delete from players where id = $id".update.run.map(_ => ())

  override def update(player: Player[D2Position]): ConnectionIO[Unit] = {
    sql""" update players set nick = ${player.nick},
         | photo = ${player.photo}, team_id = ${player.teamId}, position = ${player.position},
         | country_id = ${player.country} where id = ${player.id}""".stripMargin.update.run.map(_ => ())
  }

  override def filter(
      query: PlayerFilter[D2Position],
      sortBy: Option[SortBy[D2Point]],
      page: PageRequest
  ): ConnectionIO[List[Player[D2Position]]] =
    page
      .paginate[Player[D2Position]](
        (fr""" select p.id, p.name, p.nick, p.photo, p.team_id, p.position, p.country_id from players as p
           | left join aggregation_points ap on p.id = ap.player_id """.stripMargin
          ++ buildFilter(query)
          ++ Fragment
            .const(buildOrder(sortBy)))
      )
      .to[List]

  override def count(query: PlayerFilter[D2Position]): ConnectionIO[Int] =
    (fr0"select count(1) from players as p " ++ buildFilter(query)).query[Int].unique

  private def buildFilter(query: PlayerFilter[D2Position]) = whereAndOpt(
    query.name
      .filter(_.nonEmpty)
      .map(s => s"%$s%".toLowerCase)
      .map(n => fr"LOWER(name) like $n or LOWER(nick) like $n"),
    query.team.map(t => fr"team_id = $t"),
    query.position.map(t => fr"position = ${t.id}"),
    query.minPrice.map(p => fr"price >= $p"),
    query.maxPrice.map(p => fr"price <= $p"),
    query.hasMatches.map2(query.currentRound) { (has, round) =>
      (if (!has) fr"not " else fr"") ++
        fr""" exists(select 1 from series s join teams t on t.id = s.home_team_id or t.id = s.guest_team_id
            | where s.round_id = $round and t.id = p.team_id) """.stripMargin
    }
  )

  private def buildOrder(sortBy: Option[SortBy[D2Point]]) =
    sortBy
      .map(s => s" order by COALESCE(cast(ap.points->>'${s.by.id}' as real), 0) ${s.direction}, p.id")
      .getOrElse(s" order by COALESCE(cast(ap.points->>'${D2Point.Total.id}' as real), 0) DESC, p.id")

}
