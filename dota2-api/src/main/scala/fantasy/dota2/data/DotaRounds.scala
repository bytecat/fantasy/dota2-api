package fantasy.dota2.data

import java.time.LocalDateTime

import cats.syntax.functor._
import cats.syntax.apply._
import cats.instances.option._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import fantasy.domain.`match`.CalculatedPointValue
import fantasy.domain.roster.RosterId
import fantasy.domain.series.{Round, RoundId, RoundPoints, RoundRepository, TournamentId}

object DotaRounds extends RoundRepository[ConnectionIO] {

  import fantasy.util.db.postgres._

  type RawRound =
    (
        RoundId,
        LocalDateTime,
        LocalDateTime,
        TournamentId,
        Option[CalculatedPointValue],
        Option[CalculatedPointValue],
        Option[RosterId]
    )

  override def create(from: LocalDateTime, to: LocalDateTime, tournamentId: TournamentId): ConnectionIO[Unit] =
    sql"""insert into rounds (from_date, to_date, tournament_id)
         | values ($from, $to, $tournamentId)""".stripMargin.update.run.void

  override def get(number: RoundId): ConnectionIO[Option[Round]] =
    sql"""select id, from_date, to_date, tournament_id, average_points, top_points, top_roster_id from rounds where id = ${number}"""
      .query[RawRound]
      .map(map)
      .option

  override def delete(number: RoundId): ConnectionIO[Unit] =
    sql"delete from rounds where id = ${number}".update.run.void

  override def update(round: Round): ConnectionIO[Unit] =
    sql"""update rounds set from_date = ${round.from}, to_date = ${round.to},
         | tournament_id = ${round.tournament}, average_points = ${round.points.map(_.average)},
         | top_points = ${round.points.map(_.top)}, top_roster_id = ${round.points.map(_.topRoster)}
         | where id = ${round.number}""".stripMargin.update.run.void

  override def current(): ConnectionIO[Option[Round]] =
    sql"""select id, from_date, to_date, tournament_id, average_points, top_points, top_roster_id from rounds
         | where from_date <= now() and now() <= to_date""".stripMargin.query[RawRound].map(map).option

  override def lastCompleted(): ConnectionIO[Option[Round]] =
    sql"""select id, from_date, to_date, tournament_id, average_points, top_points, top_roster_id from rounds
         | where from_date <= now() and now() >= to_date order by from_date desc limit 1""".stripMargin
      .query[RawRound]
      .map(map)
      .option

  def map(t: RawRound): Round = Round(t._1, t._2, t._3, t._4, (t._5, t._6, t._7).mapN(RoundPoints.apply))
}
