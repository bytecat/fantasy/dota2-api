package fantasy.dota2.data

import fantasy.domain.player.PlayerId
import fantasy.domain.team.TeamId
import fantasy.dota2.engine.ExternalData
import doobie._
import doobie.implicits._
import fantasy.domain.`match`.MatchId

object DotaValve extends ExternalData[ConnectionIO] {
  override def team(id: ExternalData.ExternalTeamId): ConnectionIO[Option[TeamId]] =
    sql"select team_id from valve_teams where valve_id = $id".query[TeamId].option

  override def player(id: ExternalData.ExternalPlayerId): ConnectionIO[Option[PlayerId]] =
    sql"select player_id from valve_players where valve_id = $id".query[PlayerId].option

  override def saveLink(valveId: ExternalData.ExternalMatchId, internalId: MatchId): ConnectionIO[Unit] =
    sql"insert into valve_matches(valve_id, match_id) values ($valveId, $internalId)".update.run.map(_ => ())
}
