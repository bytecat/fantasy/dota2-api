package fantasy.opendota

import cats.Monad
import cats.effect.Sync
import com.twitter.finagle.{Http, Service}
import com.twitter.finagle.http.{Method, Request, Response}
import com.twitter.util.Future
import fantasy.opendota.model.{OpenDotaMatch, OpenDotaMatchInfo}
import io.circe.parser._
import simulacrum.typeclass

@typeclass
trait OpenDotaClient[F[_]] {
  def matches(lessThen: Option[Long]): F[List[OpenDotaMatch]]
  def get(id: Long): F[Option[OpenDotaMatchInfo]]
}
object OpenDotaClient {
  def create[I[_]: Sync, F[_]: Monad: DeferFinagleFuture](service: String, token: String): I[OpenDotaClient[F]] =
    Sync[I].delay {
      new FinagleOpenDotaClient[F](
        Http.client
          .withTls(service.substring(0, service.indexOf(':')))
          .newClient(service, "OpenDotaClient")
          .toService,
        token
      )
    }

}

//https://twitter.github.io/finagle/guide/Clients.html
final class FinagleOpenDotaClient[F[_]: DeferFinagleFuture: Monad](client: Service[Request, Response], token: String)
    extends OpenDotaClient[F] {
  override def matches(lessThen: Option[Long]): F[List[OpenDotaMatch]] = DeferFinagleFuture[F].defer {
    client(Request(Method.Get, s"/api/proMatches${lessThen.map("?less_than_match_id=" + _).getOrElse("")}"))
      .filter(resp => resp.status.code == 200)
      .flatMap { resp =>
        parse(resp.contentString).flatMap(_.as[List[OpenDotaMatch]]) match {
          case Left(err) => Future.exception(err)
          case Right(v)  => Future.value(v)
        }
      }
  }

  override def get(id: Long): F[Option[OpenDotaMatchInfo]] = DeferFinagleFuture[F].defer {
    client(Request(Method.Get, s"/api/matches/$id"))
      .filter(resp => resp.status.code == 200 || resp.status.code == 404)
      .flatMap { resp =>
        resp.status.code match {
          case 404 => Future.value(None)
          case _ =>
            parse(resp.contentString).flatMap(_.as[OpenDotaMatchInfo]) match {
              case Left(err) => Future.exception(err)
              case Right(v)  => Future.value(Some(v))
            }
        }
      }
  }
}
