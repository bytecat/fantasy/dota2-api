package fantasy.opendota

import com.twitter.util.Future
import simulacrum.typeclass
@typeclass
trait DeferFinagleFuture[F[_]] {
  def defer[T](future: => Future[T]): F[T]
}
