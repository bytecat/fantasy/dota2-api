package fantasy.opendota.model

import cats.Show
import io.circe.Decoder
import io.circe.derivation._

case class Team(teamId: Option[Long], name: Option[String])
object Team {
  implicit val decoder: Decoder[Team] = deriveDecoder(io.circe.derivation.renaming.snakeCase)
}

case class OpenDotaMatchInfo(
    matchId: Long,
    radiantWin: Boolean,
    startTime: Long,
    duration: Long,
    radiantTeam: Option[Team],
    direTeam: Option[Team],
    players: List[OpenDotaPlayerInMatch]
)

object OpenDotaMatchInfo {
  implicit val decoder: Decoder[OpenDotaMatchInfo] = deriveDecoder(io.circe.derivation.renaming.snakeCase)
  implicit val show: Show[OpenDotaMatchInfo]       = Show.fromToString

}

case class OpenDotaPlayerInMatch(
//    matchId: Option[Long],
//    playerSlot: Option[Int],
//    abilityUpgradesArr: Option[Seq[Int]],
    accountId: Option[Long],
    assists: Option[Int],
    campsStacked: Option[Int],
    creepsStacked: Option[Int],
    deaths: Option[Int],
    denies: Option[Int],
    gold: Option[Int],
    goldPerMin: Option[Int],
    heroDamage: Option[Int],
    heroHealing: Option[Int],
//    heroId: Option[Int],
    killStreaks: Map[String, Int], // "1": 2
    kills: Option[Int],
    lastHits: Option[Int],
//    leaverStatus: Option[Int],
//    level: Option[Int],
    multiKills: Map[String, Int], // "1": 2
    runePickups: Option[Int],
//    runes: Option[Map[String, Int]],
    obsPlaced: Option[Int], // obersvers
    senPlaced: Option[Int], // sentries
    stuns: Option[Double],
    towerDamage: Option[Int],
    xpPerMin: Option[Int],
    name: Option[String], // nick
    radiantWin: Option[Boolean],
//    startTime: Option[Int],
//    duration: Option[Int],
    isRadiant: Option[Boolean],
//    win: Option[Int],
//    lose: Option[Int],
    totalGold: Option[Int],
    totalXp: Option[Int],
    killsPerMin: Option[Double],
//    kda: Option[Double],
//    abandons: Option[Int],
    neutralKills: Option[Int],
    towerKills: Option[Int],
    courierKills: Option[Int],
    laneKills: Option[Int],
    heroKills: Option[Int],
    observerKills: Option[Int],
    sentryKills: Option[Int],
    roshanKills: Option[Int],
    necronomiconKills: Option[Int],
    ancientKills: Option[Int],
//    buybackCount: Option[Int],
    observerUses: Option[Int],
    sentryUses: Option[Int],
    laneEfficiency: Option[Double],
    laneEfficiencyPct: Option[Double],
    lane: Option[Int],
    laneRole: Option[Int],
    isRoaming: Option[Boolean]
)

object OpenDotaPlayerInMatch {
  implicit val decoder: Decoder[OpenDotaPlayerInMatch] = deriveDecoder {
    case x @ "isRadiant" => x
    case x               => io.circe.derivation.renaming.snakeCase(x)
  }
}
