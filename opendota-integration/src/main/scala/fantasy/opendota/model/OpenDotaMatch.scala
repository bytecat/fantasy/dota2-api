package fantasy.opendota.model

import io.circe.Decoder
import io.circe.derivation._

case class OpenDotaMatch(
    matchId: Long,
    duration: Long,
    startTime: Long,
    radiantTeamId: Option[Long],
    radiantName: Option[String],
    direTeamId: Option[Long],
    direName: Option[String],
    leagueid: Option[Long],
    seriesId: Option[Long],
    seriesType: Option[Int],
    radiantScore: Option[Int],
    direScore: Option[Int],
    radiantWin: Boolean
)

object OpenDotaMatch {

  implicit val decoder: Decoder[OpenDotaMatch] = deriveDecoder(io.circe.derivation.renaming.snakeCase)
}
