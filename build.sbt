name := "fantasy"
organization := "bytecat.io"

val gitlabRepository = Option("registry.gitlab.com/bytecat/fantasy")

lazy val `dota2-api` = project
  .in(file("dota2-api"))
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(
    name := "dota2-api",
    version := "0.0.1",
    scalaVersion := "2.12.8",
    scalacOptions := Build.scalacOptions,
    libraryDependencies ++= Build.coreDependencies,
    resolvers += Resolver.sonatypeRepo("releases"),
    dockerBaseImage := "openjdk:11",
    dockerRepository := gitlabRepository,
    dockerExposedPorts := Seq(),
    dockerExposedUdpPorts := Seq()
  )
  .settings(
    Build.compilePlugins.map(addCompilerPlugin): _*
  )
  .dependsOn(`opendota-integration`)

lazy val `dota2-db` = project
  .in(file("dota2-db"))
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "dota2-db",
    version := "0.0.1",
    scalaVersion := "2.12.8",
    scalacOptions := Build.scalacOptions,
    libraryDependencies ++= Build.migrationDependencies,
    mainClass in compile := Some("fantasy.dota.db.MigrateRunner")
  )

lazy val `opendota-integration` = project
  .in(file("opendota-integration"))
  .settings(
    name := "opendota-integration",
    version := "17.6.1",
    scalaVersion := "2.12.8",
    scalacOptions := Build.scalacOptions,
    libraryDependencies ++= Build.opendotaDependencies
  )
  .settings(
    Build.compilePlugins.map(addCompilerPlugin): _*
  )

lazy val fantasy = project
  .in(file("."))
  .aggregate(
    `dota2-api`,
    `dota2-db`,
    `opendota-integration`
  )
