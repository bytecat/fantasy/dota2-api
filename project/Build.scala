import sbt._

object Build {

  object Versions {
    val Cats         = "2.0.0-RC1"
    val CatsEffect   = "2.0.0-RC1"
    val CatsTagless  = "0.9"
    val Circe        = "0.11.1"
    val PureConfig   = "0.11.1"
    val Simulacrum   = "0.19.0"
    val TypedSchema  = "0.11.0-RC1"
    val SwaggerUI    = "3.20.5"
    val Monix        = "3.0.0-RC3"
    val Derevo       = "0.10.1"
    val Doobie       = "0.8.0-RC1"
    val FinchVersion = "0.22.0"
    val Tofu         = "0.1"
  }

  lazy val cats = Seq(
    "org.typelevel"        %% "cats-effect"         % Versions.CatsEffect,
    "org.typelevel"        %% "cats-core"           % Versions.Cats,
    "org.typelevel"        %% "cats-tagless-macros" % Versions.CatsTagless,
    "com.github.mpilquist" %% "simulacrum"          % Versions.Simulacrum,
    "org.typelevel"        %% "cats-mtl-core"       % "0.5.0"
  )

  lazy val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % Versions.Circe)

  lazy val circeDerivation = Seq("io.circe" %% "circe-derivation" % "0.11.0-M1")

  lazy val tofu = Seq(
    "ru.tinkoff" %% "tofu-core",
    "ru.tinkoff" %% "tofu-env",
    "ru.tinkoff" %% "tofu-logging"
  ).map(_ % Versions.Tofu)

  lazy val logging = Seq(
    "ch.qos.logback"             % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging"  % "3.9.2",
    "io.sentry"                  % "sentry-logback"  % "1.7.16"
  )

  lazy val commonDependencies = Seq(
    "com.roundeights"       %% "hasher"           % "1.2.0",
    "com.beachape"          %% "enumeratum"       % "1.5.13",
    "com.beachape"          %% "enumeratum-circe" % "1.5.20",
    "com.github.pureconfig" %% "pureconfig"       % "0.10.2"
  ) ++ scopt

  lazy val scopt = Seq("com.github.scopt" %% "scopt" % "4.0.0-RC2")

  lazy val fs2 = Seq(
    "co.fs2"     %% "fs2-core"      % "1.0.2",
    "co.fs2"     %% "fs2-cats"      % "0.5.0",
    "eu.timepit" %% "fs2-cron-core" % "0.1.0"
  )

  lazy val db = Seq(
    "org.tpolecat" %% "doobie-core",
    "org.tpolecat" %% "doobie-hikari",
    "org.tpolecat" %% "doobie-postgres"
  ).map(_ % Versions.Doobie)

  lazy val liquid = Seq("org.liquibase" % "liquibase-core" % "3.6.3")

  lazy val monix = Seq(
    "io.monix" %% "monix" % Versions.Monix
  )

  lazy val http = Seq(
    "ru.tinkoff"      %% "typed-schema-finagle-env"   % Versions.TypedSchema,
    "ru.tinkoff"      %% "typed-schema-finagle-circe" % Versions.TypedSchema,
    "com.pauldijou"   %% "jwt-core"                   % "1.1.0",
    "org.webjars.npm" % "swagger-ui-dist"             % Versions.SwaggerUI,
    "com.lihaoyi"     %% "scalatags"                  % "0.6.7"
  )

  lazy val derevo = Seq(
    "org.manatki" %% "derevo-tschema" % Versions.Derevo,
    "org.manatki" %% "derevo-circe"   % Versions.Derevo
  )

  lazy val compilePlugins = Seq(
    "org.typelevel"   %% "kind-projector"     % "0.10.3",
    "com.olegpy"      %% "better-monadic-for" % "0.3.0-M4",
    "org.scalamacros" % "paradise"            % "2.1.0" cross CrossVersion.full
  )

  lazy val finagle = Seq("com.twitter" %% "finagle-http" % "19.3.0")

  lazy val pg = Seq("org.postgresql" % "postgresql" % "42.2.5")

  lazy val circeyaml = Seq("io.circe" %% "circe-yaml" % "0.8.0")

  lazy val migrationDependencies = cats ++ logging ++ liquid ++ scopt ++ pg ++ circeyaml ++ circe

  lazy val coreDependencies = cats ++ circe ++ logging ++ commonDependencies ++ fs2 ++ db ++ http ++ monix ++ derevo ++ testLib ++ tofu

  lazy val opendotaDependencies = circe ++ cats ++ finagle ++ circeDerivation

  lazy val testLib =
    Seq(
      "org.scalatest" % "scalatest_2.12"    % "3.0.5"         % Test,
      "org.tpolecat"  %% "doobie-scalatest" % Versions.Doobie % Test
    )

  lazy val scalacOptions = Seq(
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-encoding",
    "utf-8",                            // Specify character encoding used by source files.
    "-explaintypes",                    // Explain type errors in more detail.
    "-feature",                         // Emit warning and location for usages of features that should be imported explicitly.
    "-language:existentials",           // Existential types (besides wildcard types) can be written and inferred
    "-language:experimental.macros",    // Allow macro definition (besides implementation and application)
    "-language:higherKinds",            // Allow higher-kinded types
    "-language:implicitConversions",    // Allow definition of implicit functions called views
    "-unchecked",                       // Enable additional warnings where generated code depends on assumptions.
    "-Xcheckinit",                      // Wrap field accessors to throw an exception on uninitialized access.
    "-Xfatal-warnings",                 // Fail the compilation if there are any warnings.
    "-Xfuture",                         // Turn on future language features.
    "-Xlint:adapted-args",              // Warn if an argument list is modified to match the receiver.
    "-Xlint:by-name-right-associative", // By-name parameter of right associative operator.
    "-Xlint:constant",                  // Evaluation of a constant arithmetic expression results in an error.
    "-Xlint:delayedinit-select",        // Selecting member of DelayedInit.
    "-Xlint:doc-detached",              // A Scaladoc comment appears to be detached from its element.
    "-Xlint:inaccessible",              // Warn about inaccessible types in method signatures.
    "-Xlint:infer-any",                 // Warn when a type argument is inferred to be `Any`.
    "-Xlint:missing-interpolator",      // A string literal appears to be missing an interpolator id.
    "-Xlint:nullary-override",          // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Xlint:nullary-unit",              // Warn when nullary methods return Unit.
    "-Xlint:option-implicit",           // Option.apply used implicit view.
    "-Xlint:package-object-classes",    // Class or object defined in package object.
    "-Xlint:poly-implicit-overload",    // Parameterized overloaded implicit methods are not visible as view bounds.
    "-Xlint:private-shadow",            // A private field (or class parameter) shadows a superclass field.
    "-Xlint:stars-align",               // Pattern sequence wildcard must align with sequence component.
    "-Xlint:type-parameter-shadow",     // A local type parameter shadows a type already in scope.
    "-Xlint:unsound-match",             // Pattern match may not be typesafe.
    "-Yno-adapted-args",                // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
    "-Ypartial-unification",            // Enable partial unification in type constructor inference
    "-Ywarn-dead-code",                 // Warn when dead code is identified.
    "-Ywarn-extra-implicit",            // Warn when more than one implicit parameter section is defined.
    "-Ywarn-inaccessible",              // Warn about inaccessible types in method signatures.
    "-Ywarn-infer-any",                 // Warn when a type argument is inferred to be `Any`.
    "-Ywarn-nullary-override",          // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Ywarn-nullary-unit",              // Warn when nullary methods return Unit.
    "-Ywarn-numeric-widen",             // Warn when numerics are widened.
    //  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
    "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
    "-Ywarn-unused:locals",  // Warn if a local definition is unused.
    //  "-Ywarn-unused:params", // Warn if a value parameter is unused.
    "-Ywarn-unused:patvars",  // Warn if a variable bound in a pattern is unused.
    "-Ywarn-unused:privates", // Warn if a private member is unused.
    "-Ywarn-value-discard"    // Warn when non-Unit expression results are unused.
  )

}
